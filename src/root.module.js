var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Injector, APP_INITIALIZER, LOCALE_ID } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { AbpModule } from '@abp/abp.module';
import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { RootRoutingModule } from './root-routing.module';
import { AppConsts } from '@shared/AppConsts';
import { AppSessionService } from '@shared/session/app-session.service';
import { API_BASE_URL } from '@shared/service-proxies/service-proxies';
import { RootComponent } from './root.component';
import { AppPreBootstrap } from './AppPreBootstrap';
// import { ModalModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
export function appInitializerFactory(injector, platformLocation) {
    return function () {
        abp.ui.setBusy();
        return new Promise(function (resolve, reject) {
            AppConsts.appBaseHref = getBaseHref(platformLocation);
            var appBaseUrl = getDocumentOrigin() + AppConsts.appBaseHref;
            AppPreBootstrap.run(appBaseUrl, function () {
                abp.event.trigger('abp.dynamicScriptsInitialized');
                var appSessionService = injector.get(AppSessionService);
                appSessionService.init().then(function (result) {
                    abp.ui.clearBusy();
                    resolve(result);
                }, function (err) {
                    abp.ui.clearBusy();
                    reject(err);
                });
            });
        });
    };
}
export function getRemoteServiceBaseUrl() {
    return AppConsts.remoteServiceBaseUrl;
}
export function getCurrentLanguage() {
    return abp.localization.currentLanguage.name;
}
var RootModule = /** @class */ (function () {
    function RootModule() {
    }
    RootModule = __decorate([
        NgModule({
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule.forRoot(),
                // ModalModule.forRoot(),
                AbpModule,
                ServiceProxyModule,
                RootRoutingModule,
                HttpClientModule
            ],
            declarations: [
                RootComponent
            ],
            providers: [
                { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
                { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
                {
                    provide: APP_INITIALIZER,
                    useFactory: appInitializerFactory,
                    deps: [Injector, PlatformLocation],
                    multi: true
                },
                {
                    provide: LOCALE_ID,
                    useFactory: getCurrentLanguage
                }
            ],
            bootstrap: [RootComponent]
        })
    ], RootModule);
    return RootModule;
}());
export { RootModule };
export function getBaseHref(platformLocation) {
    var baseUrl = platformLocation.getBaseHrefFromDOM();
    if (baseUrl) {
        return baseUrl;
    }
    return '/';
}
function getDocumentOrigin() {
    if (!document.location.origin) {
        return document.location.protocol + '//' + document.location.hostname + (document.location.port ? ':' + document.location.port : '');
    }
    return document.location.origin;
}
