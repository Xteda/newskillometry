var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { AUTH_SERVICE, AuthModule, PROTECTED_FALLBACK_PAGE_URI, PUBLIC_FALLBACK_PAGE_URI } from 'ngx-auth';
import { TokenStorage } from './token-storage.service';
import { AuthenticationService } from './authentication.service';
export function factory(authenticationService) {
    return authenticationService;
}
var AuthenticationModule = /** @class */ (function () {
    function AuthenticationModule() {
    }
    AuthenticationModule = __decorate([
        NgModule({
            imports: [AuthModule],
            providers: [
                TokenStorage,
                AuthenticationService,
                { provide: PROTECTED_FALLBACK_PAGE_URI, useValue: '/' },
                { provide: PUBLIC_FALLBACK_PAGE_URI, useValue: '/login' },
                {
                    provide: AUTH_SERVICE,
                    deps: [AuthenticationService],
                    useFactory: factory
                }
            ]
        })
    ], AuthenticationModule);
    return AuthenticationModule;
}());
export { AuthenticationModule };
