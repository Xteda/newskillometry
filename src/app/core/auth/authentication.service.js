var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Subject, from, throwError } from 'rxjs';
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, tokenStorage, util) {
        this.http = http;
        this.tokenStorage = tokenStorage;
        this.util = util;
        this.API_URL = 'api';
        this.API_ENDPOINT_LOGIN = '/login';
        this.API_ENDPOINT_REFRESH = '/refresh';
        this.API_ENDPOINT_REGISTER = '/register';
        this.onCredentialUpdated$ = new Subject();
    }
    /**
     * Check, if user already authorized.
     * @description Should return Observable with true or false values
     * @returns {Observable<boolean>}
     * @memberOf AuthService
     */
    AuthenticationService.prototype.isAuthorized = function () {
        return this.tokenStorage.getAccessToken().pipe(map(function (token) { return !!token; }));
    };
    /**
     * Get access token
     * @description Should return access token in Observable from e.g. localStorage
     * @returns {Observable<string>}
     */
    AuthenticationService.prototype.getAccessToken = function () {
        return this.tokenStorage.getAccessToken();
    };
    /**
     * Get user roles
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.getUserRoles = function () {
        return this.tokenStorage.getUserRoles();
    };
    /**
     * Function, that should perform refresh token verifyTokenRequest
     * @description Should be successfully completed so interceptor
     * can execute pending requests or retry original one
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.refreshToken = function () {
        var _this = this;
        return this.tokenStorage.getRefreshToken().pipe(switchMap(function (refreshToken) {
            return _this.http.get(_this.API_URL + _this.API_ENDPOINT_REFRESH + '?' + _this.util.urlParam(refreshToken));
        }), tap(this.saveAccessData.bind(this)), catchError(function (err) {
            _this.logout();
            return throwError(err);
        }));
    };
    /**
     * Function, checks response of failed request to determine,
     * whether token be refreshed or not.
     * @description Essentialy checks status
     * @param {Response} response
     * @returns {boolean}
     */
    AuthenticationService.prototype.refreshShouldHappen = function (response) {
        return response.status === 401;
    };
    /**
     * Verify that outgoing request is refresh-token,
     * so interceptor won't intercept this request
     * @param {string} url
     * @returns {boolean}
     */
    AuthenticationService.prototype.verifyTokenRequest = function (url) {
        return url.endsWith(this.API_ENDPOINT_REFRESH);
    };
    /**
     * Submit login request
     * @param {Credential} credential
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.login = function (credential) {
        // Expecting response from API
        var userInfo = {
            'userNameOrEmailAddress': 'admin',
            'password': '123qwe',
            'rememberClient': true
        };
        return this.http.post('http://54.214.10.12/api/TokenAuth/Authenticate', userInfo).pipe(map(function (result) {
            /*if (result instanceof Array) {
                return result.pop();
            }*/
            return result;
        }), tap(this.saveAccessData.bind(this)), catchError(this.handleError('login', [])));
        // {"id":1,"username":"admin","password":"demo","email":"admin@demo.com","accessToken":"access-token-0.022563452858263444","refreshToken":"access-token-0.9348573301432961","roles":["ADMIN"],"pic":"./assets/app/media/img/users/user4.jpg","fullname":"Mark Andre"}
        /*return this.http.get<AccessData>(this.API_URL + this.API_ENDPOINT_LOGIN + '?' + this.util.urlParam(credential)).pipe(
            map((result: any) => {
                if (result instanceof Array) {
                    return result.pop();
                }
                return result;
            }),
            tap(this.saveAccessData.bind(this)),
            catchError(this.handleError('login', []))
        );*/
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    AuthenticationService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // Let the app keep running by returning an empty result.
            return from(result);
        };
    };
    /**
     * Logout
     */
    AuthenticationService.prototype.logout = function (refresh) {
        this.tokenStorage.clear();
        if (refresh) {
            location.reload(true);
        }
    };
    /**
     * Save access data in the storage
     * @private
     * @param {AccessData} data
     */
    AuthenticationService.prototype.saveAccessData = function (accessData) {
        if (typeof accessData !== 'undefined') {
            this.tokenStorage
                .setAccessToken(accessData.accessToken)
                .setRefreshToken(accessData.refreshToken)
                .setUserRoles(accessData.roles);
            this.onCredentialUpdated$.next(accessData);
        }
    };
    /**
     * Submit registration request
     * @param {Credential} credential
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.register = function (credential) {
        // dummy token creation
        credential = Object.assign({}, credential, {
            accessToken: 'access-token-' + Math.random(),
            refreshToken: 'access-token-' + Math.random(),
            roles: ['USER']
        });
        return this.http.post(this.API_URL + this.API_ENDPOINT_REGISTER, credential)
            .pipe(catchError(this.handleError('register', [])));
    };
    /**
     * Submit forgot password request
     * @param {Credential} credential
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.requestPassword = function (credential) {
        return this.http.get(this.API_URL + this.API_ENDPOINT_LOGIN + '?' + this.util.urlParam(credential))
            .pipe(catchError(this.handleError('forgot-password', [])));
    };
    AuthenticationService = __decorate([
        Injectable()
    ], AuthenticationService);
    return AuthenticationService;
}());
export { AuthenticationService };
