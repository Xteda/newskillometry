var AclModel = /** @class */ (function () {
    function AclModel() {
        // default permissions
        this.permissions = {
            ADMIN: ['canDoAnything'],
            USER: ['canDoLimitedThings']
        };
        // store an object of current user roles
        this.currentUserRoles = {};
    }
    return AclModel;
}());
export { AclModel };
