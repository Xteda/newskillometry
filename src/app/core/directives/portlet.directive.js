var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive, HostListener, Input, HostBinding } from '@angular/core';
import * as objectPath from 'object-path';
import { combineLatest } from 'rxjs';
var PortletDirective = /** @class */ (function () {
    function PortletDirective(el, layoutRefService, layoutConfigService) {
        this.el = el;
        this.layoutRefService = layoutRefService;
        this.layoutConfigService = layoutConfigService;
        this["class"] = this.el.nativeElement.classList;
    }
    PortletDirective.prototype.onResize = function (event) {
        if (this.portlet instanceof mPortlet && objectPath.get(this.options, 'enableSticky')) {
            this.portlet.updateSticky();
        }
    };
    PortletDirective.prototype.ngOnInit = function () { };
    PortletDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        var cls = objectPath.get(this.options, 'class');
        if (Array.isArray(cls)) {
            cls.forEach(function (c) {
                _this["class"].add(c);
            });
        }
        else if (cls) {
            this["class"].add(cls);
        }
        if (objectPath.get(this.options, 'enableSticky')) {
            combineLatest(this.layoutRefService.layoutRefs$, this.layoutConfigService.onLayoutConfigUpdated$).subscribe(function (result) {
                if (_this.portlet instanceof mPortlet) {
                    _this.portlet.updateSticky();
                }
                else {
                    _this.initPortlet(result[0], result[1]);
                }
            });
        }
        if (objectPath.get(this.options, 'headOverlay')) {
            this["class"].add('m-portlet--head-overlay');
        }
        if (objectPath.get(this.options, 'headLarge')) {
            this["class"].add('m-portlet--head-lg');
        }
    };
    PortletDirective.prototype.initPortlet = function (res, config) {
        if (typeof res === undefined || res === null) {
            return;
        }
        // check if all the required element exist
        var hasAllParts = true;
        ['header', 'content', 'asideLeft'].forEach(function (part) {
            if (!res.hasOwnProperty(part)) {
                hasAllParts = false;
            }
        });
        if (!hasAllParts) {
            return;
        }
        var headerHeight = parseInt(window.getComputedStyle(objectPath.get(res, 'header'))['height'], null);
        var contentEl = window.getComputedStyle(objectPath.get(res, 'content'));
        var asideLeftEl = window.getComputedStyle(objectPath.get(res, 'asideLeft'));
        var options = {
            sticky: {
                offset: headerHeight + parseInt(objectPath.get(config, 'config.portlet.sticky.offset') || 0, null),
                zIndex: 100,
                position: {
                    top: function () {
                        return headerHeight;
                    },
                    left: function () {
                        var left = parseInt(contentEl['paddingLeft'], null);
                        if (mUtil.isInResponsiveRange('desktop')) {
                            left += parseInt(asideLeftEl['width'], null);
                        }
                        return left;
                    },
                    right: function () {
                        return parseInt(contentEl['paddingRight'], null);
                    }
                }
            }
        };
        this.options = Object.assign(this.options, options);
        this.portlet = new mPortlet(this.el.nativeElement, this.options);
        this.portlet.initSticky();
    };
    PortletDirective.prototype.ngOnDestroy = function () {
        if (this.portlet instanceof mPortlet && objectPath.get(this.options, 'enableSticky')) {
            this.portlet.destroySticky();
        }
    };
    __decorate([
        Input()
    ], PortletDirective.prototype, "options");
    __decorate([
        HostBinding('class')
    ], PortletDirective.prototype, "class");
    __decorate([
        HostListener('window:resize', ['$event'])
    ], PortletDirective.prototype, "onResize");
    PortletDirective = __decorate([
        Directive({
            selector: '[mPortlet]'
        })
    ], PortletDirective);
    return PortletDirective;
}());
export { PortletDirective };
