var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive } from '@angular/core';
var MenuHorizontalOffcanvasDirective = /** @class */ (function () {
    function MenuHorizontalOffcanvasDirective(el) {
        this.el = el;
    }
    MenuHorizontalOffcanvasDirective.prototype.ngAfterViewInit = function () {
        // init the mOffcanvas plugin
        this.menuOffcanvas = new mOffcanvas(this.el.nativeElement, {
            overlay: true,
            baseClass: 'm-aside-header-menu-mobile',
            closeBy: 'm_aside_header_menu_mobile_close_btn',
            toggleBy: {
                target: 'm_aside_header_menu_mobile_toggle',
                state: 'm-brand__toggler--active'
            }
        });
    };
    MenuHorizontalOffcanvasDirective.prototype.ngOnDestroy = function () { };
    MenuHorizontalOffcanvasDirective = __decorate([
        Directive({
            selector: '[mMenuHorizontalOffcanvas]'
        })
    ], MenuHorizontalOffcanvasDirective);
    return MenuHorizontalOffcanvasDirective;
}());
export { MenuHorizontalOffcanvasDirective };
