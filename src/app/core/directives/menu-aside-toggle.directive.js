var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive } from '@angular/core';
var MenuAsideToggleDirective = /** @class */ (function () {
    function MenuAsideToggleDirective(el) {
        this.el = el;
    }
    MenuAsideToggleDirective.prototype.ngAfterViewInit = function () {
        this.toggle = new mToggle(this.el.nativeElement, {
            target: 'body',
            targetState: 'm-brand--minimize m-aside-left--minimize',
            togglerState: 'm-brand__toggler--active'
        });
        this.el.nativeElement.addEventListener('toggle', function (e) {
            console.log(e);
        });
    };
    MenuAsideToggleDirective.prototype.ngOnDestroy = function () { };
    MenuAsideToggleDirective = __decorate([
        Directive({
            selector: '[mMenuAsideToggle]'
        })
    ], MenuAsideToggleDirective);
    return MenuAsideToggleDirective;
}());
export { MenuAsideToggleDirective };
