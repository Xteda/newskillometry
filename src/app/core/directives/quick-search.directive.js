var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive } from '@angular/core';
import { Subject } from 'rxjs';
var QuickSearchDirective = /** @class */ (function () {
    function QuickSearchDirective(el) {
        this.el = el;
        this.onSearch$ = new Subject();
    }
    QuickSearchDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        var mode = this.el.nativeElement.getAttribute('m-quicksearch-mode');
        // init mQuicksearch plugin
        this.quicksearch = new mQuicksearch(this.el.nativeElement, {
            mode: mode,
            minLength: 1
        });
        this.quicksearch.on('search', function (plugin) {
            _this.onSearch$.next(plugin);
        });
    };
    QuickSearchDirective.prototype.ngOnDestroy = function () { };
    QuickSearchDirective = __decorate([
        Directive({
            selector: '[mQuickSearch]'
        })
    ], QuickSearchDirective);
    return QuickSearchDirective;
}());
export { QuickSearchDirective };
