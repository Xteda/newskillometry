var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive } from '@angular/core';
var HeaderDirective = /** @class */ (function () {
    function HeaderDirective(el) {
        this.el = el;
    }
    HeaderDirective.prototype.ngAfterViewInit = function () {
        var tmp;
        var options = {
            offset: {},
            minimize: {}
        };
        if (this.el.nativeElement.getAttribute('m-minimize-mobile') === 'hide') {
            options.minimize = {
                mobile: {
                    on: 'm-header--hide',
                    off: 'm-header--show'
                }
            };
        }
        else {
            options.minimize = { mobile: false };
        }
        var attrMinimize = this.el.nativeElement.getAttribute('m-minimize');
        if (attrMinimize === 'hide') {
            options.minimize = {
                desktop: {
                    on: 'm-header--hide',
                    off: 'm-header--show'
                }
            };
        }
        else if (attrMinimize === 'minimize') {
            options.minimize = {
                desktop: {
                    on: 'm-header--minimize-on',
                    off: 'm-header--minimize-off'
                },
                mobile: {
                    on: 'm-header--minimize-on',
                    off: 'm-header--minimize-off'
                }
            };
        }
        else {
            options.minimize = { desktop: false };
        }
        if ((tmp = this.el.nativeElement.getAttribute('m-minimize-offset'))) {
            options.offset = { desktop: tmp };
        }
        if ((tmp = mUtil.attr(this.el.nativeElement, 'm-minimize-mobile-offset'))) {
            options.offset = { mobile: tmp };
        }
        this.header = new mHeader(this.el.nativeElement, options);
    };
    HeaderDirective.prototype.ngOnDestroy = function () {
    };
    HeaderDirective = __decorate([
        Directive({
            selector: '[mHeader]'
        })
    ], HeaderDirective);
    return HeaderDirective;
}());
export { HeaderDirective };
