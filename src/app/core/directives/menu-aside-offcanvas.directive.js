var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive } from '@angular/core';
var MenuAsideOffcanvasDirective = /** @class */ (function () {
    function MenuAsideOffcanvasDirective(el) {
        this.el = el;
    }
    MenuAsideOffcanvasDirective.prototype.ngAfterViewInit = function () {
        // check class for the offcanvas option
        // tslint:disable-next-line:max-line-length
        var offcanvasClass = mUtil.hasClass(this.el.nativeElement, 'm-aside-left--offcanvas-default') ? 'm-aside-left--offcanvas-default' : 'm-aside-left';
        // init the mOffcanvas plugin
        this.menuOffcanvas = new mOffcanvas(this.el.nativeElement, {
            baseClass: offcanvasClass,
            overlay: true,
            closeBy: 'm_aside_left_close_btn',
            toggleBy: {
                target: 'm_aside_left_offcanvas_toggle',
                state: 'm-brand__toggler--active'
            }
        });
    };
    MenuAsideOffcanvasDirective.prototype.ngOnDestroy = function () { };
    MenuAsideOffcanvasDirective = __decorate([
        Directive({
            selector: '[mMenuAsideOffcanvas]'
        })
    ], MenuAsideOffcanvasDirective);
    return MenuAsideOffcanvasDirective;
}());
export { MenuAsideOffcanvasDirective };
