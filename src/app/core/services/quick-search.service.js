var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
var QuickSearchService = /** @class */ (function () {
    function QuickSearchService(http) {
        this.http = http;
        this.API_URL = 'api';
        this.API_ENDPOINT = '/quick_search';
    }
    QuickSearchService.prototype.search = function () {
        return this.http.get(this.API_URL + this.API_ENDPOINT);
    };
    QuickSearchService = __decorate([
        Injectable()
    ], QuickSearchService);
    return QuickSearchService;
}());
export { QuickSearchService };
