var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { NavigationStart } from '@angular/router';
import { MenuConfig } from '../../config/menu';
var MenuConfigService = /** @class */ (function () {
    function MenuConfigService(router) {
        var _this = this;
        this.router = router;
        this.configModel = new MenuConfig();
        this.onMenuUpdated$ = new BehaviorSubject(this.configModel);
        this.menuHasChanged = false;
        this.router.events
            .pipe(filter(function (event) { return event instanceof NavigationStart; }))
            .subscribe(function (event) {
            if (_this.menuHasChanged) {
                _this.resetModel();
            }
        });
    }
    MenuConfigService.prototype.setModel = function (menuModel) {
        this.configModel = Object.assign(this.configModel, menuModel);
        this.onMenuUpdated$.next(this.configModel);
        this.menuHasChanged = true;
    };
    MenuConfigService.prototype.resetModel = function () {
        this.configModel = new MenuConfig();
        this.onMenuUpdated$.next(this.configModel);
        this.menuHasChanged = false;
    };
    MenuConfigService = __decorate([
        Injectable()
    ], MenuConfigService);
    return MenuConfigService;
}());
export { MenuConfigService };
