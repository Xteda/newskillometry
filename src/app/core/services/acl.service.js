var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { AclModel } from '../models/acl';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
var AclService = /** @class */ (function () {
    function AclService(roleService, permService, authService) {
        var _this = this;
        this.roleService = roleService;
        this.permService = permService;
        this.authService = authService;
        // set initial permission model
        this.aclModel = new AclModel();
        this.onAclUpdated$ = new BehaviorSubject(this.aclModel);
        this.authService.getUserRoles().subscribe(function (roles) {
            _this.setCurrrentUserRoles(roles);
        });
        // subscribe to credential changed, eg. after login response
        this.authService.onCredentialUpdated$
            .pipe(mergeMap(function (accessData) { return _this.authService.getUserRoles(); }))
            .subscribe(function (roles) { return _this.setCurrrentUserRoles(roles); });
        // subscribe to acl data observable
        this.onAclUpdated$.subscribe(function (acl) {
            var permissions = Object.keys(acl.permissions).map(function (key) {
                return acl.permissions[key];
            });
            // load default permission list
            _this.permService.loadPermissions(permissions, function (permissionName, permissionStore) { return !!permissionStore[permissionName]; });
            // merge current user roles
            var roles = Object.assign({}, _this.aclModel.currentUserRoles, {
                // default user role is GUEST
                GUEST: function () {
                    // return this.authService.isAuthorized().toPromise();
                }
            });
            // add to role service
            _this.roleService.addRoles(roles);
        });
    }
    /**
     * Set AclModel and fire off event that all subscribers will listen to
     * @param aclModel
     */
    AclService.prototype.setModel = function (aclModel) {
        aclModel = Object.assign({}, this.aclModel, aclModel);
        this.onAclUpdated$.next(aclModel);
    };
    AclService.prototype.setCurrrentUserRoles = function (roles) {
        var _this = this;
        // update roles if the credential data has roles
        if (roles != null) {
            this.aclModel.currentUserRoles = {};
            roles.forEach(function (role) {
                _this.aclModel.currentUserRoles[role] = _this.aclModel.permissions[role];
            });
            // set updated acl model back to service
            this.setModel(this.aclModel);
        }
    };
    AclService = __decorate([
        Injectable()
    ], AclService);
    return AclService;
}());
export { AclService };
