var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PagesConfig } from '../../config/pages';
import * as objectPath from 'object-path';
var PageConfigService = /** @class */ (function () {
    function PageConfigService(router) {
        this.router = router;
        this.configModel = new PagesConfig();
        this.onPageUpdated$ = new BehaviorSubject(this.configModel);
    }
    PageConfigService.prototype.setModel = function (menuModel) {
        this.configModel = Object.assign(this.configModel, menuModel);
        this.onPageUpdated$.next(this.configModel);
    };
    PageConfigService.prototype.getCurrentPageConfig = function () {
        return objectPath.get(this.configModel, 'config.' + this.router.url.substring(1).replace(/\//g, '.'));
    };
    PageConfigService = __decorate([
        Injectable()
    ], PageConfigService);
    return PageConfigService;
}());
export { PageConfigService };
