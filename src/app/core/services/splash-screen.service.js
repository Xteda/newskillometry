var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { animate, style } from '@angular/animations';
import { NavigationEnd } from '@angular/router';
var SplashScreenService = /** @class */ (function () {
    function SplashScreenService(animationBuilder, router) {
        this.animationBuilder = animationBuilder;
        this.router = router;
    }
    SplashScreenService.prototype.init = function (element) {
        var _this = this;
        // Get the splash screen element
        this.splashElement = element;
        // Hide it on the first NavigationEnd event
        var routerEvents = this.router.events.subscribe(function (event) {
            if (event instanceof NavigationEnd) {
                _this.hide();
                routerEvents.unsubscribe();
            }
        });
    };
    SplashScreenService.prototype.show = function () {
        var _this = this;
        this.player = this.animationBuilder
            .build([
            style({ opacity: '0', zIndex: '99999' }),
            animate('600ms ease', style({ opacity: '1' }))
        ])
            .create(this.splashElement);
        setTimeout(function () {
            _this.player.play();
        }, 0);
    };
    SplashScreenService.prototype.hide = function () {
        var _this = this;
        this.player = this.animationBuilder
            .build([
            style({ opacity: '1' }),
            animate('600ms ease', style({ opacity: '0' }))
        ])
            .create(this.splashElement);
        setTimeout(function () {
            _this.player.onDone(function () { return (_this.splashElement.style.display = 'none'); });
            _this.player.play();
        }, 0);
    };
    SplashScreenService = __decorate([
        Injectable()
    ], SplashScreenService);
    return SplashScreenService;
}());
export { SplashScreenService };
