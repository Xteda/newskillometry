var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { filter, mergeMap } from 'rxjs/operators';
import { NavigationEnd } from '@angular/router';
import { LayoutConfig } from '../../config/layout';
var LayoutConfigService = /** @class */ (function () {
    function LayoutConfigService(router, utils, layoutConfigStorageService) {
        var _this = this;
        this.router = router;
        this.utils = utils;
        this.layoutConfigStorageService = layoutConfigStorageService;
        // default config
        this.layoutConfig = new LayoutConfig();
        // register on config changed event and set default config
        this.onLayoutConfigUpdated$ = new BehaviorSubject(this.layoutConfig);
        this.router.events
            .pipe(filter(function (event) { return event instanceof NavigationEnd; }), mergeMap(function () { return _this.layoutConfigStorageService.loadConfig(); }))
            .subscribe(function (config) {
            _this.layoutConfig = config;
            _this.onLayoutConfigUpdated$.next(config);
        });
    }
    /**
     * Reset existing configurations
     * NOTE: This method will remove older config and pass only new;
     * @param model
     * @param doNotSave
     */
    LayoutConfigService.prototype.setModel = function (model, doNotSave) {
        // merge and replace existing config object
        // deep merge for mutltidimentional arrays
        this.layoutConfig = Object.assign({}, this.layoutConfig, model);
        if (!doNotSave) {
            this.layoutConfigStorageService.saveConfig(this.layoutConfig);
        }
        // fire off an event that all subscribers will listen
        this.onLayoutConfigUpdated$.next(this.layoutConfig);
    };
    LayoutConfigService.prototype.reloadSavedConfig = function () {
        this.setModel(new LayoutConfig(this.getSavedConfig()), true);
    };
    /**
     * Set current config as default template.
     * This config is changeable via layout builder.
     * Useful when want to reset layout without clearing the config at layout
     */
    LayoutConfigService.prototype.getSavedConfig = function () {
        return this.layoutConfigStorageService.loadConfig();
    };
    LayoutConfigService = __decorate([
        Injectable()
    ], LayoutConfigService);
    return LayoutConfigService;
}());
export { LayoutConfigService };
