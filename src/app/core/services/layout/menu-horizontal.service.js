var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import * as objectPath from 'object-path';
var MenuHorizontalService = /** @class */ (function () {
    function MenuHorizontalService(menuConfigService, classInitService, layoutConfigService) {
        var _this = this;
        this.menuConfigService = menuConfigService;
        this.classInitService = classInitService;
        this.layoutConfigService = layoutConfigService;
        this.menuList$ = new BehaviorSubject([]);
        // get menu list
        this.menuConfigService.onMenuUpdated$.subscribe(function (model) {
            _this.menuList$.next(objectPath.get(model.config, 'header.items'));
        });
        // subscribe to menu classes update
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            // default class
            _this.menuClasses =
                'm-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas';
            // join the classes array and pass to variable
            // add classes to this host binding class
            _this.menuClasses += ' ' + classes.header_menu.join(' ');
        });
    }
    MenuHorizontalService = __decorate([
        Injectable()
    ], MenuHorizontalService);
    return MenuHorizontalService;
}());
export { MenuHorizontalService };
