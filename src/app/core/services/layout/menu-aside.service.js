var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import * as objectPath from 'object-path';
var MenuAsideService = /** @class */ (function () {
    function MenuAsideService(menuConfigService, classInitService, layoutConfigService) {
        var _this = this;
        this.menuConfigService = menuConfigService;
        this.classInitService = classInitService;
        this.layoutConfigService = layoutConfigService;
        this.menuList$ = new BehaviorSubject([]);
        this.isDropdown = 0;
        this.isScrollable = 0;
        // get menu list
        this.menuConfigService.onMenuUpdated$.subscribe(function (model) {
            setTimeout(function () {
                return _this.menuList$.next(objectPath.get(model.config, 'aside.items'));
            });
        });
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (config) {
            if (objectPath.get(config, 'config.aside.left.fixed')) {
                _this.isScrollable = 1;
                _this.isDropdown = 0;
            }
            if (!objectPath.get(config, 'config.aside.left.fixed') && !objectPath.get(config, 'config.menu.aside.desktop_and_mobile.submenu.accordion')) {
                _this.isScrollable = 0;
                _this.isDropdown = 1;
                _this.dropdownTimeout = objectPath.get(config, 'config.menu.aside.desktop_and_mobile.submenu.dropdown.hover_timeout');
            }
        });
    }
    MenuAsideService = __decorate([
        Injectable()
    ], MenuAsideService);
    return MenuAsideService;
}());
export { MenuAsideService };
