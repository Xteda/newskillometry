var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { LayoutConfig } from '../../config/layout';
import { Injectable } from '@angular/core';
var LayoutConfigStorageService = /** @class */ (function () {
    function LayoutConfigStorageService(utils) {
        this.utils = utils;
    }
    LayoutConfigStorageService.prototype.saveConfig = function (layoutConfig) {
        if (layoutConfig != null) {
            // config storage
            localStorage.setItem('layoutConfig', JSON.stringify(layoutConfig));
        }
    };
    LayoutConfigStorageService.prototype.getSavedConfig = function () {
        var config = localStorage.getItem('layoutConfig');
        try {
            return of(JSON.parse(config));
        }
        catch (e) { }
    };
    LayoutConfigStorageService.prototype.loadConfig = function () {
        return this.getSavedConfig().pipe(map(function (config) {
            return Object.assign({}, new LayoutConfig(), config);
        }));
    };
    LayoutConfigStorageService.prototype.resetConfig = function () {
        localStorage.removeItem('layoutConfig');
    };
    LayoutConfigStorageService = __decorate([
        Injectable()
    ], LayoutConfigStorageService);
    return LayoutConfigStorageService;
}());
export { LayoutConfigStorageService };
