var LayoutConfig = /** @class */ (function () {
    function LayoutConfig(config) {
        this.config = {
            demo: 'default',
            // == Base Layout
            self: {
                layout: 'fluid',
                background: './assets/app/media/img/bg/bg-4.jpg' // boxed layout background
            },
            // == Page Loader(splash)
            loader: {
                enabled: true,
                image: './assets/demo/default/media/img/logo/logo_large.png'
            },
            // == Header
            header: {
                // Header itself
                self: {
                    // Fixed header(sticky) mode
                    fixed: {
                        desktop: true,
                        mobile: true,
                        // Minimize header on scroll
                        minimize: {
                            // Desktop mode
                            desktop: {
                                enabled: false,
                                offset: 200 // Offset(in px) to start header minimization
                            },
                            // Mobile mode
                            mobile: {
                                enabled: false,
                                offset: 200 // Offset(in px) to start header minimization
                            }
                        }
                    },
                    logo: {
                        dark: './assets/demo/default/media/img/logo/logo_default_dark.png',
                        light: './assets/demo/default/media/img/logo/logo_default_light.png'
                    }
                },
                // Header search(quicksearch)
                search: {
                    type: 'search-dropdown',
                    // Header search results dropdown
                    dropdown: {
                        skin: 'light' // Select results dropdown skin from available options:  light|dark
                    }
                }
            },
            // == Asides(left, right and mobile asides)
            aside: {
                // Left aside(used for left aside menu)
                left: {
                    display: true,
                    fixed: true,
                    skin: 'dark',
                    push_footer: true,
                    //  Left aside minimize toggle
                    minimize: {
                        toggle: true,
                        "default": false // Set left aside minimized by default
                    }
                },
                // Right aside(used for blank right aside)
                right: {
                    display: false //  Display or hide right aside
                }
            },
            // == Menus
            menu: {
                header: {
                    // Display or hide header menu
                    display: true,
                    //  header menu desktop mode
                    desktop: {
                        skin: 'light',
                        arrow: true,
                        toggle: 'click',
                        submenu: {
                            skin: 'light',
                            arrow: true // Enable header menu submenu arrow
                        }
                    },
                    //  header menu mobile mode
                    mobile: {
                        skin: 'dark' // Select header menu skin from available options:  light|dark
                    }
                },
                // Left aside menu
                aside: {
                    // Display or hide header menu
                    display: true,
                    // left aside menu desktop and mobile modes
                    desktop_and_mobile: {
                        // Left aside menu submenu settings
                        submenu: {
                            skin: 'inherit',
                            accordion: true,
                            dropdown: {
                                // Set submenu dropdown mode(for minimized left aside mode and or submenu dropdown mode activated when accordion: false set)
                                arrow: true,
                                hover_timeout: 500 // Timeout to auto hide the opened submenu dropdown
                            }
                        },
                        // Minimized left aside menu
                        minimize: {
                            submenu_type: 'default' // Select submenu type for minimized left aside mode from available options: default/compact
                        }
                    }
                }
            },
            // == Content
            content: {
                skin: 'light2' // Select main content skin from available options: light|light2,
            },
            // == Footer
            footer: {
                fixed: false // Set fixed footer layout
            },
            // == Quick Sidebar
            quicksidebar: {
                display: true // Display or hide quicksidebar
            },
            // == Portlet Plugin
            portlet: {
                sticky: {
                    offset: 50
                }
            }
        };
        if (config) {
            this.config = config;
        }
    }
    return LayoutConfig;
}());
export { LayoutConfig };
