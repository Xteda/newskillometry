var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
var HeaderSearchComponent = /** @class */ (function () {
    function HeaderSearchComponent(layoutConfigService, el) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.el = el;
        this.classes = '';
        this.attrQuickSearchMode = 'default';
        this.onLayoutConfigUpdated = this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            _this.classes =
                // tslint:disable-next-line:max-line-length
                'm-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-';
        });
    }
    HeaderSearchComponent.prototype.ngOnInit = function () { };
    HeaderSearchComponent.prototype.ngOnDestroy = function () {
        this.onLayoutConfigUpdated.unsubscribe();
    };
    __decorate([
        HostBinding('class')
    ], HeaderSearchComponent.prototype, "classes");
    __decorate([
        HostBinding('attr.m-quicksearch-mode')
    ], HeaderSearchComponent.prototype, "attrQuickSearchMode");
    HeaderSearchComponent = __decorate([
        Component({
            selector: 'm-header-search',
            templateUrl: './header-search.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], HeaderSearchComponent);
    return HeaderSearchComponent;
}());
export { HeaderSearchComponent };
