var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { ChangeDetectionStrategy, Component, HostBinding, Inject, Input } from '@angular/core';
import * as objectPath from 'object-path';
import { DOCUMENT } from '@angular/common';
var BrandComponent = /** @class */ (function () {
    function BrandComponent(classInitService, layoutConfigService, document) {
        var _this = this;
        this.classInitService = classInitService;
        this.layoutConfigService = layoutConfigService;
        this.document = document;
        this.classes = 'm-stack__item m-brand';
        this.menuAsideLeftSkin = '';
        this.menuAsideMinimizeDefault = false;
        this.menuAsideMinimizToggle = false;
        this.menuAsideDisplay = false;
        this.menuHeaderDisplay = true;
        this.headerLogo = '';
        // subscribe to class update event
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            _this.classes = 'm-stack__item m-brand ' + classes.brand.join(' ');
        });
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            _this.menuAsideLeftSkin = objectPath.get(model, 'config.aside.left.skin');
            _this.menuAsideMinimizeDefault = objectPath.get(model, 'config.aside.left.minimize.default');
            _this.menuAsideMinimizToggle = objectPath.get(model, 'config.aside.left.minimize.toggle');
            _this.menuAsideDisplay = objectPath.get(model, 'config.menu.aside.display');
            _this.menuHeaderDisplay = objectPath.get(model, 'config.menu.header.display');
            var headerLogo = objectPath.get(model, 'config.header.self.logo');
            if (typeof headerLogo === 'object') {
                _this.headerLogo = objectPath.get(headerLogo, _this.menuAsideLeftSkin);
            }
            else {
                _this.headerLogo = headerLogo;
            }
        });
    }
    BrandComponent.prototype.ngOnInit = function () { };
    /**
     * Toggle class topbar show/hide
     * @param event
     */
    BrandComponent.prototype.clickTopbarToggle = function (event) {
        this.document.body.classList.toggle('m-topbar--on');
    };
    __decorate([
        HostBinding('class')
    ], BrandComponent.prototype, "classes");
    __decorate([
        Input()
    ], BrandComponent.prototype, "menuAsideLeftSkin");
    __decorate([
        Input()
    ], BrandComponent.prototype, "menuAsideMinimizeDefault");
    __decorate([
        Input()
    ], BrandComponent.prototype, "menuAsideMinimizToggle");
    __decorate([
        Input()
    ], BrandComponent.prototype, "menuAsideDisplay");
    __decorate([
        Input()
    ], BrandComponent.prototype, "menuHeaderDisplay");
    __decorate([
        Input()
    ], BrandComponent.prototype, "headerLogo");
    BrandComponent = __decorate([
        Component({
            selector: 'm-brand',
            templateUrl: './brand.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __param(2, Inject(DOCUMENT))
    ], BrandComponent);
    return BrandComponent;
}());
export { BrandComponent };
