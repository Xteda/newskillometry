var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationStart, RouteConfigLoadEnd, RouteConfigLoadStart } from '@angular/router';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router, layoutRefService, headerService, loader) {
        var _this = this;
        this.router = router;
        this.layoutRefService = layoutRefService;
        this.headerService = headerService;
        this.loader = loader;
        // page progress bar percentage
        this.router.events.subscribe(function (event) {
            if (event instanceof NavigationStart) {
                // set page progress bar loading to start on NavigationStart event router
                _this.loader.start();
            }
            if (event instanceof RouteConfigLoadStart) {
                _this.loader.increment(35);
            }
            if (event instanceof RouteConfigLoadEnd) {
                _this.loader.increment(75);
            }
            if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
                // set page progress bar loading to end on NavigationEnd event router
                _this.loader.complete();
            }
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.ngAfterViewInit = function () {
        // keep header element in the service
        this.layoutRefService.addElement('header', this.mHeader.nativeElement);
    };
    __decorate([
        ViewChild('mHeader')
    ], HeaderComponent.prototype, "mHeader");
    HeaderComponent = __decorate([
        Component({
            selector: 'm-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.scss'],
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], HeaderComponent);
    return HeaderComponent;
}());
export { HeaderComponent };
