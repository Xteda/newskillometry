var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, Input, ChangeDetectionStrategy } from '@angular/core';
import * as objectPath from 'object-path';
var SearchDefaultComponent = /** @class */ (function () {
    function SearchDefaultComponent(layoutConfigService, el) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.el = el;
        this.classes = '';
        // @HostBinding('id') id = 'm_quicksearch';
        this.attrQuickSearchMode = 'default';
        this.attrDropdownPersistent = '1';
        this.onLayoutConfigUpdated = this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            _this.headerSearchSkinClass =
                'm-list-search--skin-' +
                    objectPath.get(config, 'header.search.dropdown.skin');
            _this.classes =
                // tslint:disable-next-line:max-line-length
                'm-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-header-search m-header-search--expandable';
            _this.classes +=
                ' m-dropdown--skin-' +
                    objectPath.get(config, 'header.search.dropdown.skin');
        });
    }
    SearchDefaultComponent.prototype.ngOnInit = function () { };
    SearchDefaultComponent.prototype.ngOnDestroy = function () {
        this.onLayoutConfigUpdated.unsubscribe();
    };
    __decorate([
        HostBinding('class')
    ], SearchDefaultComponent.prototype, "classes");
    __decorate([
        HostBinding('attr.m-quicksearch-mode')
    ], SearchDefaultComponent.prototype, "attrQuickSearchMode");
    __decorate([
        HostBinding('attr.m-dropdown-persistent')
    ], SearchDefaultComponent.prototype, "attrDropdownPersistent");
    __decorate([
        Input()
    ], SearchDefaultComponent.prototype, "headerSearchSkinClass");
    SearchDefaultComponent = __decorate([
        Component({
            selector: 'm-search-default',
            templateUrl: './search-default.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], SearchDefaultComponent);
    return SearchDefaultComponent;
}());
export { SearchDefaultComponent };
