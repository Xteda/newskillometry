var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
var QuickActionComponent = /** @class */ (function () {
    function QuickActionComponent() {
        // tslint:disable-next-line:max-line-length
        this.classes = 'm-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light';
        this.attrDropdownToggle = 'click';
    }
    QuickActionComponent.prototype.ngOnInit = function () { };
    __decorate([
        HostBinding('class')
        // tslint:disable-next-line:max-line-length
    ], QuickActionComponent.prototype, "classes");
    __decorate([
        HostBinding('attr.m-dropdown-toggle')
    ], QuickActionComponent.prototype, "attrDropdownToggle");
    QuickActionComponent = __decorate([
        Component({
            selector: 'm-quick-action',
            templateUrl: './quick-action.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], QuickActionComponent);
    return QuickActionComponent;
}());
export { QuickActionComponent };
