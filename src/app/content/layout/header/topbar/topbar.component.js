var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, Input, ChangeDetectionStrategy } from '@angular/core';
import * as objectPath from 'object-path';
var TopbarComponent = /** @class */ (function () {
    function TopbarComponent(layoutConfigService, router) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.router = router;
        this.id = 'm_header_nav';
        this.classes = 'm-stack__item m-stack__item--fluid m-header-head';
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            _this.searchType = objectPath.get(config, 'header.search.type');
        });
    }
    TopbarComponent.prototype.ngOnInit = function () { };
    TopbarComponent.prototype.ngAfterViewInit = function () { };
    __decorate([
        HostBinding('id')
    ], TopbarComponent.prototype, "id");
    __decorate([
        HostBinding('class')
    ], TopbarComponent.prototype, "classes");
    __decorate([
        Input()
    ], TopbarComponent.prototype, "searchType");
    TopbarComponent = __decorate([
        Component({
            selector: 'm-topbar',
            templateUrl: './topbar.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], TopbarComponent);
    return TopbarComponent;
}());
export { TopbarComponent };
