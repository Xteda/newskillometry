var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { ChangeDetectionStrategy, Component, HostBinding, Input, ViewChild } from '@angular/core';
var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(router, authService, sanitizer) {
        this.router = router;
        this.authService = authService;
        this.sanitizer = sanitizer;
        // tslint:disable-next-line:max-line-length
        this.classes = 'm-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light';
        this.attrDropdownToggle = 'click';
        this.avatar = './assets/app/media/img/users/user4.jpg';
        this.avatarBg = '';
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        if (!this.avatarBg)
            this.avatarBg = this.sanitizer.bypassSecurityTrustStyle('url(./assets/app/media/img/misc/user_profile_bg.jpg)');
    };
    UserProfileComponent.prototype.logout = function () {
        this.authService.logout(true);
    };
    __decorate([
        HostBinding('class')
        // tslint:disable-next-line:max-line-length
    ], UserProfileComponent.prototype, "classes");
    __decorate([
        HostBinding('attr.m-dropdown-toggle')
    ], UserProfileComponent.prototype, "attrDropdownToggle");
    __decorate([
        Input()
    ], UserProfileComponent.prototype, "avatar");
    __decorate([
        Input()
    ], UserProfileComponent.prototype, "avatarBg");
    __decorate([
        ViewChild('mProfileDropdown')
    ], UserProfileComponent.prototype, "mProfileDropdown");
    UserProfileComponent = __decorate([
        Component({
            selector: 'm-user-profile',
            templateUrl: './user-profile.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], UserProfileComponent);
    return UserProfileComponent;
}());
export { UserProfileComponent };
