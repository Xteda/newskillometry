var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import * as objectPath from 'object-path';
import { QuickSearchDirective } from '../../../../../core/directives/quick-search.directive';
var SearchDropdownComponent = /** @class */ (function () {
    function SearchDropdownComponent(layoutConfigService, el, quickSearchService) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.el = el;
        this.quickSearchService = quickSearchService;
        this.classes = '';
        this.id = 'm_quicksearch';
        this.attrDropdownToggle = 'click';
        this.attrDropdownPersistent = '1';
        this.attrQuicksearchMode = 'dropdown';
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            _this.classes =
                // tslint:disable-next-line:max-line-length
                'm-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light m-list-search m-list-search--skin-light';
            _this.classes +=
                ' m-dropdown--skin-' +
                    objectPath.get(config, 'header.search.dropdown.skin');
        });
    }
    SearchDropdownComponent.prototype.ngOnInit = function () { };
    SearchDropdownComponent.prototype.ngOnDestroy = function () {
        this.onSearch.unsubscribe();
    };
    SearchDropdownComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this.mQuickSearchDirective = new QuickSearchDirective(_this.el);
            _this.mQuickSearchDirective.ngAfterViewInit();
            // listen to search event
            _this.onSearch = _this.mQuickSearchDirective.onSearch$.subscribe(function (mQuickSearch) {
                mQuickSearch.showProgress();
                _this.quickSearchService.search().subscribe(function (result) {
                    // append search result
                    mQuickSearch.showResult(result[0]);
                    mQuickSearch.hideProgress();
                });
            });
        });
    };
    __decorate([
        HostBinding('class')
    ], SearchDropdownComponent.prototype, "classes");
    __decorate([
        HostBinding('id')
    ], SearchDropdownComponent.prototype, "id");
    __decorate([
        HostBinding('attr.m-dropdown-toggle')
    ], SearchDropdownComponent.prototype, "attrDropdownToggle");
    __decorate([
        HostBinding('attr.m-dropdown-persistent')
    ], SearchDropdownComponent.prototype, "attrDropdownPersistent");
    __decorate([
        HostBinding('attr.m-quicksearch-mode')
    ], SearchDropdownComponent.prototype, "attrQuicksearchMode");
    __decorate([
        HostBinding('attr.mQuickSearch')
    ], SearchDropdownComponent.prototype, "mQuickSearchDirective");
    SearchDropdownComponent = __decorate([
        Component({
            selector: 'm-search-dropdown',
            templateUrl: './search-dropdown.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], SearchDropdownComponent);
    return SearchDropdownComponent;
}());
export { SearchDropdownComponent };
