var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as objectPath from 'object-path';
var FooterComponent = /** @class */ (function () {
    function FooterComponent(configService) {
        var _this = this;
        this.configService = configService;
        this.classes = 'm-grid__item m-footer';
        this.footerContainerClass$ = new BehaviorSubject('');
        this.configService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            var pageBodyClass = '';
            var selfLayout = objectPath.get(config, 'self.layout');
            if (selfLayout === 'boxed' || selfLayout === 'wide') {
                pageBodyClass += 'm-container--responsive m-container--xxl';
            }
            else {
                pageBodyClass += 'm-container--fluid';
            }
            _this.footerContainerClass$.next(pageBodyClass);
        });
    }
    FooterComponent.prototype.ngOnInit = function () { };
    __decorate([
        HostBinding('class')
    ], FooterComponent.prototype, "classes");
    FooterComponent = __decorate([
        Component({
            selector: 'm-footer',
            templateUrl: './footer.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], FooterComponent);
    return FooterComponent;
}());
export { FooterComponent };
