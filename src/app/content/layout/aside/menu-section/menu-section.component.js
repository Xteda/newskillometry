var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, Input, ChangeDetectionStrategy } from '@angular/core';
var MenuSectionComponent = /** @class */ (function () {
    function MenuSectionComponent() {
        this.classes = 'm-menu__section';
    }
    MenuSectionComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], MenuSectionComponent.prototype, "item");
    __decorate([
        HostBinding('class')
    ], MenuSectionComponent.prototype, "classes");
    MenuSectionComponent = __decorate([
        Component({
            selector: 'm-menu-section',
            templateUrl: './menu-section.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], MenuSectionComponent);
    return MenuSectionComponent;
}());
export { MenuSectionComponent };
