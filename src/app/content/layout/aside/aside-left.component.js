var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { ChangeDetectionStrategy, Component, HostBinding, Inject } from '@angular/core';
import { filter } from 'rxjs/operators';
import { NavigationEnd } from '@angular/router';
import { MenuAsideOffcanvasDirective } from '../../../core/directives/menu-aside-offcanvas.directive';
import { DOCUMENT } from '@angular/common';
var AsideLeftComponent = /** @class */ (function () {
    function AsideLeftComponent(el, classInitService, menuAsideService, layoutConfigService, router, layoutRefService, document) {
        var _this = this;
        this.el = el;
        this.classInitService = classInitService;
        this.menuAsideService = menuAsideService;
        this.layoutConfigService = layoutConfigService;
        this.router = router;
        this.layoutRefService = layoutRefService;
        this.document = document;
        this.classes = 'm-grid__item m-aside-left';
        this.id = 'm_aside_left';
        this.currentRouteUrl = '';
        // subscribe to menu classes update
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            // join the classes array and pass to variable
            _this.classes = 'm-grid__item m-aside-left ' + classes.aside_left.join(' ');
        });
    }
    AsideLeftComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.mMenuAsideOffcanvas = new MenuAsideOffcanvasDirective(_this.el);
            // manually call the directives' lifecycle hook method
            _this.mMenuAsideOffcanvas.ngAfterViewInit();
            // keep aside left element reference
            _this.layoutRefService.addElement('asideLeft', _this.el.nativeElement);
        });
    };
    AsideLeftComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentRouteUrl = this.router.url.split(/[?#]/)[0];
        this.router.events
            .pipe(filter(function (event) { return event instanceof NavigationEnd; }))
            .subscribe(function (event) { return _this.currentRouteUrl = _this.router.url.split(/[?#]/)[0]; });
    };
    AsideLeftComponent.prototype.isMenuItemIsActive = function (item) {
        if (item.submenu) {
            return this.isMenuRootItemIsActive(item);
        }
        if (!item.page) {
            return false;
        }
        // dashboard
        if (item.page !== '/' && this.currentRouteUrl.startsWith(item.page)) {
            return true;
        }
        return this.currentRouteUrl === item.page;
    };
    AsideLeftComponent.prototype.isMenuRootItemIsActive = function (item) {
        var result = false;
        for (var _i = 0, _a = item.submenu; _i < _a.length; _i++) {
            var subItem = _a[_i];
            result = this.isMenuItemIsActive(subItem);
            if (result) {
                return true;
            }
        }
        return false;
    };
    /**
     * Use for fixed left aside menu, to show menu on mouseenter event.
     * @param e Event
     */
    AsideLeftComponent.prototype.mouseEnter = function (e) {
        var _this = this;
        // check if the left aside menu is fixed
        if (this.document.body.classList.contains('m-aside-left--fixed')) {
            if (this.outsideTm) {
                clearTimeout(this.outsideTm);
                this.outsideTm = null;
            }
            this.insideTm = setTimeout(function () {
                // if the left aside menu is minimized
                if (_this.document.body.classList.contains('m-aside-left--minimize') && mUtil.isInResponsiveRange('desktop')) {
                    // show the left aside menu
                    _this.document.body.classList.remove('m-aside-left--minimize');
                    _this.document.body.classList.add('m-aside-left--minimize-hover');
                }
            }, 300);
        }
    };
    /**
     * Use for fixed left aside menu, to show menu on mouseenter event.
     * @param e Event
     */
    AsideLeftComponent.prototype.mouseLeave = function (e) {
        var _this = this;
        if (this.document.body.classList.contains('m-aside-left--fixed')) {
            if (this.insideTm) {
                clearTimeout(this.insideTm);
                this.insideTm = null;
            }
            this.outsideTm = setTimeout(function () {
                // if the left aside menu is expand
                if (_this.document.body.classList.contains('m-aside-left--minimize-hover') && mUtil.isInResponsiveRange('desktop')) {
                    // hide back the left aside menu
                    _this.document.body.classList.remove('m-aside-left--minimize-hover');
                    _this.document.body.classList.add('m-aside-left--minimize');
                }
            }, 500);
        }
    };
    __decorate([
        HostBinding('class')
    ], AsideLeftComponent.prototype, "classes");
    __decorate([
        HostBinding('id')
    ], AsideLeftComponent.prototype, "id");
    __decorate([
        HostBinding('attr.mMenuAsideOffcanvas')
    ], AsideLeftComponent.prototype, "mMenuAsideOffcanvas");
    AsideLeftComponent = __decorate([
        Component({
            selector: 'm-aside-left',
            templateUrl: './aside-left.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        __param(6, Inject(DOCUMENT))
    ], AsideLeftComponent);
    return AsideLeftComponent;
}());
export { AsideLeftComponent };
