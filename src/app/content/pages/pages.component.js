var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NavigationEnd, NavigationStart } from '@angular/router';
import { Component, HostBinding, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import * as objectPath from 'object-path';
import { BehaviorSubject } from 'rxjs';
import { style, animate } from '@angular/animations';
var PagesComponent = /** @class */ (function () {
    function PagesComponent(el, configService, classInitService, router, layoutRefService, animationBuilder, translationService) {
        var _this = this;
        this.el = el;
        this.configService = configService;
        this.classInitService = classInitService;
        this.router = router;
        this.layoutRefService = layoutRefService;
        this.animationBuilder = animationBuilder;
        this.translationService = translationService;
        this.classes = 'm-grid m-grid--hor m-grid--root m-page';
        this.selfLayout = 'blank';
        // class for the header container
        this.pageBodyClass$ = new BehaviorSubject('');
        this.configService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            var pageBodyClass = '';
            _this.selfLayout = objectPath.get(config, 'self.layout');
            if (_this.selfLayout === 'boxed' || _this.selfLayout === 'wide') {
                pageBodyClass += ' m-container m-container--responsive m-container--xxl m-page__container';
            }
            _this.pageBodyClass$.next(pageBodyClass);
            _this.asideLeftDisplay = objectPath.get(config, 'aside.left.display');
            _this.asideRightDisplay = objectPath.get(config, 'aside.right.display');
        });
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            _this.asideLeftCloseClass = objectPath.get(classes, 'aside_left_close');
        });
        // animate page load
        this.router.events.subscribe(function (event) {
            if (event instanceof NavigationStart) {
                if (_this.contenWrapper) {
                    // hide content
                    _this.contenWrapper.nativeElement.style.display = 'none';
                }
            }
            if (event instanceof NavigationEnd) {
                if (_this.contenWrapper) {
                    // show content back
                    _this.contenWrapper.nativeElement.style.display = '';
                    // animate the content
                    _this.animate(_this.contenWrapper.nativeElement);
                }
            }
        });
    }
    PagesComponent.prototype.ngOnInit = function () { };
    PagesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.mContent) {
                // keep content element in the service
                _this.layoutRefService.addElement('content', _this.mContent.nativeElement);
            }
        });
    };
    /**
     * Animate page load
     */
    PagesComponent.prototype.animate = function (element) {
        this.player = this.animationBuilder
            .build([
            style({ opacity: 0, transform: 'translateY(15px)' }),
            animate('500ms ease', style({ opacity: 1, transform: 'translateY(0)' })),
            style({ transform: 'none' }),
        ])
            .create(element);
        this.player.play();
    };
    __decorate([
        HostBinding('class')
    ], PagesComponent.prototype, "classes");
    __decorate([
        Input()
    ], PagesComponent.prototype, "selfLayout");
    __decorate([
        Input()
    ], PagesComponent.prototype, "asideLeftDisplay");
    __decorate([
        Input()
    ], PagesComponent.prototype, "asideRightDisplay");
    __decorate([
        Input()
    ], PagesComponent.prototype, "asideLeftCloseClass");
    __decorate([
        ViewChild('mContentWrapper')
    ], PagesComponent.prototype, "contenWrapper");
    __decorate([
        ViewChild('mContent')
    ], PagesComponent.prototype, "mContent");
    PagesComponent = __decorate([
        Component({
            selector: 'm-pages',
            templateUrl: './pages.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], PagesComponent);
    return PagesComponent;
}());
export { PagesComponent };
