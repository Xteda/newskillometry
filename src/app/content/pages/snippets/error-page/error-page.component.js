var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, Input } from '@angular/core';
var ErrorPageComponent = /** @class */ (function () {
    function ErrorPageComponent(route) {
        this.route = route;
        this.classes = 'm-grid m-grid--hor m-grid--root m-page';
    }
    ErrorPageComponent.prototype.ngOnInit = function () {
        this.errorType = +this.route.snapshot.paramMap.get('type');
        if (!this.errorType) {
            this.errorType = Math.floor((Math.random() * 6) + 1);
        }
    };
    __decorate([
        HostBinding('class')
    ], ErrorPageComponent.prototype, "classes");
    __decorate([
        Input()
    ], ErrorPageComponent.prototype, "errorType");
    ErrorPageComponent = __decorate([
        Component({
            selector: 'm-error-page',
            templateUrl: './error-page.component.html',
            styleUrls: ['./error-page.component.scss']
        })
    ], ErrorPageComponent);
    return ErrorPageComponent;
}());
export { ErrorPageComponent };
