var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { LayoutConfig } from '../../../config/layout';
var BuilderComponent = /** @class */ (function () {
    function BuilderComponent(layoutConfigService, classInitService, layoutConfigStorageService) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.classInitService = classInitService;
        this.layoutConfigStorageService = layoutConfigStorageService;
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (config) {
            _this.model = config.config;
        });
    }
    BuilderComponent.prototype.ngOnInit = function () { };
    BuilderComponent.prototype.submitPreview = function (form) {
        this.layoutConfigService.setModel(new LayoutConfig(this.model));
    };
    BuilderComponent.prototype.resetPreview = function (event) {
        event.preventDefault();
        this.layoutConfigStorageService.resetConfig();
        location.reload();
    };
    __decorate([
        Input()
    ], BuilderComponent.prototype, "model");
    __decorate([
        ViewChild('builderForm')
    ], BuilderComponent.prototype, "form");
    BuilderComponent = __decorate([
        Component({
            selector: 'm-builder',
            templateUrl: './builder.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], BuilderComponent);
    return BuilderComponent;
}());
export { BuilderComponent };
