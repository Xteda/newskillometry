var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../../../partials/partials.module';
import { EntityComponent } from './entity.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// Core => Services
import { CurriculumsService } from './_core/services/curriculums.service';
import { CurriculumCriteriaService } from './_core/services/curriculum-criteria.service';
// Core => Utils
import { HttpUtilsService } from './_core/utils/http-utils.service';
import { TypesUtilsService } from './_core/utils/types-utils.service';
import { LayoutUtilsService } from './_core/utils/layout-utils.service';
// Shared
import { ActionNotificationComponent } from './_shared/action-natification/action-notification.component';
import { DeleteEntityDialogComponent } from './_shared/delete-entity-dialog/delete-entity-dialog.component';
import { FetchEntityDialogComponent } from './_shared/fetch-entity-dialog/fetch-entity-dialog.component';
import { UpdateStatusDialogComponent } from './_shared/update-status-dialog/update-status-dialog.component';
import { AlertComponent } from './_shared/alert/alert.component';
// Curriculums
import { CurriculumsListComponent } from './curriculums/curriculums-list/curriculums-list.component';
import { CurriculumEditComponent } from './curriculums/curriculum-edit/curriculum-edit.component';
import { CriteriaModel } from './_core/models/criteria.model';
import { CriteriaService } from './_core/utils/criteria.service';
import { FileService } from './_core/utils/file.service';
// Courses
import { CourseEditComponent } from './courses/course-edit/course-edit.component';
// Lessons
import { LessonsListComponent } from './lessons/lessons-list/lessons-list.component';
// Material
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule, MatSelectModule, MatMenuModule, MatProgressBarModule, MatButtonModule, MatCheckboxModule, MatDialogModule, MatTabsModule, MatNativeDateModule, MatCardModule, MatRadioModule, MatIconModule, MatDatepickerModule, MatAutocompleteModule, MAT_DIALOG_DEFAULT_OPTIONS, MatSnackBarModule, MatTooltipModule, MatDialogRef, MAT_DIALOG_DATA, MatSlideToggleModule } from '@angular/material';
import { VideoService } from './_core/services/video.service';
import { MarkersService } from './_core/services/markers.service';
import { MarkerService } from './_core/utils/marker.service';
import { CoursesListComponent } from './courses/courses-list/courses-list.component';
import { CoursesService } from './_core/services/courses.service';
import { CriteriaComponent } from './_subs/criteria/criteria-list.component';
import { ResourcesComponent } from './_subs/resources/resources.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { FilesService } from './_core/services/files.service';
import { LessonsService } from './_core/services/lessons.service';
import { LessonEditComponent } from './lessons/lesson-edit/lesson-edit.component';
import { InstructionsListComponent } from './instructions/instructions-list/instructions-list.component';
import { InstructionService } from './_core/services/instruction.service';
import { InstructionEditComponent } from './instructions/instruction-edit/instruction-edit.component';
import { CurriculumEditResolver } from './curriculums/curriculum-edit/curriculum-edit.resolver';
import { DropZoneDirective } from './_subs/resources/drop-zone.directive';
var DEFAULT_DROPZONE_CONFIG = {
    // Change this to your upload POST address:
    url: 'https://httpbin.org/post',
    maxFilesize: 50
};
var routes = [
    {
        path: '',
        component: EntityComponent,
        children: [
            {
                path: '',
                redirectTo: 'curriculums',
                pathMatch: 'full'
            },
            {
                path: 'curriculums',
                component: CurriculumsListComponent
            },
            {
                path: 'curriculums/edit',
                component: CurriculumEditComponent
            },
            {
                path: 'curriculums/edit/:id',
                component: CurriculumEditComponent
            },
            {
                path: 'curriculums/add',
                component: CurriculumEditComponent
            },
            {
                path: 'courses',
                component: CoursesListComponent
            },
            {
                path: 'courses:parentId',
                component: CoursesListComponent
            },
            {
                path: 'course/edit',
                component: CourseEditComponent
            },
            {
                path: 'course/edit/:id',
                component: CourseEditComponent
            },
            {
                path: 'course/add',
                component: CourseEditComponent
            },
            {
                path: 'course/add:parentId',
                component: CourseEditComponent
            },
            {
                path: 'lessons',
                component: LessonsListComponent
            },
            {
                path: 'lessons:parentId',
                component: LessonsListComponent
            },
            {
                path: 'lesson/edit',
                component: LessonEditComponent
            },
            {
                path: 'lesson/edit/:id',
                component: LessonEditComponent
            },
            {
                path: 'lesson/add',
                component: LessonEditComponent
            },
            {
                path: 'instructions',
                component: InstructionsListComponent
            },
            {
                path: 'instructions:parentId',
                component: InstructionsListComponent
            },
            {
                path: 'instruction/edit',
                component: InstructionEditComponent
            },
            {
                path: 'instruction/edit/:id',
                component: InstructionEditComponent
            },
            {
                path: 'instruction/add',
                component: InstructionEditComponent
            },
        ]
    }
];
var EntityModule = /** @class */ (function () {
    function EntityModule() {
    }
    EntityModule = __decorate([
        NgModule({
            imports: [
                MatDialogModule,
                CommonModule,
                HttpClientModule,
                PartialsModule,
                RouterModule.forChild(routes),
                FormsModule,
                ReactiveFormsModule,
                TranslateModule.forChild(),
                MatButtonModule,
                MatMenuModule,
                MatSelectModule,
                MatInputModule,
                MatTableModule,
                MatAutocompleteModule,
                MatRadioModule,
                MatIconModule,
                MatNativeDateModule,
                MatProgressBarModule,
                MatDatepickerModule,
                MatCardModule,
                MatPaginatorModule,
                MatSortModule,
                MatCheckboxModule,
                MatProgressSpinnerModule,
                MatSnackBarModule,
                MatTabsModule,
                MatTooltipModule,
                NgbModule,
                MatSlideToggleModule,
                PerfectScrollbarModule,
                AngularEditorModule,
                DropzoneModule,
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DEFAULT_OPTIONS,
                    useValue: {
                        hasBackdrop: true,
                        panelClass: 'm-mat-dialog-container__wrapper',
                        height: 'auto',
                        width: '900px'
                    }
                },
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: {} },
                {
                    provide: DROPZONE_CONFIG,
                    useValue: DEFAULT_DROPZONE_CONFIG
                },
                HttpUtilsService,
                TypesUtilsService,
                LayoutUtilsService,
                CurriculumsService,
                CurriculumCriteriaService,
                CriteriaModel,
                CriteriaService,
                FileService,
                VideoService,
                MarkersService,
                MarkerService,
                CoursesService,
                FilesService,
                LessonsService,
                InstructionService,
                CurriculumEditResolver
            ],
            entryComponents: [
                ActionNotificationComponent,
                DeleteEntityDialogComponent,
                FetchEntityDialogComponent,
                UpdateStatusDialogComponent,
            ],
            declarations: [
                EntityComponent,
                // Shared
                ActionNotificationComponent,
                DeleteEntityDialogComponent,
                FetchEntityDialogComponent,
                UpdateStatusDialogComponent,
                AlertComponent,
                // Products
                CurriculumEditComponent,
                CurriculumsListComponent,
                CriteriaComponent,
                CoursesListComponent,
                CourseEditComponent,
                ResourcesComponent,
                LessonsListComponent,
                LessonEditComponent,
                InstructionsListComponent,
                InstructionEditComponent,
                DropZoneDirective
            ]
        })
    ], EntityModule);
    return EntityModule;
}());
export { EntityModule };
