import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'entity-grid',
	templateUrl: './entity.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntityComponent implements OnInit {
	constructor() {}

	ngOnInit() {}
}
