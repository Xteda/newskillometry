var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewChild, ChangeDetectionStrategy } from '@angular/core';
// Material
import { MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { fromEvent, merge } from 'rxjs';
import { MessageType } from '../../_core/utils/layout-utils.service';
// Models
import { CourseDatasourse } from '../../_core/models/data-sources/course.datasourse';
import { QueryParamsModel } from '../../_core/models/query-models/query-params.model';
import * as moment from 'moment';
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
var CoursesListComponent = /** @class */ (function () {
    function CoursesListComponent(entityService, router, dialog, route, subheaderService, layoutUtilsService) {
        this.entityService = entityService;
        this.router = router;
        this.dialog = dialog;
        this.route = route;
        this.subheaderService = subheaderService;
        this.layoutUtilsService = layoutUtilsService;
        this.displayedColumns = ['select', 'title', 'author', 'children', 'create', 'update', 'status', 'actions'];
        this.filterStatus = '';
        this.filterCondition = '';
        // Selection
        this.selection = new SelectionModel(true, []);
        this.coursesResult = [];
    }
    /** LOAD DATA */
    CoursesListComponent.prototype.ngOnInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        /* Data load will be triggered in two cases:
        - when a pagination event occurs => this.paginator.page
        - when a sort event occurs => this.sort.sortChange
        **/
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () {
            _this.loadProductsList();
        }))
            .subscribe();
        // Filtration, bind to searchInput
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.paginator.pageIndex = 0;
            _this.loadProductsList();
        }))
            .subscribe();
        // Init DataSource
        this.dataSource = new CourseDatasourse(this.entityService);
        var queryParams = new QueryParamsModel({});
        queryParams.sortOrder = 'desc';
        queryParams.sortField = 'create';
        // Read from URL itemId, for restore previous state
        this.route.queryParams.subscribe(function (params) {
            // Set title to page breadCrumbs
            _this.subheaderService.setTitle('Courses');
            if (params.parentId) {
                _this.parentId = params.parentId;
                _this.dataSource.loadProducts(queryParams, params.parentId);
                _this.subheaderService.setBreadcrumbs([
                    { title: 'Curriculums', page: '/educational-center/curriculums' },
                    { title: 'Courses', page: "/educational-center/courses", queryParams: { parentId: params.parentId } }
                ]);
            }
            else {
                // First load
                _this.dataSource.loadProducts(queryParams);
                _this.subheaderService.setBreadcrumbs([
                    { title: 'Curriculums', page: '/educational-center/curriculums' },
                    { title: 'Courses', page: "/educational-center/courses" }
                ]);
            }
        });
        this.dataSource.entitySubject.subscribe(function (res) { return _this.coursesResult = res; });
    };
    CoursesListComponent.prototype.loadProductsList = function () {
        var queryParams = new QueryParamsModel(this.filterConfiguration(), this.sort.direction = 'desc', this.sort.active = '_createdDate', this.paginator.pageIndex, this.paginator.pageSize);
        this.dataSource.loadProducts(queryParams);
    };
    /** FILTRATION */
    CoursesListComponent.prototype.filterConfiguration = function () {
        var filter = {};
        var searchText = this.searchInput.nativeElement.value;
        if (this.filterStatus && this.filterStatus.length > 0) {
            filter.status = +this.filterStatus;
        }
        if (this.filterCondition && this.filterCondition.length > 0) {
            filter.condition = +this.filterCondition;
        }
        filter.author = searchText;
        filter.title = searchText;
        return filter;
    };
    CoursesListComponent.prototype.restoreState = function (queryParams, parentId) {
        console.log('restoreState', parentId);
        if (parentId > 0) {
            this.entityService.getCoursesByParentId(parentId).subscribe(function (res) {
                console.log('res', res);
            });
        }
        if (!queryParams.filter) {
            return;
        }
        if ('condition' in queryParams.filter) {
            this.filterCondition = queryParams.filter.condition.toString();
        }
        if ('status' in queryParams.filter) {
            this.filterStatus = queryParams.filter.status.toString();
        }
        if (queryParams.filter.model) {
            this.searchInput.nativeElement.value = queryParams.filter.model;
        }
    };
    /** ACTIONS */
    /** Delete */
    CoursesListComponent.prototype.deleteProduct = function (event, _item) {
        var _this = this;
        event.stopPropagation();
        var _title = 'Course Delete';
        var _description = 'Are you sure to permanently delete this course?';
        var _waitDesciption = 'Course is deleting...';
        var _deleteMessage = "Course has been deleted";
        var dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(function (res) {
            if (!res) {
                return;
            }
            _this.entityService.deleteCourse(_item.id).subscribe(function () {
                _this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
                _this.loadProductsList();
            });
        });
    };
    CoursesListComponent.prototype.deleteProducts = function () {
        var _this = this;
        var _title = 'Courses Delete';
        var _description = 'Are you sure to permanently delete selected courses?';
        var _waitDesciption = 'Courses are deleting...';
        var _deleteMessage = 'Selected courses have been deleted';
        var dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(function (res) {
            if (!res) {
                return;
            }
            var idsForDeletion = [];
            for (var i = 0; i < _this.selection.selected.length; i++) {
                idsForDeletion.push(_this.selection.selected[i].id);
            }
            _this.entityService.deleteCourses(idsForDeletion).subscribe(function () {
                _this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
                _this.loadProductsList();
                _this.selection.clear();
            });
        });
    };
    /** Update Product */
    CoursesListComponent.prototype.updateStatusForProducts = function () {
        var _this = this;
        var _title = 'Update status for selected courses';
        var _updateMessage = 'Status has been updated for selected Courses';
        var _statuses = [{ value: 1, text: 'Active' }, { value: 0, text: 'Closed' }];
        var _messages = [];
        this.selection.selected.forEach(function (elem) {
            _messages.push({
                text: elem.author + ", " + elem.title,
                id: 'test id',
                status: elem.status,
                statusTitle: _this.getItemStatusString(elem.status),
                statusCssClass: _this.getItemCssClassByStatus(elem.status)
            });
        });
        var dialogRef = this.layoutUtilsService.updateStatusForCustomers(_title, _statuses, _messages);
        dialogRef.afterClosed().subscribe(function (res) {
            if (!res) {
                _this.selection.clear();
                return;
            }
            _this.entityService.updateStatusForCourse(_this.selection.selected, +res).subscribe(function () {
                _this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update);
                _this.loadProductsList();
                _this.selection.clear();
            });
        });
    };
    /** SELECTION */
    CoursesListComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.coursesResult.length;
        return numSelected === numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    CoursesListComponent.prototype.masterToggle = function () {
        var _this = this;
        if (this.isAllSelected()) {
            this.selection.clear();
        }
        else {
            this.coursesResult.forEach(function (row) { return _this.selection.select(row); });
        }
    };
    /* UI */
    CoursesListComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = 0; }
        return status ? 'Active' : 'Closed';
    };
    CoursesListComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = 0; }
        return status ? 'success' : 'metal';
    };
    CoursesListComponent.prototype.getNewCssClassByStatus = function (x) {
        return moment(new Date()).diff(x, 'days') > 1;
    };
    CoursesListComponent.prototype.getChildrenEntities = function (id) {
        this.router.navigateByUrl('educational-center/lessons?parentId=' + id);
    };
    CoursesListComponent.prototype.timeAgo = function (x) {
        return moment(x).fromNow();
    };
    __decorate([
        ViewChild(MatPaginator)
    ], CoursesListComponent.prototype, "paginator");
    __decorate([
        ViewChild(MatSort)
    ], CoursesListComponent.prototype, "sort");
    __decorate([
        ViewChild('searchInput')
    ], CoursesListComponent.prototype, "searchInput");
    CoursesListComponent = __decorate([
        Component({
            selector: 'm-courses-list',
            templateUrl: './courses-list.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], CoursesListComponent);
    return CoursesListComponent;
}());
export { CoursesListComponent };
