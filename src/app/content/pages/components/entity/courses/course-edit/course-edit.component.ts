import {Component, OnInit, ChangeDetectionStrategy, ViewChild, OnDestroy} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, BehaviorSubject } from 'rxjs';
import { TypesUtilsService } from '../../_core/utils/types-utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../_core/utils/layout-utils.service';
import { CurriculumCriteriaService} from '../../_core/services/curriculum-criteria.service';
import { CriteriaModel } from '../../_core/models/criteria.model';
import { VideoModel } from '../../_core/models/video.model';
import { VideoService } from '../../_core/services/video.service';
import { MarkersService } from '../../_core/services/markers.service';
import { MarkerModel } from '../../_core/models/marker.model';
import { MarkerService} from '../../_core/utils/marker.service';
import { PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { RecordVideoComponent} from '../../../../../partials/content/general/record-video/record-video.component';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as $ from 'jquery';
import { CurriculumsService} from '../../_core/services/curriculums.service';
import {CriteriaService} from '../../_core/utils/criteria.service';
import {CourseModel} from '../../_core/models/course.model';
import {CoursesService} from '../../_core/services/courses.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FileModel } from '../../_core/models/file.model';
import { FileService } from '../../_core/utils/file.service';
import { FilesService } from '../../_core/services/files.service';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';


@Component({
	selector: 'm-course-edit',
	templateUrl: './course-edit.component.html'
	// changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseEditComponent implements OnInit, OnDestroy {
	entityId;
	parentId;
	parentEntityArray: Array<any> = [];

	fakeStatus: boolean = false;

	course: CourseModel;

	criteria: CriteriaModel;

	videoJSplayer;
	video: VideoModel;
	markers: MarkerModel[] = [];
	oldMarkers: MarkerModel[] = [];
	markersGroup;

	files: FileModel[];
	oldFiles: FileModel[];

	selectedIndex;
	previousTab;
	selectedTab: number = 0;

	loadingSubject = new BehaviorSubject<boolean>(false);
	loading$ = this.loadingSubject.asObservable();

	courseForm: FormGroup;
	hasFormErrors: boolean = false;

	htmlContent = '';

	config: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		height: '15rem',
		minHeight: '5rem',
		placeholder: 'Enter text here...',
		translate: 'yes'
	};

	@ViewChild('entityTabs') tabGroup;
	@ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;
	@ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;

	constructor(private activatedRoute: ActivatedRoute,
				private router: Router,
				private curriculumService: CurriculumsService,
				private storage: AngularFireStorage,

				private criteriaInnerService: CriteriaService,
				private criteriaOuterService: CurriculumCriteriaService,

				private fileInnerService: FileService,
				private fileOuterService: FilesService,

				private courseService: CoursesService,
				private typesUtilsService: TypesUtilsService,

				private videoService: VideoService,
				private markersService: MarkersService,
				private markerService: MarkerService,

				private courseFB: FormBuilder,
				public dialog: MatDialog,
				private subheaderService: SubheaderService,
				private layoutUtilsService: LayoutUtilsService) { }

	ngOnInit() {
		this.loadingSubject.next(true);
		this.activatedRoute.queryParams.subscribe(params => {
			this.entityId = +params.id;
			this.curriculumService.getAllCurriculums().subscribe(res => {
				for (let i = 0; i < res.length; i++) {
					const tempObj = {
						value: res[i].id,
						title: res[i].title
					};
					this.parentEntityArray.push(tempObj);
				}
			});
			if (this.entityId && this.entityId > 0) {
				this.courseService.getCourseById(this.entityId).subscribe(res => {
					window.dispatchEvent(new Event('resize'));
					this.criteria = res.criteria[0];
					this.video = Object.assign({}, res.videos[0]);
					this.markers = res.markers;
					this.files = res.files;

					delete res['criteria'];
					delete res['videos'];
					delete res['markers'];
					delete res['files'];

					this.course = res;
					this.fakeStatus = res.status;
					this.course._prevState = Object.assign({}, res);
					this.initCourse();

					if (this.criteria) {
						this.criteriaInnerService.setSavedCriteria(this.criteria);
						this.criteria._prevState = Object.assign({}, this.criteria);
					} else {
						const newCriteria = new CriteriaModel();
						this.criteria = newCriteria;
						this.criteriaInnerService.setSavedCriteria(this.criteria);
						this.criteria._prevState = Object.assign({}, newCriteria);
					}

					if (this.video) {
						this.video._prevState = Object.assign({}, this.video);
						if (this.markers) {
							this.markers = _.orderBy(this.markers, ['time'], ['asc']);
							this.oldMarkers = this.markers.map(x => Object.assign({}, x));
							this.markersGroup = this.courseFB.array(this.getMarkersFields().map(marker => this.courseFB.group(marker)));
							this.courseForm.addControl('markers', this.markersGroup);
						}
						setTimeout(() => {
							this.initVideo();
						}, 0);
					} else {
						const newVideo = new VideoModel();
						this.video = newVideo;
						this.video._prevState = Object.assign({}, this.video);
					}

					if (this.files) {
						this.fileInnerService.setSavedFiles(this.files);
						this.oldFiles = Object.assign({}, this.files);
					} else {
						const newFiles = new Array<FileModel>();
						this.files = newFiles;
						this.fileInnerService.setSavedFiles(this.files);
						this.oldFiles = Object.assign({}, newFiles);
					}
				});

			} else {
				const newProduct = new CourseModel();
				const newCriteria = new CriteriaModel();
				const newVideo = new VideoModel();
				const newMarkers = [];
				const newFiles = new Array<FileModel>();
				// newProduct.clear();
				newCriteria.clear(this.entityId);
				newVideo.clear(this.entityId);

				this.course = newProduct;
				this.course._prevState = Object.assign({}, newProduct);

				this.criteria = newCriteria;
				this.criteriaInnerService.setSavedCriteria(this.criteria);
				this.criteria._prevState = Object.assign({}, newCriteria);

				this.video = newVideo;
				this.video._prevState = Object.assign({}, newVideo);

				this.markers = newMarkers;
				this.oldMarkers = this.markers.map(x => Object.assign({}, x));

				this.files = newFiles;
				this.fileInnerService.setSavedFiles(this.files);
				this.oldFiles = Object.assign({}, newFiles);

				this.initCourse();
			}
		});
	}

	ngOnDestroy() {
		this.destroyEvent();
	}

	initCourse() {
		this.createForm();
		this.loadingSubject.next(false);
		if (!this.course.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: 'Courses',  page: '/educational-center/courses' },
				{ title: 'Create course', page: '/educational-center/course/add' }
			]);
			return;
		}
		this.subheaderService.setTitle('Edit course');
		this.subheaderService.setBreadcrumbs([
			{ title: 'Courses',  page: '/educational-center/courses' },
			{ title: 'Edit course', page: '/educational-center/course/edit', queryParams: { id: this.course.id } }
		]);
	}

	createForm() {
		const tempObj = _.find(this.parentEntityArray, (o) =>  o.value == this.course.curriculumId) || '';
		const selectValue = tempObj ? tempObj.value.toString() : '';
		this.courseForm = this.courseFB.group({
			title: [this.course.title, Validators.required],
			description: [this.course.description],
			selectedParent: [selectValue, Validators.required]
		});
	}

	private getMarkersFields() {
		const markersControlArray = [];
		for (let i = 0; i < this.markers.length; i++) {
			markersControlArray.push({
				markerTitle: [this.markers[i].text, Validators.required],
				markerDescription: [this.markers[i].overlayText, Validators.required]
			});
		}
		return markersControlArray;
	}

	goBack(id = 0) {
		let _backUrl = 'educational-center/courses';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
	}

	refreshProduct(id = 0) {
		const _refreshUrl = 'educational-center/course/edit?id=' + id;
		this.router.navigateByUrl(_refreshUrl);
	}

	reset() {
		const tempCurriculum = this.course._prevState;
		const tempCriteria = this.criteria._prevState;
		const tempVideo = this.video._prevState;
		const tempFiles = this.oldFiles;
		const hasVideo = this.course.hasVideo;

		this.course = Object.assign({}, this.course._prevState);
		this.course._prevState = Object.assign({}, tempCurriculum);
		this.fakeStatus = !!this.course.status;
		this.createForm();
		if (this.oldMarkers.length) {
			this.markers = this.oldMarkers.map(x => Object.assign({}, x));
			this.markersGroup = this.courseFB.array(this.getMarkersFields().map(marker => this.courseFB.group(marker)));
			this.courseForm.addControl('markers', this.markersGroup);
			if (hasVideo && this.tabGroup.selectedIndex === 0) {
				console.log(this.tabGroup.selectedIndex);
				this.videoJSplayer.markers.reset(this.markers);
			}
		}
		if (!hasVideo && tempCurriculum.hasVideo) {
			this.video = Object.assign({}, tempVideo);
			this.video._prevState = Object.assign({}, tempVideo);
			setTimeout(() => {
				if (this.tabGroup.selectedIndex === 0) {
					this.initVideo();
					if (this.markers) {
						console.log(this.tabGroup.selectedIndex);
						this.videoJSplayer.markers.reset(this.markers);
					}
				}
			});
		}
		if (tempFiles.length) {
			this.files = tempFiles.map(x => Object.assign({}, x));
		}
		if (tempCriteria) {
			this.criteria = Object.assign({}, tempCriteria);
			this.criteria._prevState = Object.assign({}, tempCriteria);
			this.criteriaInnerService.setSavedCriteria(this.criteria);

		}
		this.hasFormErrors = false;
		this.courseForm.markAsPristine();
		this.courseForm.markAsUntouched();
		this.courseForm.updateValueAndValidity();
	}

	onSumbit(withBack: boolean = false) {
		this.hasFormErrors = false;
		const controls = this.courseForm.controls;
		/** check form */
		if (this.courseForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		// tslint:disable-next-line:prefer-const
		let editedProduct = this.prepareProduct();

		if (editedProduct.id > 0) {
			this.updateProduct(editedProduct, withBack);
			return;
		}
		this.addCourse(editedProduct, withBack);
	}

	prepareProduct(): CourseModel {
		const controls = this.courseForm.controls;
		const _product = new CourseModel();
		_product.id = this.course.id;
		_product.title = controls['title'].value;
		_product.author = this.course.author || 'John Smith';
		_product.description = controls['description'].value;
		_product.status = this.fakeStatus ? 1 : 0;
		_product.curriculumId = controls['selectedParent'].value;
		_product._userId = 1; // TODO: get version from userId
		_product.children = this.course.children || 0;
		_product._createdDate = this.course._createdDate;
		_product._updatedDate = this.course._updatedDate;
		_product.hasVideo = !!this.video.videoUrl;

		_product._updatedDate = Date.now();
		_product._createdDate = this.course.id > 0 ? _product._createdDate : _product._updatedDate;
		_product._isNew = this.course.id <= 0;
		_product._isUpdated = this.course.id > 0;

		return _product;
	}

	prepareCriteria(entityId?): CriteriaModel {
		const _criteria = new CriteriaModel();
		const savedCriteria =  this.criteriaInnerService.getSavedCriteria();
		_criteria.id = savedCriteria.id;
		_criteria.courseId = this.entityId || entityId;
		_criteria.competencyNumCorrect = +savedCriteria.competencyNumCorrect;
		_criteria.competencyTrialLimit = +savedCriteria.competencyTrialLimit;
		_criteria.competencyTrialNumInput = +savedCriteria.competencyTrialNumInput;
		_criteria.maintenanceEnable = savedCriteria.maintenanceEnable;
		_criteria.maintenancePicker = savedCriteria.maintenancePicker;
		_criteria.maintenanceNumCorrect = +savedCriteria.maintenanceNumCorrect;
		_criteria.maintenanceTrialLimit = +savedCriteria.maintenanceTrialLimit;
		_criteria.maintenanceTrialNumInput = +savedCriteria.maintenanceTrialNumInput;

		_criteria._updatedDate =  Date.now();
		_criteria._createdDate = this.criteria.id > 0 ? _criteria._createdDate : _criteria._updatedDate;
		_criteria._isNew = this.criteria.id <= 0;
		_criteria._isUpdated = this.criteria.id > 0;

		return _criteria;
	}

	prepareVideo(entityId?): VideoModel {
		const _video = new VideoModel();
		_video.id = this.video.id;
		_video.courseId = this.entityId || entityId;
		_video.poster = this.video.poster;
		_video.videoUrl = this.video. videoUrl;
		_video._userId = 1; // TODO: get version from userId
		_video._updatedDate =  Date.now();
		_video._createdDate = this.video.id > 0 ? this.video._createdDate : _video._updatedDate;
		_video._isUpdated = this.video._isUpdated;
		_video._isDeleted = this.video._isDeleted;
		_video._isNew = this.video.id <= 0;

		return _video;
	}

	prepareMarkers(entityId?): MarkerModel[] {
		const _markers = [];
		for (let i = 0; i < this.markers.length; i++) {
			const _marker = new MarkerModel();
			_marker.id = this.markers[i].id;
			_marker.courseId = this.markers[i].courseId || entityId;
			_marker.time = this.markers[i].time;
			_marker.overlayText = this.markers[i].overlayText;
			_marker.text = this.markers[i].text;
			_marker.displayTime = this.markers[i].displayTime;
			_marker._isEditMode = this.markers[i]._isEditMode;
			_marker._isUpdated = this.markers[i]._isUpdated;
			_marker._isDeleted = this.markers[i]._isDeleted;
			_marker._updatedDate = Date.now();
			_marker._createdDate = this.markers[i]._createdDate ? this.markers[i]._createdDate : this.markers[i]._updatedDate;
			_marker._isNew = this.markers[i]._isNew;
			_markers.push(_marker);
		}
		return _markers;
	}

	prepareFiles(entityId?) {
		const _files = [];
		const savedFiles = this.fileInnerService.getSavedFiles();
		for (let i = 0; i < savedFiles.length; i++) {
			const _file = new FileModel();
			_file.id = savedFiles[i].id;
			_file.courseId = savedFiles[i].curriculumId || entityId;
			_file.type = savedFiles[i].type;
			_file.size = savedFiles[i].size;
			_file.title = savedFiles[i].title;
			_file.link = savedFiles[i].link;
			_file._updatedDate = Date.now();
			_file._isUpdated = savedFiles[i]._isUpdated;
			_file._isDeleted = savedFiles[i]._isDeleted;
			_file._createdDate = savedFiles[i]._createdDate ? savedFiles[i]._createdDate : savedFiles[i]._updatedDate;
			_file._isNew = savedFiles[i]._isNew;
			_files.push(_file);
		}
		return _files;
	}

	addCourse(_product: CourseModel, withBack: boolean = false) {
		this.loadingSubject.next(true);

		this.courseService.createCourse(_product).subscribe(res => {
			const criteria = this.prepareCriteria(res.id);
			const video = this.prepareVideo(res.id);

			forkJoin<CriteriaModel, VideoModel>([this.criteriaOuterService.createCriteria(criteria),
														 this.videoService.createVideo(video)]).subscribe(([criteria, video]) => {
				this.loadingSubject.next(false);
				if (withBack) {
					this.goBack(res.id);
				} else {
					const message = `New course successfully has been added.`;
					this.layoutUtilsService.showActionNotification(message, MessageType.Create, 10000, true, false);
					this.refreshProduct(res.id);
				}
			});

		});
	}

	updateProduct(_product: CourseModel, withBack: boolean = false) {
		this.loadingSubject.next(true);
		// Update Product
		// tslint:disable-next-line:prefer-const
		let tasks$ = [this.courseService.updateCourse(_product)];

		// Update Criteria
		const criteria = this.prepareCriteria(this.entityId);
		tasks$.push(this.criteriaOuterService.updateCriteria(criteria));

		// Update video
		if (this.course.hasVideo || this.course._prevState.hasVideo) {
			const video = this.prepareVideo(this.entityId);
			if (video._isDeleted) {
				this.storage.storage.refFromURL(this.video._prevState.videoUrl).delete();
				tasks$.push(this.videoService.deleteVideo(video));
			} else if (video.id) {
				tasks$.push(this.videoService.updateVideo(video));
			} else {
				tasks$.push(this.videoService.createVideo(video));
			}
			// Update markers
			const markers = this.prepareMarkers(this.entityId);
			if (markers.length > 0) {
				const deletedMarkers = [];
				const updatedMarkers = [];
				for (let i = 0; i < markers.length; i++) {
					if (markers[i]._isDeleted) {
						deletedMarkers.push(markers[i]);
						continue;
					}
					if (markers[i]._isNew || markers[i]._isUpdated) {
						updatedMarkers.push(markers[i]);
					}
				}
				if (deletedMarkers.length > 0) {
					for (let i = 0; i < deletedMarkers.length; i++) {
						const _marker = deletedMarkers[i];
						tasks$.push(this.markersService.deleteMarker(_marker));
					}
				}
				if (updatedMarkers.length > 0) {
					for (let i = 0; i < updatedMarkers.length; i++) {
						const _marker = updatedMarkers[i];
						if (_marker.id) {
							tasks$.push(this.markersService.updateMarker(_marker));
						} else {
							tasks$.push(this.markersService.createMarker(_marker));
						}
					}
				}
			}
		}
		const files = this.prepareFiles(this.entityId);
		if (files.length > 0) {
			const deletedFiles = [];
			const updatedFiles = [];
			for (let i = 0; i < files.length; i++) {
				if (files[i]._isDeleted && typeof files[i].id != 'undefined') {
					deletedFiles.push(files[i]);
					continue;
				}
				if (files[i]._isNew) {
					updatedFiles.push(files[i]);
				}
			}
			if (deletedFiles.length > 0) {
				for (let i = 0; i < deletedFiles.length; i++) {
					const _files = deletedFiles[i];
					tasks$.push(this.fileOuterService.deleteFile(_files));
				}
			}
			if (updatedFiles.length > 0) {
				for (let i = 0; i < updatedFiles.length; i++) {
					const _file = updatedFiles[i];
					if (_file.id) {
						tasks$.push(this.fileOuterService.updateFile(_file));
					} else {
						tasks$.push(this.fileOuterService.createFile(_file));
					}
				}
			}
		}

		forkJoin(tasks$).subscribe(res => {
			this.loadingSubject.next(false);
			if (withBack) {
				this.goBack(_product.id);
			} else {
				const message = `Course successfully has been saved.`;
				this.layoutUtilsService.showActionNotification(message, MessageType.Update, 10000, true, false);
				this.refreshProduct(_product.id);
			}
		});
	}

	getComponentTitle() {
		let result = 'Create course';
		if (!this.course || !this.course.id) {
			return result;
		}

		result = `Edit course - "${this.course.title}" by ${this.course.author}`;
		return result;
	}

	onAlertClose($event) {
		this.hasFormErrors = false;
	}

	tabChange() {
		this.previousTab = this.selectedIndex;
		this.selectedIndex = this.tabGroup.selectedIndex;
		this.criteria = this.criteriaInnerService.getSavedCriteria();
		if (this.tabGroup.selectedIndex === 0) {
			this.initVideo();
		}
	}

	public destroyEvent() {
		if (this.video.videoUrl) {
			const videoArray = videojs.getPlayers();
			for (const videoId in videoArray) {
				if (videoArray.hasOwnProperty(videoId)) {
					videojs(videoId).dispose();
					delete videoArray[videoId];
				}
			}
		}
	}

	public deleteVideo(addNew?) {
		this.videoJSplayer.markers.removeAll();
		for (let i = 0; i < this.markers.length; i++) {
			this.markers[i]._isDeleted = true;
		}
		this.courseForm.removeControl('markers');
		this.destroyEvent();
		this.video.videoUrl = '';
		this.video.poster = '';
		this.video._isDeleted = true;
		this.course.hasVideo = false;
		if (addNew) {
			this.addVideo();
		}
	}

	private initVideo() {
		if (this.video.videoUrl) {
			const scrollService = this.componentRef;
			this.videoJSplayer = videojs('video', {
				fluid: true
			});
			const markerElArray = document.getElementsByClassName('marker');
			this.videoJSplayer.markers({
				breakOverlay: {
					display: true,
				},
				onMarkerReached: function (marker, index) {
					for (let i = 0; i < markerElArray.length; i++) {
						markerElArray[i].classList.remove('bg-success', 'text-white');
					}
					markerElArray[index].classList.add('bg-success', 'text-white');
					scrollService.directiveRef.scrollTo(0, document.getElementById('marker-' + index).offsetTop - 3, 500);
				},
				onMarkerLeft: function (marker, index) {
					markerElArray[index].classList.remove('bg-success', 'text-white');
				},
				markers: _.filter(this.markers, function(o) { return !o._isDeleted; })
			});
		}
	}

	public timeMarkerAction(mIndex, mTime, mStart?) {
		this.markerService.setCurrentTime(this.videoJSplayer, this.markers, mIndex, mTime, mStart);
	}

	public mEdit(mIndex) {
		this.markerService.markerEdit(this.videoJSplayer, this.markers, mIndex, this.courseForm);
	}

	public mDelete(mIndex) {
		this.markerService.markerDelete(this.videoJSplayer, this.markers, mIndex);
	}

	public checkMarkers(markersArray) {
		for (let i = 0; i < markersArray.length; i++) {
			if (markersArray[i]._isEditMode) {
				return true;
			}
		}
		return false;
	}

	public checkDeletedMarkers() {
		for (let i = 0; i < this.markers.length; i++) {
			if (!this.markers[i]._isDeleted) {
				return true;
			}
		}
		return false;
	}

	public convertTime(sec) {
		return moment.utc(sec * 1000).format('HH:mm:ss');
	}

	public mAdd() {
		const success = this.markerService.addMarker(this.videoJSplayer, this.markers);
		if (success) {
			this.markers = _.orderBy(this.markers, ['time'], ['asc']);
			this.courseForm.removeControl('markers');
			this.markersGroup = this.courseFB.array(this.getMarkersFields().map(marker => this.courseFB.group(marker)));
			this.courseForm.addControl('markers', this.markersGroup);
			window.dispatchEvent(new Event('resize'));
			setTimeout(() => {
				let mIndex;
				mIndex = _.findIndex(this.markers, {'_isEditMode': true});
				this.componentRef.directiveRef.scrollTo(0, document.getElementById('marker-' + mIndex).offsetTop - 3, 500);
			}, 120);
		}
	}

	public mRemoveAll() {
		for (let i = 0; i < this.markers.length; i++) {
			this.markers[i]._isDeleted = true;
		}
		this.courseForm.removeControl('markers');
		this.videoJSplayer.markers.removeAll();
		this.layoutUtilsService.showActionNotification('All Annotation have been deleted', MessageType.Create, 10000, true, false);
	}

	addVideo() {
		const dialogRef = this.dialog.open(RecordVideoComponent, {
			width: '800px',
			id: 'videoModal',
			disableClose: true
		});

		dialogRef.afterClosed().subscribe((result) => {
			if (result) {
				this.video.videoUrl = result.videoUrl;
				this.course.hasVideo = true;
				window.dispatchEvent(new Event('resize'));
				setTimeout(() => {
					this.initVideo();
				}, 50);
			}
		});
	}

	public changeStatus(status) {
		this.fakeStatus = !this.fakeStatus;
	}
}
