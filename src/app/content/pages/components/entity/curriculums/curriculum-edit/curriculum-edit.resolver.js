var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
var CurriculumEditResolver = /** @class */ (function () {
    function CurriculumEditResolver(curService) {
        this.curService = curService;
    }
    CurriculumEditResolver.prototype.resolve = function (route) {
        return this.curService.getCurriculumById(+route.paramMap.get('id'))["catch"](function () {
            return Observable.of('data not available at this time');
        });
    };
    CurriculumEditResolver = __decorate([
        Injectable()
    ], CurriculumEditResolver);
    return CurriculumEditResolver;
}());
export { CurriculumEditResolver };
