import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { CurriculumsService} from '../../_core/services/curriculums.service';

@Injectable()
export class CurriculumEditResolver implements Resolve<Observable<string>> {
	constructor(private curService: CurriculumsService) {}

	resolve(route: ActivatedRouteSnapshot) {
		return this.curService.getCurriculumById(+route.paramMap.get('id')).catch(() => {
			return Observable.of('data not available at this time');
		});
	}
}
