var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewChild, ChangeDetectionStrategy } from '@angular/core';
// Material
import { MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { fromEvent, merge } from 'rxjs';
import { MessageType } from '../../_core/utils/layout-utils.service';
// Models
import { CurriculumDatasource } from '../../_core/models/data-sources/curriculum.datasource';
import { QueryParamsModel } from '../../_core/models/query-models/query-params.model';
import * as moment from 'moment';
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
var CurriculumsListComponent = /** @class */ (function () {
    function CurriculumsListComponent(entityService, router, dialog, route, subheaderService, layoutUtilsService) {
        this.entityService = entityService;
        this.router = router;
        this.dialog = dialog;
        this.route = route;
        this.subheaderService = subheaderService;
        this.layoutUtilsService = layoutUtilsService;
        this.displayedColumns = ['select', 'title', 'author', 'children', 'create', 'update', 'status', 'actions'];
        this.filterStatus = '';
        this.filterCondition = '';
        // Selection
        this.selection = new SelectionModel(true, []);
        this.curriculumsResult = [];
    }
    /** LOAD DATA */
    CurriculumsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        /* Data load will be triggered in two cases:
        - when a pagination event occurs => this.paginator.page
        - when a sort event occurs => this.sort.sortChange
        **/
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () {
            _this.loadCurriculumsList();
        }))
            .subscribe();
        // Filtration, bind to searchInput
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.paginator.pageIndex = 0;
            _this.loadCurriculumsList();
        }))
            .subscribe();
        // Set title to page breadCrumbs
        this.subheaderService.setTitle('Curriculums');
        // Init DataSource
        this.dataSource = new CurriculumDatasource(this.entityService);
        var queryParams = new QueryParamsModel({});
        queryParams.sortOrder = 'desc';
        queryParams.sortField = 'create';
        // Read from URL itemId, for restore previous state
        this.route.queryParams.subscribe(function (params) {
            if (params.id) {
                queryParams = _this.entityService.lastFilter$.getValue();
                _this.restoreState(queryParams, +params.id);
            }
            // First load
            _this.dataSource.loadProducts(queryParams);
        });
        this.dataSource.entitySubject.subscribe(function (res) { return _this.curriculumsResult = res; });
    };
    CurriculumsListComponent.prototype.loadCurriculumsList = function () {
        var queryParams = new QueryParamsModel(this.filterConfiguration(), this.sort.direction = 'desc', this.sort.active = '_createdDate', this.paginator.pageIndex, this.paginator.pageSize);
        this.dataSource.loadProducts(queryParams);
    };
    /** FILTRATION */
    CurriculumsListComponent.prototype.filterConfiguration = function () {
        var filter = {};
        var searchText = this.searchInput.nativeElement.value;
        if (this.filterStatus && this.filterStatus.length > 0) {
            filter.status = +this.filterStatus;
        }
        if (this.filterCondition && this.filterCondition.length > 0) {
            filter.condition = +this.filterCondition;
        }
        filter.author = searchText;
        filter.title = searchText;
        return filter;
    };
    CurriculumsListComponent.prototype.restoreState = function (queryParams, id) {
        var _this = this;
        console.log('restoreState', id);
        if (id > 0) {
            this.entityService.getCurriculumById(id).subscribe(function (res) {
                console.log('res', res);
                var message = res._createdDate === res._updatedDate ?
                    "New curriculum successfully has been added." :
                    "Curriculum successfully has been saved.";
                _this.layoutUtilsService.showActionNotification(message, res._isNew ? MessageType.Create : MessageType.Update, 10000, true, false);
            });
        }
        if (!queryParams.filter) {
            return;
        }
        if ('create' in queryParams.filter) {
            this.filterCondition = queryParams.filter.condition.toString();
        }
        if ('status' in queryParams.filter) {
            this.filterStatus = queryParams.filter.status.toString();
        }
        if (queryParams.filter.model) {
            this.searchInput.nativeElement.value = queryParams.filter.model;
        }
    };
    /** ACTIONS */
    /** Delete */
    CurriculumsListComponent.prototype.deleteCurriculum = function (event, _item) {
        var _this = this;
        event.stopPropagation();
        var _title = 'Curriculum Delete';
        var _description = 'Are you sure to permanently delete this curriculum?';
        var _waitDesciption = 'Curriculum is deleting...';
        var _deleteMessage = "Curriculum has been deleted";
        var dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(function (res) {
            if (!res) {
                return;
            }
            _this.entityService.deleteCurriculum(_item.id).subscribe(function () {
                _this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
                _this.loadCurriculumsList();
            });
        });
    };
    CurriculumsListComponent.prototype.deleteCurriculums = function () {
        var _this = this;
        var _title = 'Curriculums Delete';
        var _description = 'Are you sure to permanently delete selected curriculums?';
        var _waitDesciption = 'Curriculums are deleting...';
        var _deleteMessage = 'Selected curriculums have been deleted';
        var dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(function (res) {
            if (!res) {
                return;
            }
            var idsForDeletion = [];
            for (var i = 0; i < _this.selection.selected.length; i++) {
                idsForDeletion.push(_this.selection.selected[i].id);
            }
            _this.entityService.deleteProducts(idsForDeletion).subscribe(function () {
                _this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
                _this.loadCurriculumsList();
                _this.selection.clear();
            });
        });
    };
    /** Update Product */
    CurriculumsListComponent.prototype.updateStatusForProducts = function () {
        var _this = this;
        var _title = 'Update status for selected curriculums';
        var _updateMessage = 'Status has been updated for selected curriculums';
        var _statuses = [{ value: 1, text: 'Active' }, { value: 0, text: 'Closed' }];
        var _messages = [];
        this.selection.selected.forEach(function (elem) {
            _messages.push({
                text: elem.author + ", " + elem.title + " ",
                id: elem.id,
                status: elem.status,
                statusTitle: _this.getItemStatusString(elem.status),
                statusCssClass: _this.getItemCssClassByStatus(elem.status)
            });
        });
        var dialogRef = this.layoutUtilsService.updateStatusForCustomers(_title, _statuses, _messages);
        dialogRef.afterClosed().subscribe(function (res) {
            if (!res) {
                _this.selection.clear();
                return;
            }
            _this.entityService.updateStatusForCurriculum(_this.selection.selected, +res).subscribe(function () {
                _this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update);
                _this.loadCurriculumsList();
                _this.selection.clear();
            });
        });
    };
    /** SELECTION */
    CurriculumsListComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.curriculumsResult.length;
        return numSelected === numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    CurriculumsListComponent.prototype.masterToggle = function () {
        var _this = this;
        if (this.isAllSelected()) {
            this.selection.clear();
        }
        else {
            this.curriculumsResult.forEach(function (row) { return _this.selection.select(row); });
        }
    };
    /* UI */
    CurriculumsListComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = 0; }
        return status ? 'Active' : 'Closed';
    };
    CurriculumsListComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = 0; }
        return status ? 'success' : 'metal';
    };
    CurriculumsListComponent.prototype.getNewCssClassByStatus = function (x) {
        return moment(new Date()).diff(x, 'days') > 1;
    };
    CurriculumsListComponent.prototype.getChildrenEntities = function (id) {
        this.router.navigateByUrl('educational-center/courses?parentId=' + id);
    };
    CurriculumsListComponent.prototype.timeAgo = function (x) {
        return moment(x).fromNow();
    };
    __decorate([
        ViewChild(MatPaginator)
    ], CurriculumsListComponent.prototype, "paginator");
    __decorate([
        ViewChild(MatSort)
    ], CurriculumsListComponent.prototype, "sort");
    __decorate([
        ViewChild('searchInput')
    ], CurriculumsListComponent.prototype, "searchInput");
    CurriculumsListComponent = __decorate([
        Component({
            selector: 'm-curriculums-list',
            templateUrl: './curriculums-list.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], CurriculumsListComponent);
    return CurriculumsListComponent;
}());
export { CurriculumsListComponent };
