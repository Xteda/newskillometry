import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { fromEvent, merge } from 'rxjs';
// Services
import { CurriculumsService} from '../../_core/services/curriculums.service';
import { LayoutUtilsService, MessageType } from '../../_core/utils/layout-utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
// Models
import { CurriculumDatasource} from '../../_core/models/data-sources/curriculum.datasource';
import { QueryParamsModel } from '../../_core/models/query-models/query-params.model';
import { CurriculumModel } from '../../_core/models/curriculum.model';
import * as moment from 'moment';
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4

@Component({
	selector: 'm-curriculums-list',
	templateUrl: './curriculums-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CurriculumsListComponent implements OnInit {
	// Table fields
	dataSource: CurriculumDatasource;
	displayedColumns = ['select', 'title', 'author', 'children', 'create', 'update', 'status', 'actions'];
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<CurriculumModel>(true, []);
	curriculumsResult: CurriculumModel[] = [];

	constructor(private entityService: CurriculumsService,
				private router: Router,
				public dialog: MatDialog,
				private route: ActivatedRoute,
				private subheaderService: SubheaderService,
				private layoutUtilsService: LayoutUtilsService) { }

	/** LOAD DATA */
	ngOnInit() {
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadCurriculumsList();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadCurriculumsList();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		this.subheaderService.setTitle('Curriculums');
		// Init DataSource
		this.dataSource = new CurriculumDatasource(this.entityService);
		let queryParams = new QueryParamsModel({});
		queryParams.sortOrder = 'desc';
		queryParams.sortField = 'create';
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				queryParams = this.entityService.lastFilter$.getValue();
				this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadProducts(queryParams);
		});
		this.dataSource.entitySubject.subscribe(res => this.curriculumsResult = res);
	}

	loadCurriculumsList() {
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction = 'desc',
			this.sort.active = '_createdDate',
			this.paginator.pageIndex,
			this.paginator.pageSize,
		);
		this.dataSource.loadProducts(queryParams);
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;

		if (this.filterStatus && this.filterStatus.length > 0) {
			filter.status = +this.filterStatus;
		}

		if (this.filterCondition && this.filterCondition.length > 0) {
			filter.condition = +this.filterCondition;
		}

		filter.author = searchText;

		filter.title = searchText;

		return filter;
	}

	restoreState(queryParams: QueryParamsModel, id: number) {
		console.log('restoreState', id);
		if (id > 0) {
			this.entityService.getCurriculumById(id).subscribe((res: CurriculumModel) => {
				console.log('res', res);
				const message = res._createdDate === res._updatedDate ?
					`New curriculum successfully has been added.` :
					`Curriculum successfully has been saved.`;
				this.layoutUtilsService.showActionNotification(message, res._isNew ? MessageType.Create : MessageType.Update, 10000, true, false);
			});
		}

		if (!queryParams.filter) {
			return;
		}

		if ('create' in queryParams.filter) {
			this.filterCondition = queryParams.filter.condition.toString();
		}

		if ('status' in queryParams.filter) {
			this.filterStatus = queryParams.filter.status.toString();
		}

		if (queryParams.filter.model) {
			this.searchInput.nativeElement.value = queryParams.filter.model;
		}
	}

	/** ACTIONS */
	/** Delete */
	deleteCurriculum(event, _item: CurriculumModel) {
		event.stopPropagation();
		const _title: string = 'Curriculum Delete';
		const _description: string = 'Are you sure to permanently delete this curriculum?';
		const _waitDesciption: string = 'Curriculum is deleting...';
		const _deleteMessage = `Curriculum has been deleted`;

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.entityService.deleteCurriculum(_item.id).subscribe(() => {
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
				this.loadCurriculumsList();
			});
		});
	}

	deleteCurriculums() {
		const _title: string = 'Curriculums Delete';
		const _description: string = 'Are you sure to permanently delete selected curriculums?';
		const _waitDesciption: string = 'Curriculums are deleting...';
		const _deleteMessage = 'Selected curriculums have been deleted';

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			const idsForDeletion: number[] = [];
			for (let i = 0; i < this.selection.selected.length; i++) {
				idsForDeletion.push(this.selection.selected[i].id);
			}
			this.entityService.deleteProducts(idsForDeletion).subscribe(() => {
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
				this.loadCurriculumsList();
				this.selection.clear();
			});
		});
	}

	/** Update Product */
	updateStatusForProducts() {
		const _title = 'Update status for selected curriculums';
		const _updateMessage = 'Status has been updated for selected curriculums';
		const _statuses = [{ value: 1, text: 'Active' }, { value: 0, text: 'Closed' }];
		const _messages = [];

		this.selection.selected.forEach(elem => {
			_messages.push({
				text: `${elem.author}, ${elem.title} `,
				id: elem.id,
				status: elem.status,
				statusTitle: this.getItemStatusString(elem.status),
				statusCssClass: this.getItemCssClassByStatus(elem.status)
			});
		});

		const dialogRef = this.layoutUtilsService.updateStatusForCustomers(_title, _statuses, _messages);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				this.selection.clear();
				return;
			}

			this.entityService.updateStatusForCurriculum(this.selection.selected, +res).subscribe(() => {
				this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update);
				this.loadCurriculumsList();
				this.selection.clear();
			});
		});
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.curriculumsResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.curriculumsResult.forEach(row => this.selection.select(row));
		}
	}

	/* UI */
	getItemStatusString(status: number = 0): string {
		return status ? 'Active' : 'Closed';
	}

	getItemCssClassByStatus(status: number = 0): string {
		return status ? 'success' : 'metal';
	}

	getNewCssClassByStatus(x) {
		return moment(new Date()).diff(x, 'days') > 1;
	}

	getChildrenEntities(id) {
		this.router.navigateByUrl('educational-center/courses?parentId=' + id);
	}

	public timeAgo(x) {
		return moment(x).fromNow();
	}
}
