import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { VideoModel} from "../models/video.model";
import {CriteriaModel} from "../models/criteria.model";
import {map} from "rxjs/operators";

const API_PRODUCTREMARKS_URL = `http://88.80.2.13:3000/videos`;
/*const API_PRODUCTREMARKS_URL = `http://${location.hostname}:3000/videos`;*/

@Injectable()
export class VideoService {
	httpOptions = this.httpUtils.getHTTPHeader();
	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {

	}

	// CREATE =>  POST: add a new video to the server
	createVideo(video): Observable<VideoModel> {
		return this.http.post<VideoModel>(API_PRODUCTREMARKS_URL, video, this.httpUtils.getHTTPHeader());
	}

	// READ
	getVideoByEntityId(entityId: number) {
		return this.http.get<VideoModel[]>(API_PRODUCTREMARKS_URL)
			.pipe(
				map(video => video.filter(v => v => v.curriculumId === entityId || v.courseId === entityId || v.lessonId === entityId || v.instructionId === entityId))
			);
	}

	getVideoById(videoId: number): Observable<VideoModel> {
		return this.http.get<VideoModel>(API_PRODUCTREMARKS_URL + `/${videoId}`);
	}

	findVideos(queryParams: QueryParamsModel, entityId: number): Observable<VideoModel[]> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// comment this, when you work with real server
		// start
		return this.getVideoByEntityId(entityId);
		// end

		// uncomment this, when you work with real server
		// start
		//  const url = this.API_PRODUCTREMARKS_URL + '/find';
		//  return this.http.get<ProductRemarkModel[]>(url, params);
		// Note: Add headers if needed
		// end
	}

	// UPDATE => PUT: update the video on the server
	updateVideo(video: VideoModel): Observable<any> {
		return this.http.put(API_PRODUCTREMARKS_URL + `/${video.id}`, video, this.httpUtils.getHTTPHeader());
	}

	// DELETE => delete the video from the server
	deleteVideo(video: VideoModel): Observable<VideoModel> {
		const url = `${API_PRODUCTREMARKS_URL}/${video.id}`;
		return this.http.delete<VideoModel>(url);
	}
}
