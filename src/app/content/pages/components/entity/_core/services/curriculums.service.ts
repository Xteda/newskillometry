import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpUtilsService } from '../utils/http-utils.service';
import { CurriculumModel} from '../models/curriculum.model';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';


const JSON_PRODUCTS_URL = `http://88.80.2.13:3000/curriculums`;
/*const JSON_PRODUCTS_URL = `http://${location.hostname}:3000/curriculums`;*/

/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/

@Injectable()
export class CurriculumsService {
	lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));

	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {}

	// CREATE =>  POST: add a new product to the server
	createCurriculum(curriculum): Observable<CurriculumModel> {
		return this.http.post<CurriculumModel>(JSON_PRODUCTS_URL, curriculum, this.httpUtils.getHTTPHeader());
	}

	// READ
	getAllCurriculums(): Observable<CurriculumModel[]> {
		return this.http.get<CurriculumModel[]>(JSON_PRODUCTS_URL);
	}

	getCurriculumById(curriculumId: number): Observable<any> {
		return this.http.get<CurriculumModel>(JSON_PRODUCTS_URL + `/${curriculumId}?_embed=criteria&_embed=videos&_embed=markers&_embed=files`);
	}

	findCurriculum(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		return this.getAllCurriculums().pipe(
			mergeMap(res => of(new QueryResultsModel(res)))
		);
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
		// END
	}

	// UPDATE => PUT: update the product on the server
	updateCurriculum(curriculum: CurriculumModel): Observable<any> {
		return this.http.put(JSON_PRODUCTS_URL + `/${curriculum.id}`, curriculum, this.httpUtils.getHTTPHeader());
	}

	// UPDATE Status
	// Comment this when you start work with real server
	// This code imitates server calls
	updateStatusForCurriculum(curriculums: CurriculumModel[], status: number): Observable<any> {
		const tasks$ = [];
		for (let i = 0; i < curriculums.length; i++) {
			const _curriculum = curriculums[i];
			_curriculum.status = status;
			tasks$.push(this.updateCurriculum(_curriculum));
		}
		return forkJoin(tasks$);
	}

	// DELETE => delete the product from the server
	deleteCurriculum(curriculumId: number): Observable<CurriculumModel> {
		const url = `${JSON_PRODUCTS_URL}/${curriculumId}`;
		return this.http.delete<CurriculumModel>(url);
	}

	// Method imitates server calls which deletes items from DB (should rewrite this to real server call)
	// START
	deleteProducts(ids: number[] = []) {
		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			tasks$.push(this.deleteCurriculum(ids[i]));
		}
		return forkJoin(tasks$);
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*for (let i = 0; i < length; i++) {
			const url = `${JSON_PRODUCTS_URL}/${ids}`;
			return this.http.delete<CurriculumModel>(url);
		}*/
		/*const url = JSON_PRODUCTS_URL + '/delete';
		return this.http.get<QueryResultsModel>(url);*/
		// END
	}
}
