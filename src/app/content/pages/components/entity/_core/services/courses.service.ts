import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
import {CourseModel} from '../models/course.model';


const JSON_PRODUCTS_URL = `http://88.80.2.13:3000/courses`;
// const JSON_PRODUCTS_URL = `http://${location.hostname}:3000/courses`;

/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/

@Injectable()
export class CoursesService {
	lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));

	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {}

	// CREATE =>  POST: add a new product to the server
	createCourse(course): Observable<CourseModel> {
		return this.http.post<CourseModel>(JSON_PRODUCTS_URL, course, this.httpUtils.getHTTPHeader());
	}

	// READ
	getAllCourses(): Observable<CourseModel[]> {
		return this.http.get<CourseModel[]>(JSON_PRODUCTS_URL);
	}

	getCoursesByParentId(parentId: number): Observable<CourseModel[]> {
		return this.http.get<CourseModel[]>(JSON_PRODUCTS_URL + `/?curriculumId=${parentId}`);
	}

	getCourseById(courseId: number): Observable<any> {
		return this.http.get<CourseModel>(JSON_PRODUCTS_URL + `/${courseId}?_embed=criteria&_embed=videos&_embed=markers&_embed=files`);
	}

	findCourse(queryParams: QueryParamsModel, parentId?): Observable<QueryResultsModel> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		if (parentId) {
			return this.getCoursesByParentId(parentId).pipe(
				mergeMap(res => of(new QueryResultsModel(res)))
			);
		} else {
			return this.getAllCourses().pipe(
				mergeMap(res => of(new QueryResultsModel(res)))
			);
		}
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
		// END
	}

	// UPDATE => PUT: update the product on the server
	updateCourse(course: CourseModel): Observable<any> {
		return this.http.put(JSON_PRODUCTS_URL + `/${course.id}`, course, this.httpUtils.getHTTPHeader());
	}

	// UPDATE Status
	// Comment this when you start work with real server
	// This code imitates server calls
	updateStatusForCourse(courses: CourseModel[], status: number): Observable<any> {
		const tasks$ = [];
		for (let i = 0; i < courses.length; i++) {
			const _course = courses[i];
			_course.status = status;
			tasks$.push(this.updateCourse(_course));
		}
		return forkJoin(tasks$);
	}

	// DELETE => delete the product from the server
	deleteCourse(courseId: number): Observable<CourseModel> {
		const url = `${JSON_PRODUCTS_URL}/${courseId}`;
		return this.http.delete<CourseModel>(url);
	}

	// Method imitates server calls which deletes items from DB (should rewrite this to real server call)
	// START
	deleteCourses(ids: number[] = []) {
		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			tasks$.push(this.deleteCourse(ids[i]));
		}
		return forkJoin(tasks$);
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*for (let i = 0; i < length; i++) {
			const url = `${JSON_PRODUCTS_URL}/${ids}`;
			return this.http.delete<courseModel>(url);
		}*/
		/*const url = JSON_PRODUCTS_URL + '/delete';
		return this.http.get<QueryResultsModel>(url);*/
		// END
	}
}
