import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { FileModel} from '../models/file.model';
import {forkJoin} from 'rxjs/index';
import {MarkerModel} from '../models/marker.model';

const API_PRODUCTREMARKS_URL = `http://88.80.2.13:3000/files`;
/*const API_PRODUCTREMARKS_URL = `http://${location.hostname}:3000/files`;*/

@Injectable()
export class FilesService {
	httpOptions = this.httpUtils.getHTTPHeader();
	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {

	}

	// CREATE =>  POST: add a new video to the server
	createFile(file): Observable<FileModel> {
		return this.http.post<FileModel>(API_PRODUCTREMARKS_URL, file, this.httpUtils.getHTTPHeader());
	}

	// READ
	getFiles(entityId?: number) {
		return this.http.get<FileModel[]>(API_PRODUCTREMARKS_URL);
	}

	getFileById(markerId: number): Observable<FileModel> {
		return this.http.get<FileModel>(API_PRODUCTREMARKS_URL + `/${markerId}`);
	}

	findFile(queryParams: QueryParamsModel, entityId: number): Observable<FileModel[]> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// comment this, when you work with real server
		// start
		return this.getFiles(entityId);
		// end

		// uncomment this, when you work with real server
		// start
		//  const url = this.API_PRODUCTREMARKS_URL + '/find';
		//  return this.http.get<ProductRemarkModel[]>(url, params);
		// Note: Add headers if needed
		// end
	}

	// UPDATE => PUT: update the video on the server
	updateFile(file: FileModel): Observable<any> {
		return this.http.put(API_PRODUCTREMARKS_URL + `/${file.id}`, file, this.httpUtils.getHTTPHeader());
	}

	updateFiles(files: FileModel[]): Observable<any> {
		const tasks$ = [];
		for (let i = 0; i < files.length; i++) {
			const _file = files[i];
			if (_file.id) {
				tasks$.push(this.updateFile(_file));
			} else {
				tasks$.push(this.createFile(_file));
			}
		}
		return forkJoin(tasks$);
	}

	// DELETE => delete the video from the server
	deleteFile(file: FileModel): Observable<FileModel> {
		const url = `${API_PRODUCTREMARKS_URL}/${file.id}`;
		return this.http.delete<FileModel>(url);
	}

	deleteFiles(ids: number[] = []) {
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			const url = `${API_PRODUCTREMARKS_URL}/${ids[i]}`;
			tasks$.push(this.http.delete<FileModel>(url));
		}
		return forkJoin(tasks$);
	}
}
