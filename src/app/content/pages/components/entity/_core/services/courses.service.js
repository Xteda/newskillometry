var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
// const API_PRODUCTS_URL = `https://my-json-server.typicode.com/Xteda87/jsonServer/courses`;
var JSON_PRODUCTS_URL = "http://" + location.hostname + ":3000/courses";
/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/
var CoursesService = /** @class */ (function () {
    function CoursesService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.lastFilter$ = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    }
    // CREATE =>  POST: add a new product to the server
    CoursesService.prototype.createCourse = function (course) {
        return this.http.post(JSON_PRODUCTS_URL, course, this.httpUtils.getHTTPHeader());
    };
    // READ
    CoursesService.prototype.getAllCourses = function () {
        return this.http.get(JSON_PRODUCTS_URL);
    };
    CoursesService.prototype.getCoursesByParentId = function (parentId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/?curriculumId=" + parentId));
    };
    CoursesService.prototype.getCourseById = function (courseId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/" + courseId + "?_embed=criteria&_embed=videos&_embed=markers&_embed=files"));
    };
    CoursesService.prototype.findCourse = function (queryParams, parentId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        if (parentId) {
            return this.getCoursesByParentId(parentId).pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        }
        else {
            return this.getAllCourses().pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        }
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    // UPDATE => PUT: update the product on the server
    CoursesService.prototype.updateCourse = function (course) {
        return this.http.put(JSON_PRODUCTS_URL + ("/" + course.id), course, this.httpUtils.getHTTPHeader());
    };
    // UPDATE Status
    // Comment this when you start work with real server
    // This code imitates server calls
    CoursesService.prototype.updateStatusForCourse = function (courses, status) {
        var tasks$ = [];
        for (var i = 0; i < courses.length; i++) {
            var _course = courses[i];
            _course.status = status;
            tasks$.push(this.updateCourse(_course));
        }
        return forkJoin(tasks$);
    };
    // DELETE => delete the product from the server
    CoursesService.prototype.deleteCourse = function (courseId) {
        var url = JSON_PRODUCTS_URL + "/" + courseId;
        return this.http["delete"](url);
    };
    // Method imitates server calls which deletes items from DB (should rewrite this to real server call)
    // START
    CoursesService.prototype.deleteCourses = function (ids) {
        if (ids === void 0) { ids = []; }
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        var tasks$ = [];
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            tasks$.push(this.deleteCourse(ids[i]));
        }
        return forkJoin(tasks$);
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*for (let i = 0; i < length; i++) {
            const url = `${JSON_PRODUCTS_URL}/${ids}`;
            return this.http.delete<courseModel>(url);
        }*/
        /*const url = JSON_PRODUCTS_URL + '/delete';
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    CoursesService = __decorate([
        Injectable()
    ], CoursesService);
    return CoursesService;
}());
export { CoursesService };
