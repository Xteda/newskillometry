var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
var API_PRODUCTREMARKS_URL = "http://" + location.hostname + ":3000/videos";
var VideoService = /** @class */ (function () {
    function VideoService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.httpOptions = this.httpUtils.getHTTPHeader();
    }
    // CREATE =>  POST: add a new video to the server
    VideoService.prototype.createVideo = function (video) {
        return this.http.post(API_PRODUCTREMARKS_URL, video, this.httpUtils.getHTTPHeader());
    };
    // READ
    VideoService.prototype.getVideoByEntityId = function (entityId) {
        return this.http.get(API_PRODUCTREMARKS_URL)
            .pipe(map(function (video) { return video.filter(function (v) { return function (v) { return v.curriculumId === entityId || v.courseId === entityId || v.lessonId === entityId || v.instructionId === entityId; }; }); }));
    };
    VideoService.prototype.getVideoById = function (videoId) {
        return this.http.get(API_PRODUCTREMARKS_URL + ("/" + videoId));
    };
    VideoService.prototype.findVideos = function (queryParams, entityId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // comment this, when you work with real server
        // start
        return this.getVideoByEntityId(entityId);
        // end
        // uncomment this, when you work with real server
        // start
        //  const url = this.API_PRODUCTREMARKS_URL + '/find';
        //  return this.http.get<ProductRemarkModel[]>(url, params);
        // Note: Add headers if needed
        // end
    };
    // UPDATE => PUT: update the video on the server
    VideoService.prototype.updateVideo = function (video) {
        return this.http.put(API_PRODUCTREMARKS_URL + ("/" + video.id), video, this.httpUtils.getHTTPHeader());
    };
    // DELETE => delete the video from the server
    VideoService.prototype.deleteVideo = function (video) {
        var url = API_PRODUCTREMARKS_URL + "/" + video.id;
        return this.http["delete"](url);
    };
    VideoService = __decorate([
        Injectable()
    ], VideoService);
    return VideoService;
}());
export { VideoService };
