import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { CriteriaModel} from '../models/criteria.model';

const API_PRODUCTREMARKS_URL = `http://88.80.2.13:3000/criteria`;
/*const API_PRODUCTREMARKS_URL = `http://${location.hostname}:3000/criteria`;*/

@Injectable()
export class CurriculumCriteriaService {
	httpOptions = this.httpUtils.getHTTPHeader();
	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {

	}

	// CREATE =>  POST: add a new product remark to the server
	createCriteria(criteria): Observable<CriteriaModel> {
		return this.http.post<CriteriaModel>(API_PRODUCTREMARKS_URL, criteria, this.httpUtils.getHTTPHeader());
	}

	// READ
	getCriteriaByEntityId(productId: number) {
		return this.http.get<CriteriaModel[]>(API_PRODUCTREMARKS_URL)
			.pipe(
				map(criteria => criteria.filter(rem => rem.curriculumId === productId || rem.courseId === productId || rem.lessonId === productId || rem.instructionId === productId))
			);
	}

	getCriteriaById(criteriaId: number): Observable<CriteriaModel> {
		return this.http.get<CriteriaModel>(API_PRODUCTREMARKS_URL + `/${criteriaId}`);
	}

	findCriteria(queryParams: QueryParamsModel, productId: number): Observable<CriteriaModel[]> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// comment this, when you work with real server
		// start
		return this.getCriteriaByEntityId(productId);
		// end

		// uncomment this, when you work with real server
		// start
		//  const url = this.API_PRODUCTREMARKS_URL + '/find';
		//  return this.http.get<ProductRemarkModel[]>(url, params);
		// Note: Add headers if needed
		// end
	}

	// UPDATE => PUT: update the product remark on the server
	updateCriteria(criteria: CriteriaModel): Observable<any> {
		return this.http.put(API_PRODUCTREMARKS_URL + `/${criteria.id}`, criteria, this.httpUtils.getHTTPHeader());
	}

	// DELETE => delete the product remark from the server
	deleteCriteria(criteria: CriteriaModel): Observable<CriteriaModel> {
		const url = `${API_PRODUCTREMARKS_URL}/${criteria.id}`;
		return this.http.delete<CriteriaModel>(url);
	}
}
