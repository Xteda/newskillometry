var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
var API_PRODUCTREMARKS_URL = "http://" + location.hostname + ":3000/criteria";
var CurriculumCriteriaService = /** @class */ (function () {
    function CurriculumCriteriaService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.httpOptions = this.httpUtils.getHTTPHeader();
    }
    // CREATE =>  POST: add a new product remark to the server
    CurriculumCriteriaService.prototype.createCriteria = function (criteria) {
        return this.http.post(API_PRODUCTREMARKS_URL, criteria, this.httpUtils.getHTTPHeader());
    };
    // READ
    CurriculumCriteriaService.prototype.getCriteriaByEntityId = function (productId) {
        return this.http.get(API_PRODUCTREMARKS_URL)
            .pipe(map(function (criteria) { return criteria.filter(function (rem) { return rem.curriculumId === productId || rem.courseId === productId || rem.lessonId === productId || rem.instructionId === productId; }); }));
    };
    CurriculumCriteriaService.prototype.getCriteriaById = function (criteriaId) {
        return this.http.get(API_PRODUCTREMARKS_URL + ("/" + criteriaId));
    };
    CurriculumCriteriaService.prototype.findCriteria = function (queryParams, productId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // comment this, when you work with real server
        // start
        return this.getCriteriaByEntityId(productId);
        // end
        // uncomment this, when you work with real server
        // start
        //  const url = this.API_PRODUCTREMARKS_URL + '/find';
        //  return this.http.get<ProductRemarkModel[]>(url, params);
        // Note: Add headers if needed
        // end
    };
    // UPDATE => PUT: update the product remark on the server
    CurriculumCriteriaService.prototype.updateCriteria = function (criteria) {
        return this.http.put(API_PRODUCTREMARKS_URL + ("/" + criteria.id), criteria, this.httpUtils.getHTTPHeader());
    };
    // DELETE => delete the product remark from the server
    CurriculumCriteriaService.prototype.deleteCriteria = function (criteria) {
        var url = API_PRODUCTREMARKS_URL + "/" + criteria.id;
        return this.http["delete"](url);
    };
    CurriculumCriteriaService = __decorate([
        Injectable()
    ], CurriculumCriteriaService);
    return CurriculumCriteriaService;
}());
export { CurriculumCriteriaService };
