import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { MarkerModel} from '../models/marker.model';
import {forkJoin} from 'rxjs/index';

/*const API_PRODUCTREMARKS_URL = `http://${location.hostname}:3000/markers`;*/
const API_PRODUCTREMARKS_URL = `http://88.80.2.13:3000/markers`;

@Injectable()
export class MarkersService {
	httpOptions = this.httpUtils.getHTTPHeader();
	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {

	}

	// CREATE =>  POST: add a new video to the server
	createMarker(marker): Observable<MarkerModel> {
		return this.http.post<MarkerModel>(API_PRODUCTREMARKS_URL, marker, this.httpUtils.getHTTPHeader());
	}

	// READ
	getMarkers(entityId?: number) {
		return this.http.get<MarkerModel[]>(API_PRODUCTREMARKS_URL);
	}

	getMarkerById(markerId: number): Observable<MarkerModel> {
		return this.http.get<MarkerModel>(API_PRODUCTREMARKS_URL + `/${markerId}`);
	}

	findMarker(queryParams: QueryParamsModel, entityId: number): Observable<MarkerModel[]> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// comment this, when you work with real server
		// start
		return this.getMarkers(entityId);
		// end

		// uncomment this, when you work with real server
		// start
		//  const url = this.API_PRODUCTREMARKS_URL + '/find';
		//  return this.http.get<ProductRemarkModel[]>(url, params);
		// Note: Add headers if needed
		// end
	}

	// UPDATE => PUT: update the video on the server
	updateMarker(marker: MarkerModel): Observable<any> {
		return this.http.put(API_PRODUCTREMARKS_URL + `/${marker.id}`, marker, this.httpUtils.getHTTPHeader());
	}

	updateMarkers(markers: MarkerModel[]): Observable<any> {
		const tasks$ = [];
		for (let i = 0; i < markers.length; i++) {
			const _marker = markers[i];
			if (_marker.id) {
				tasks$.push(this.updateMarker(_marker));
			} else {
				tasks$.push(this.createMarker(_marker));
			}
		}
		return forkJoin(tasks$);
	}

	// DELETE => delete the video from the server
	deleteMarker(marker: MarkerModel): Observable<MarkerModel> {
		const url = `${API_PRODUCTREMARKS_URL}/${marker.id}`;
		return this.http.delete<MarkerModel>(url);
	}

	deleteMarkers(ids: number[] = []) {
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			const url = `${API_PRODUCTREMARKS_URL}/${ids[i]}`;
			console.log(i)
			tasks$.push(this.http.delete<MarkerModel>(url));
		}
		return forkJoin(tasks$);
	}
}
