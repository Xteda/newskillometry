var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs/index';
var API_PRODUCTREMARKS_URL = "http://" + location.hostname + ":3000/files";
var FilesService = /** @class */ (function () {
    function FilesService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.httpOptions = this.httpUtils.getHTTPHeader();
    }
    // CREATE =>  POST: add a new video to the server
    FilesService.prototype.createFile = function (file) {
        return this.http.post(API_PRODUCTREMARKS_URL, file, this.httpUtils.getHTTPHeader());
    };
    // READ
    FilesService.prototype.getFiles = function (entityId) {
        return this.http.get(API_PRODUCTREMARKS_URL);
    };
    FilesService.prototype.getFileById = function (markerId) {
        return this.http.get(API_PRODUCTREMARKS_URL + ("/" + markerId));
    };
    FilesService.prototype.findFile = function (queryParams, entityId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // comment this, when you work with real server
        // start
        return this.getFiles(entityId);
        // end
        // uncomment this, when you work with real server
        // start
        //  const url = this.API_PRODUCTREMARKS_URL + '/find';
        //  return this.http.get<ProductRemarkModel[]>(url, params);
        // Note: Add headers if needed
        // end
    };
    // UPDATE => PUT: update the video on the server
    FilesService.prototype.updateFile = function (file) {
        return this.http.put(API_PRODUCTREMARKS_URL + ("/" + file.id), file, this.httpUtils.getHTTPHeader());
    };
    FilesService.prototype.updateFiles = function (files) {
        var tasks$ = [];
        for (var i = 0; i < files.length; i++) {
            var _file = files[i];
            if (_file.id) {
                tasks$.push(this.updateFile(_file));
            }
            else {
                tasks$.push(this.createFile(_file));
            }
        }
        return forkJoin(tasks$);
    };
    // DELETE => delete the video from the server
    FilesService.prototype.deleteFile = function (file) {
        var url = API_PRODUCTREMARKS_URL + "/" + file.id;
        return this.http["delete"](url);
    };
    FilesService.prototype.deleteFiles = function (ids) {
        if (ids === void 0) { ids = []; }
        var tasks$ = [];
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            var url = API_PRODUCTREMARKS_URL + "/" + ids[i];
            tasks$.push(this.http["delete"](url));
        }
        return forkJoin(tasks$);
    };
    FilesService = __decorate([
        Injectable()
    ], FilesService);
    return FilesService;
}());
export { FilesService };
