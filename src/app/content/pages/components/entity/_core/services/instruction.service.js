var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
// const API_PRODUCTS_URL = `https://my-json-server.typicode.com/Xteda87/jsonServer/courses`;
var JSON_PRODUCTS_URL = "http://" + location.hostname + ":3000/instructions";
/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/
var InstructionService = /** @class */ (function () {
    function InstructionService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.lastFilter$ = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    }
    // CREATE =>  POST: add a new product to the server
    InstructionService.prototype.createInstruction = function (instruction) {
        return this.http.post(JSON_PRODUCTS_URL, instruction, this.httpUtils.getHTTPHeader());
    };
    // READ
    InstructionService.prototype.getAllInstructions = function () {
        return this.http.get(JSON_PRODUCTS_URL);
    };
    InstructionService.prototype.getInstructionsByParentId = function (parentId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/?parentId=" + parentId));
    };
    InstructionService.prototype.getInstructionById = function (instructionId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/" + instructionId + "?_embed=criteria&_embed=videos&_embed=markers&_embed=files"));
    };
    InstructionService.prototype.findInstruction = function (queryParams, parentId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        if (parentId) {
            return this.getInstructionsByParentId(parentId).pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        }
        else {
            return this.getAllInstructions().pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        }
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    // UPDATE => PUT: update the product on the server
    InstructionService.prototype.updateInstruction = function (instruction) {
        return this.http.put(JSON_PRODUCTS_URL + ("/" + instruction.id), instruction, this.httpUtils.getHTTPHeader());
    };
    // UPDATE Status
    // Comment this when you start work with real server
    // This code imitates server calls
    InstructionService.prototype.updateStatusForInstruction = function (instructions, status) {
        var tasks$ = [];
        for (var i = 0; i < instructions.length; i++) {
            var _instruction = instructions[i];
            _instruction.status = status;
            tasks$.push(this.updateInstruction(_instruction));
        }
        return forkJoin(tasks$);
    };
    // DELETE => delete the product from the server
    InstructionService.prototype.deleteInstruction = function (instructionId) {
        var url = JSON_PRODUCTS_URL + "/" + instructionId;
        return this.http["delete"](url);
    };
    // Method imitates server calls which deletes items from DB (should rewrite this to real server call)
    // START
    InstructionService.prototype.deleteInstructions = function (ids) {
        if (ids === void 0) { ids = []; }
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        var tasks$ = [];
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            tasks$.push(this.deleteInstruction(ids[i]));
        }
        return forkJoin(tasks$);
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*for (let i = 0; i < length; i++) {
            const url = `${JSON_PRODUCTS_URL}/${ids}`;
            return this.http.delete<courseModel>(url);
        }*/
        /*const url = JSON_PRODUCTS_URL + '/delete';
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    InstructionService = __decorate([
        Injectable()
    ], InstructionService);
    return InstructionService;
}());
export { InstructionService };
