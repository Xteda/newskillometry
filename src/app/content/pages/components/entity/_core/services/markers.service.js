var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs/index';
var API_PRODUCTREMARKS_URL = "http://" + location.hostname + ":3000/markers";
var MarkersService = /** @class */ (function () {
    function MarkersService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.httpOptions = this.httpUtils.getHTTPHeader();
    }
    // CREATE =>  POST: add a new video to the server
    MarkersService.prototype.createMarker = function (marker) {
        return this.http.post(API_PRODUCTREMARKS_URL, marker, this.httpUtils.getHTTPHeader());
    };
    // READ
    MarkersService.prototype.getMarkers = function (entityId) {
        return this.http.get(API_PRODUCTREMARKS_URL);
    };
    MarkersService.prototype.getMarkerById = function (markerId) {
        return this.http.get(API_PRODUCTREMARKS_URL + ("/" + markerId));
    };
    MarkersService.prototype.findMarker = function (queryParams, entityId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // comment this, when you work with real server
        // start
        return this.getMarkers(entityId);
        // end
        // uncomment this, when you work with real server
        // start
        //  const url = this.API_PRODUCTREMARKS_URL + '/find';
        //  return this.http.get<ProductRemarkModel[]>(url, params);
        // Note: Add headers if needed
        // end
    };
    // UPDATE => PUT: update the video on the server
    MarkersService.prototype.updateMarker = function (marker) {
        return this.http.put(API_PRODUCTREMARKS_URL + ("/" + marker.id), marker, this.httpUtils.getHTTPHeader());
    };
    MarkersService.prototype.updateMarkers = function (markers) {
        var tasks$ = [];
        for (var i = 0; i < markers.length; i++) {
            var _marker = markers[i];
            if (_marker.id) {
                tasks$.push(this.updateMarker(_marker));
            }
            else {
                tasks$.push(this.createMarker(_marker));
            }
        }
        return forkJoin(tasks$);
    };
    // DELETE => delete the video from the server
    MarkersService.prototype.deleteMarker = function (marker) {
        var url = API_PRODUCTREMARKS_URL + "/" + marker.id;
        return this.http["delete"](url);
    };
    MarkersService.prototype.deleteMarkers = function (ids) {
        if (ids === void 0) { ids = []; }
        var tasks$ = [];
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            var url = API_PRODUCTREMARKS_URL + "/" + ids[i];
            console.log(i);
            tasks$.push(this.http["delete"](url));
        }
        return forkJoin(tasks$);
    };
    MarkersService = __decorate([
        Injectable()
    ], MarkersService);
    return MarkersService;
}());
export { MarkersService };
