import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
import { InstructionModel} from '../models/instruction.model';


// const API_PRODUCTS_URL = `https://my-json-server.typicode.com/Xteda87/jsonServer/courses`;
const JSON_PRODUCTS_URL = `http://88.80.2.13:3000/instructions`;
/*const JSON_PRODUCTS_URL = `http://${location.hostname}:3000/instructions`;*/

/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/

@Injectable()
export class InstructionService {
	lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));

	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {}

	// CREATE =>  POST: add a new product to the server
	createInstruction(instruction): Observable<InstructionModel> {
		return this.http.post<InstructionModel>(JSON_PRODUCTS_URL, instruction, this.httpUtils.getHTTPHeader());
	}

	// READ
	getAllInstructions(): Observable<InstructionModel[]> {
		return this.http.get<InstructionModel[]>(JSON_PRODUCTS_URL);
	}

	getInstructionsByParentId(parentId: number): Observable<InstructionModel[]> {
		return this.http.get<InstructionModel[]>(JSON_PRODUCTS_URL + `/?parentId=${parentId}`);
	}

	getInstructionById(instructionId: number): Observable<any> {
		return this.http.get<InstructionModel>(JSON_PRODUCTS_URL + `/${instructionId}?_embed=criteria&_embed=videos&_embed=markers&_embed=files`);
	}

	findInstruction(queryParams: QueryParamsModel, parentId?): Observable<QueryResultsModel> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		if (parentId) {
			return this.getInstructionsByParentId(parentId).pipe(
				mergeMap(res => of(new QueryResultsModel(res)))
			);
		} else {
			return this.getAllInstructions().pipe(
				mergeMap(res => of(new QueryResultsModel(res)))
			);
		}
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
		// END
	}

	// UPDATE => PUT: update the product on the server
	updateInstruction(instruction: InstructionModel): Observable<any> {
		return this.http.put(JSON_PRODUCTS_URL + `/${instruction.id}`, instruction, this.httpUtils.getHTTPHeader());
	}

	// UPDATE Status
	// Comment this when you start work with real server
	// This code imitates server calls
	updateStatusForInstruction(instructions: InstructionModel[], status: number): Observable<any> {
		const tasks$ = [];
		for (let i = 0; i < instructions.length; i++) {
			const _instruction = instructions[i];
			_instruction.status = status;
			tasks$.push(this.updateInstruction(_instruction));
		}
		return forkJoin(tasks$);
	}

	// DELETE => delete the product from the server
	deleteInstruction(instructionId: number): Observable<InstructionModel> {
		const url = `${JSON_PRODUCTS_URL}/${instructionId}`;
		return this.http.delete<InstructionModel>(url);
	}

	// Method imitates server calls which deletes items from DB (should rewrite this to real server call)
	// START
	deleteInstructions(ids: number[] = []) {
		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			tasks$.push(this.deleteInstruction(ids[i]));
		}
		return forkJoin(tasks$);
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*for (let i = 0; i < length; i++) {
			const url = `${JSON_PRODUCTS_URL}/${ids}`;
			return this.http.delete<courseModel>(url);
		}*/
		/*const url = JSON_PRODUCTS_URL + '/delete';
		return this.http.get<QueryResultsModel>(url);*/
		// END
	}
}
