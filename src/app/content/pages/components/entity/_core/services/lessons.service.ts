import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpUtilsService } from '../utils/http-utils.service';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
import { LessonModel } from '../models/lesson.model';


// const API_PRODUCTS_URL = `https://my-json-server.typicode.com/Xteda87/jsonServer/courses`;
const JSON_PRODUCTS_URL = `http://88.80.2.13:3000/lessons`;
/*const JSON_PRODUCTS_URL = `http://${location.hostname}:3000/lessons`;*/

/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/

@Injectable()
export class LessonsService {
	lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));

	constructor(private http: HttpClient,
				private httpUtils: HttpUtilsService) {}

	// CREATE =>  POST: add a new product to the server
	createLesson(lesson): Observable<LessonModel> {
		return this.http.post<LessonModel>(JSON_PRODUCTS_URL, lesson, this.httpUtils.getHTTPHeader());
	}

	// READ
	getAllLessons(): Observable<LessonModel[]> {
		return this.http.get<LessonModel[]>(JSON_PRODUCTS_URL);
	}

	getLessonsByParentId(parentId: number): Observable<LessonModel[]> {
		return this.http.get<LessonModel[]>(JSON_PRODUCTS_URL + `/?courseId=${parentId}`);
	}

	getLessonById(lessonId: number): Observable<any> {
		return this.http.get<LessonModel>(JSON_PRODUCTS_URL + `/${lessonId}?_embed=criteria&_embed=videos&_embed=markers&_embed=files`);
	}

	findLesson(queryParams: QueryParamsModel, parentId?): Observable<QueryResultsModel> {
		const params = this.httpUtils.getFindHTTPParams(queryParams);

		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		if (parentId) {
			return this.getLessonsByParentId(parentId).pipe(
				mergeMap(res => of(new QueryResultsModel(res)))
			);
		} else {
			return this.getAllLessons().pipe(
				mergeMap(res => of(new QueryResultsModel(res)))
			);
		}
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
		// END
	}

	// UPDATE => PUT: update the product on the server
	updateLesson(lesson: LessonModel): Observable<any> {
		return this.http.put(JSON_PRODUCTS_URL + `/${lesson.id}`, lesson, this.httpUtils.getHTTPHeader());
	}

	// UPDATE Status
	// Comment this when you start work with real server
	// This code imitates server calls
	updateStatusForLesson(lessons: LessonModel[], status: number): Observable<any> {
		const tasks$ = [];
		for (let i = 0; i < lessons.length; i++) {
			const _lesson = lessons[i];
			_lesson.status = status;
			tasks$.push(this.updateLesson(_lesson));
		}
		return forkJoin(tasks$);
	}

	// DELETE => delete the product from the server
	deleteLesson(lessonId: number): Observable<LessonModel> {
		const url = `${JSON_PRODUCTS_URL}/${lessonId}`;
		return this.http.delete<LessonModel>(url);
	}

	// Method imitates server calls which deletes items from DB (should rewrite this to real server call)
	// START
	deleteLessons(ids: number[] = []) {
		// Comment this when you start work with real server
		// This code imitates server calls
		// START
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			tasks$.push(this.deleteLesson(ids[i]));
		}
		return forkJoin(tasks$);
		// END

		// Uncomment this when you start work with real server
		// Note: Add headers if needed
		// START
		/*for (let i = 0; i < length; i++) {
			const url = `${JSON_PRODUCTS_URL}/${ids}`;
			return this.http.delete<courseModel>(url);
		}*/
		/*const url = JSON_PRODUCTS_URL + '/delete';
		return this.http.get<QueryResultsModel>(url);*/
		// END
	}
}
