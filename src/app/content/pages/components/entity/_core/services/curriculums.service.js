var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
// const API_PRODUCTS_URL = `https://my-json-server.typicode.com/Xteda87/jsonServer/curriculums`;
var JSON_PRODUCTS_URL = "http://" + location.hostname + ":3000/curriculums";
/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/
var CurriculumsService = /** @class */ (function () {
    function CurriculumsService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.lastFilter$ = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    }
    // CREATE =>  POST: add a new product to the server
    CurriculumsService.prototype.createCurriculum = function (curriculum) {
        return this.http.post(JSON_PRODUCTS_URL, curriculum, this.httpUtils.getHTTPHeader());
    };
    // READ
    CurriculumsService.prototype.getAllCurriculums = function () {
        return this.http.get(JSON_PRODUCTS_URL);
    };
    CurriculumsService.prototype.getCurriculumById = function (curriculumId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/" + curriculumId + "?_embed=criteria&_embed=videos&_embed=markers&_embed=files"));
    };
    CurriculumsService.prototype.findCurriculum = function (queryParams) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        return this.getAllCurriculums().pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    // UPDATE => PUT: update the product on the server
    CurriculumsService.prototype.updateCurriculum = function (curriculum) {
        return this.http.put(JSON_PRODUCTS_URL + ("/" + curriculum.id), curriculum, this.httpUtils.getHTTPHeader());
    };
    // UPDATE Status
    // Comment this when you start work with real server
    // This code imitates server calls
    CurriculumsService.prototype.updateStatusForCurriculum = function (curriculums, status) {
        var tasks$ = [];
        for (var i = 0; i < curriculums.length; i++) {
            var _curriculum = curriculums[i];
            _curriculum.status = status;
            tasks$.push(this.updateCurriculum(_curriculum));
        }
        return forkJoin(tasks$);
    };
    // DELETE => delete the product from the server
    CurriculumsService.prototype.deleteCurriculum = function (curriculumId) {
        var url = JSON_PRODUCTS_URL + "/" + curriculumId;
        return this.http["delete"](url);
    };
    // Method imitates server calls which deletes items from DB (should rewrite this to real server call)
    // START
    CurriculumsService.prototype.deleteProducts = function (ids) {
        if (ids === void 0) { ids = []; }
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        var tasks$ = [];
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            tasks$.push(this.deleteCurriculum(ids[i]));
        }
        return forkJoin(tasks$);
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*for (let i = 0; i < length; i++) {
            const url = `${JSON_PRODUCTS_URL}/${ids}`;
            return this.http.delete<CurriculumModel>(url);
        }*/
        /*const url = JSON_PRODUCTS_URL + '/delete';
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    CurriculumsService = __decorate([
        Injectable()
    ], CurriculumsService);
    return CurriculumsService;
}());
export { CurriculumsService };
