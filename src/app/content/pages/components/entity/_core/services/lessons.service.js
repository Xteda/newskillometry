var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { forkJoin, BehaviorSubject, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-models/query-params.model';
import { QueryResultsModel } from '../models/query-models/query-results.model';
// const API_PRODUCTS_URL = `https://my-json-server.typicode.com/Xteda87/jsonServer/courses`;
var JSON_PRODUCTS_URL = "http://" + location.hostname + ":3000/lessons";
/*const API_PRODUCTS_URL = 'api/products';
const JSON_PRODUCTS_URL = 'api/products';*/
var LessonsService = /** @class */ (function () {
    function LessonsService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.lastFilter$ = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    }
    // CREATE =>  POST: add a new product to the server
    LessonsService.prototype.createLesson = function (lesson) {
        return this.http.post(JSON_PRODUCTS_URL, lesson, this.httpUtils.getHTTPHeader());
    };
    // READ
    LessonsService.prototype.getAllLessons = function () {
        return this.http.get(JSON_PRODUCTS_URL);
    };
    LessonsService.prototype.getLessonsByParentId = function (parentId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/?courseId=" + parentId));
    };
    LessonsService.prototype.getLessonById = function (lessonId) {
        return this.http.get(JSON_PRODUCTS_URL + ("/" + lessonId + "?_embed=criteria&_embed=videos&_embed=markers&_embed=files"));
    };
    LessonsService.prototype.findLesson = function (queryParams, parentId) {
        var params = this.httpUtils.getFindHTTPParams(queryParams);
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        if (parentId) {
            return this.getLessonsByParentId(parentId).pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        }
        else {
            return this.getAllLessons().pipe(mergeMap(function (res) { return of(new QueryResultsModel(res)); }));
        }
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*const url = API_PRODUCTS_URL + "/find";
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    // UPDATE => PUT: update the product on the server
    LessonsService.prototype.updateLesson = function (lesson) {
        return this.http.put(JSON_PRODUCTS_URL + ("/" + lesson.id), lesson, this.httpUtils.getHTTPHeader());
    };
    // UPDATE Status
    // Comment this when you start work with real server
    // This code imitates server calls
    LessonsService.prototype.updateStatusForLesson = function (lessons, status) {
        var tasks$ = [];
        for (var i = 0; i < lessons.length; i++) {
            var _lesson = lessons[i];
            _lesson.status = status;
            tasks$.push(this.updateLesson(_lesson));
        }
        return forkJoin(tasks$);
    };
    // DELETE => delete the product from the server
    LessonsService.prototype.deleteLesson = function (lessonId) {
        var url = JSON_PRODUCTS_URL + "/" + lessonId;
        return this.http["delete"](url);
    };
    // Method imitates server calls which deletes items from DB (should rewrite this to real server call)
    // START
    LessonsService.prototype.deleteLessons = function (ids) {
        if (ids === void 0) { ids = []; }
        // Comment this when you start work with real server
        // This code imitates server calls
        // START
        var tasks$ = [];
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            tasks$.push(this.deleteLesson(ids[i]));
        }
        return forkJoin(tasks$);
        // END
        // Uncomment this when you start work with real server
        // Note: Add headers if needed
        // START
        /*for (let i = 0; i < length; i++) {
            const url = `${JSON_PRODUCTS_URL}/${ids}`;
            return this.http.delete<courseModel>(url);
        }*/
        /*const url = JSON_PRODUCTS_URL + '/delete';
        return this.http.get<QueryResultsModel>(url);*/
        // END
    };
    LessonsService = __decorate([
        Injectable()
    ], LessonsService);
    return LessonsService;
}());
export { LessonsService };
