import { Injectable } from '@angular/core';
import { FileModel } from '../models/file.model';

@Injectable()
export class FileService {
	tempFiles: FileModel[];

	getSavedFiles(): FileModel[] {
		return this.tempFiles;
	}

	setSavedFiles(data) {
		this.tempFiles = data;
	}
}
