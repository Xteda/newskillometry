import * as _ from 'lodash';
export var StateActions;
(function (StateActions) {
    StateActions["CREATE"] = "CREATE";
    StateActions["UPDATE"] = "UPDATE";
    StateActions["DELETE"] = "DELETE";
})(StateActions || (StateActions = {}));
var ListStateModel = /** @class */ (function () {
    function ListStateModel(_entityId) {
        this.deletedItems = [];
        this.updatedItems = [];
        this.addedItems = [];
        this.entityId = _entityId;
    }
    ListStateModel.prototype.setItem = function (_item, actionType) {
        switch (actionType) {
            case StateActions.CREATE:
                this.createItem(_item);
                break;
            case StateActions.UPDATE:
                this.updateItem(_item);
                break;
            default: this.deleteItem(_item);
        }
    };
    ListStateModel.prototype.createItem = function (_item) {
        _item._prevState = _item;
        this.addedItems.push(_item);
    };
    ListStateModel.prototype.updateItem = function (_item) {
        if (!_item.id) {
            var c_index = _.findIndex(this.addedItems, function (o) { return o === _item._prevState; });
            if (c_index > -1) {
                this.addedItems[c_index] = _item;
                this.addedItems[c_index]._prevState = _item;
            }
        }
        else {
            var u_index = _.findIndex(this.updatedItems, function (o) { return o.id === _item.id; });
            if (u_index > -1) {
                this.updatedItems[u_index]._prevState = _item;
                this.updatedItems[u_index] = _item;
            }
            else {
                _item._prevState = _item;
                this.updatedItems.push(_item);
            }
        }
    };
    ListStateModel.prototype.deleteItem = function (_item) {
        if (_item.id) {
            var d_index = _.findIndex(this.deletedItems, function (o) { return o.id === _item.id; });
            var u_index = _.findIndex(this.updatedItems, function (o) { return o.id === _item.id; });
            if (d_index === -1) {
                this.deletedItems.push(_item);
            }
            if (u_index > -1) {
                _.pull(this.updatedItems, _item);
            }
            return;
        }
        if (!_item.id) {
            var c_index = _.findIndex(this.addedItems, function (o) { return o === _item; });
            if (c_index > -1) {
                _.pull(this.addedItems, _item);
            }
        }
    };
    ListStateModel.prototype.prepareState = function () {
        this.addedItems.forEach(function (element) {
            element._prevState = null;
        });
        this.updatedItems.forEach(function (element) {
            element._prevState = null;
        });
        this.deletedItems.forEach(function (element) {
            element._prevState = null;
        });
    };
    return ListStateModel;
}());
export { ListStateModel };
