import { Injectable } from '@angular/core';
import { CriteriaModel} from '../models/criteria.model';

@Injectable()
export class CriteriaService {
	tempCriteria: CriteriaModel;

	getSavedCriteria(): CriteriaModel {
		return this.tempCriteria;
	}

	setSavedCriteria(data) {
		this.tempCriteria = data;
	}
}
