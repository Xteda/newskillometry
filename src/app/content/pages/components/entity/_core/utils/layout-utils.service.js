var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { ActionNotificationComponent } from '../../_shared/action-natification/action-notification.component';
import { DeleteEntityDialogComponent } from '../../_shared/delete-entity-dialog/delete-entity-dialog.component';
import { FetchEntityDialogComponent } from '../../_shared/fetch-entity-dialog/fetch-entity-dialog.component';
import { UpdateStatusDialogComponent } from '../../_shared/update-status-dialog/update-status-dialog.component';
export var MessageType;
(function (MessageType) {
    MessageType[MessageType["Create"] = 0] = "Create";
    MessageType[MessageType["Read"] = 1] = "Read";
    MessageType[MessageType["Update"] = 2] = "Update";
    MessageType[MessageType["Delete"] = 3] = "Delete";
})(MessageType || (MessageType = {}));
var LayoutUtilsService = /** @class */ (function () {
    function LayoutUtilsService(snackBar, dialog) {
        this.snackBar = snackBar;
        this.dialog = dialog;
    }
    // SnackBar for notifications
    LayoutUtilsService.prototype.showActionNotification = function (message, type, duration, showCloseButton, showUndoButton, undoButtonDuration, verticalPosition) {
        if (type === void 0) { type = MessageType.Create; }
        if (duration === void 0) { duration = 10000; }
        if (showCloseButton === void 0) { showCloseButton = true; }
        if (showUndoButton === void 0) { showUndoButton = false; }
        if (undoButtonDuration === void 0) { undoButtonDuration = 3000; }
        if (verticalPosition === void 0) { verticalPosition = 'top'; }
        return this.snackBar.openFromComponent(ActionNotificationComponent, {
            duration: duration,
            data: {
                message: message,
                snackBar: this.snackBar,
                showCloseButton: showCloseButton,
                showUndoButton: showUndoButton,
                undoButtonDuration: undoButtonDuration,
                verticalPosition: verticalPosition,
                type: type,
                action: 'Undo'
            },
            verticalPosition: verticalPosition
        });
    };
    // Method returns instance of MatDialog
    LayoutUtilsService.prototype.deleteElement = function (title, description, waitDesciption) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        if (waitDesciption === void 0) { waitDesciption = ''; }
        return this.dialog.open(DeleteEntityDialogComponent, {
            data: { title: title, description: description, waitDesciption: waitDesciption },
            width: '440px'
        });
    };
    // Method returns instance of MatDialog
    LayoutUtilsService.prototype.fetchElements = function (_data) {
        return this.dialog.open(FetchEntityDialogComponent, {
            data: _data,
            width: '400px'
        });
    };
    // Method returns instance of MatDialog
    LayoutUtilsService.prototype.updateStatusForCustomers = function (title, statuses, messages) {
        return this.dialog.open(UpdateStatusDialogComponent, {
            data: { title: title, statuses: statuses, messages: messages },
            width: '480px'
        });
    };
    LayoutUtilsService = __decorate([
        Injectable()
    ], LayoutUtilsService);
    return LayoutUtilsService;
}());
export { LayoutUtilsService };
