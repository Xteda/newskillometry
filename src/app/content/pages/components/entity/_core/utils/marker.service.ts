import { Injectable } from '@angular/core';
import { MarkerModel} from '../../_core/models/marker.model';
import { LayoutUtilsService, MessageType} from '../../_core/utils/layout-utils.service';
import * as moment from 'moment';
import {FormBuilder} from '@angular/forms';

@Injectable()
export class MarkerService {

	private curriculumFB: FormBuilder;

	constructor(
		private layoutUtilsService: LayoutUtilsService
	) {}

	setCurrentTime(videoObj, markersArray, markerIndex, sec, start?) {
		if (markersArray[markerIndex]._isEditMode) {
			const timeEnd = videoObj.currentTime();
			const markers = videoObj.markers.getMarkers();
			if (start) {
				const displayTime = markersArray[markerIndex].displayTime || 0;
				for (let i = 0; i < markers.length; i++) {
					if (markers[i].id === markersArray[markerIndex].id) {
						markers[i].time = timeEnd;
						break;
					}
				}
				if (timeEnd < displayTime) {
					markersArray[markerIndex].time = timeEnd;
				} else {
					markersArray[markerIndex].time = timeEnd;
					markersArray[markerIndex].displayTime = '';
				}
			} else {
				const startTime = markersArray[markerIndex].time || 0;
				if (timeEnd < startTime) {
					this.layoutUtilsService.showActionNotification('You can\'t add annotation there. Please choose another timer position', MessageType.Create, 10000, true, false);
					return false;
				}
				for (let i = 0; i < markersArray.length; i++) {
					const startTime = markersArray[i].time;
					const endTime = markersArray[i].displayTime;
					if (timeEnd >= startTime && timeEnd <= endTime && i != markerIndex && !markersArray[i]._isDeleted) {
						markersArray[markerIndex].displayTime = startTime;
						this.layoutUtilsService.showActionNotification('You can\'t add annotation there. Please choose another timer position', MessageType.Create, 10000, true, false);
						return false;
					}
				}
				for (let i = 0; i < markers.length; i++) {
					if (markers[i].id === markersArray[markerIndex].id) {
						markers[i].displayTime = timeEnd;
						break;
					}
				}
				markersArray[markerIndex].displayTime = timeEnd;
			}
			videoObj.markers.updateTime();
		} else {
			videoObj.play();
			videoObj.pause();
			videoObj.currentTime(sec);
		}
	}

	markerEdit(videoObj, markersArray, markerIndex, form) {
		if (markersArray[markerIndex]._isEditMode) {
			const markers = videoObj.markers.getMarkers();
			markersArray[markerIndex].text = form.controls.markers.controls[markerIndex].controls['markerTitle'].value;
			markersArray[markerIndex].overlayText = form.controls.markers.controls[markerIndex].controls['markerDescription'].value;
			markersArray[markerIndex]._updatedDate = Date.now();
			markersArray[markerIndex]._isUpdated = true;
			for (let i = 0; i < markers.length; i++) {
				if (markers[i].id === markersArray[markerIndex].id) {
					videoObj.markers.remove([i]);
					break;
				}
			}
			videoObj.markers.add([{
				id: markersArray[markerIndex].id,
				time: markersArray[markerIndex].time,
				text: form.controls.markers.controls[markerIndex].controls['markerTitle'].value,
				overlayText: form.controls.markers.controls[markerIndex].controls['markerDescription'].value,
				_updatedDate: Date.now(),
				displayTime: markersArray[markerIndex].displayTime
			}]);
			this.layoutUtilsService.showActionNotification('Annotation has been saved', MessageType.Create, 10000, true, false);
		}
		markersArray[markerIndex]._isEditMode = !markersArray[markerIndex]._isEditMode;
	}

	markerDelete(videoObj, markersArray, markerIndex) {
		const markers = videoObj.markers.getMarkers();
		for (let i = 0; i < markers.length; i++) {
			if (markers[i].id === markersArray[markerIndex].id) {
				videoObj.markers.remove([i]);
				break;
			}
		}
		markersArray[markerIndex]._isDeleted = true;
		markersArray[markerIndex]._isEditMode = false;
		this.layoutUtilsService.showActionNotification('Annotation has been deleted', MessageType.Create, 10000, true, false);
	}

	addMarker(videoObj, markersArray) {
		for (let i = 0; i < markersArray.length; i++) {
			const curPlayerTime = videoObj.currentTime();
			const startTime = markersArray[i].time;
			const endTime = markersArray[i].displayTime;
			if (curPlayerTime >= startTime && curPlayerTime <= endTime && !markersArray[i]._isDeleted) {
				this.layoutUtilsService.showActionNotification('You can\'t add annotation there. Please choose another timer position', MessageType.Create, 10000, true, false);
				return false;
			}
		}
		markersArray.push({
			time: videoObj.currentTime(),
			text: '',
			overlayText: '',
			_isNew: true,
			_updatedDate: Date.now(),
			_createdDate: Date.now(),
			_isEditMode: true,
			displayTime: ''
		});
		videoObj.markers.add([{
			time: videoObj.currentTime(),
			text: '',
			overlayText: '',
			_isNew: true,
			_updatedDate: Date.now(),
			_createdDate: Date.now(),
			_isEditMode: true,
			displayTime: ''
		}]);
		return true;
	}
}
