var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { MessageType } from '../../_core/utils/layout-utils.service';
var MarkerService = /** @class */ (function () {
    function MarkerService(layoutUtilsService) {
        this.layoutUtilsService = layoutUtilsService;
    }
    MarkerService.prototype.setCurrentTime = function (videoObj, markersArray, markerIndex, sec, start) {
        if (markersArray[markerIndex]._isEditMode) {
            var timeEnd = videoObj.currentTime();
            var markers = videoObj.markers.getMarkers();
            if (start) {
                var displayTime = markersArray[markerIndex].displayTime || 0;
                for (var i = 0; i < markers.length; i++) {
                    if (markers[i].id === markersArray[markerIndex].id) {
                        markers[i].time = timeEnd;
                        break;
                    }
                }
                if (timeEnd < displayTime) {
                    markersArray[markerIndex].time = timeEnd;
                }
                else {
                    markersArray[markerIndex].time = timeEnd;
                    markersArray[markerIndex].displayTime = '';
                }
            }
            else {
                var startTime = markersArray[markerIndex].time || 0;
                if (timeEnd < startTime) {
                    this.layoutUtilsService.showActionNotification('You can\'t add annotation there. Please choose another timer position', MessageType.Create, 10000, true, false);
                    return false;
                }
                for (var i = 0; i < markersArray.length; i++) {
                    var startTime_1 = markersArray[i].time;
                    var endTime = markersArray[i].displayTime;
                    if (timeEnd >= startTime_1 && timeEnd <= endTime && i != markerIndex && !markersArray[i]._isDeleted) {
                        markersArray[markerIndex].displayTime = startTime_1;
                        this.layoutUtilsService.showActionNotification('You can\'t add annotation there. Please choose another timer position', MessageType.Create, 10000, true, false);
                        return false;
                    }
                }
                for (var i = 0; i < markers.length; i++) {
                    if (markers[i].id === markersArray[markerIndex].id) {
                        markers[i].displayTime = timeEnd;
                        break;
                    }
                }
                markersArray[markerIndex].displayTime = timeEnd;
            }
            videoObj.markers.updateTime();
        }
        else {
            videoObj.play();
            videoObj.pause();
            videoObj.currentTime(sec);
        }
    };
    MarkerService.prototype.markerEdit = function (videoObj, markersArray, markerIndex, form) {
        if (markersArray[markerIndex]._isEditMode) {
            var markers = videoObj.markers.getMarkers();
            markersArray[markerIndex].text = form.controls.markers.controls[markerIndex].controls['markerTitle'].value;
            markersArray[markerIndex].overlayText = form.controls.markers.controls[markerIndex].controls['markerDescription'].value;
            markersArray[markerIndex]._updatedDate = Date.now();
            markersArray[markerIndex]._isUpdated = true;
            for (var i = 0; i < markers.length; i++) {
                if (markers[i].id === markersArray[markerIndex].id) {
                    videoObj.markers.remove([i]);
                    break;
                }
            }
            videoObj.markers.add([{
                    id: markersArray[markerIndex].id,
                    time: markersArray[markerIndex].time,
                    text: form.controls.markers.controls[markerIndex].controls['markerTitle'].value,
                    overlayText: form.controls.markers.controls[markerIndex].controls['markerDescription'].value,
                    _updatedDate: Date.now(),
                    displayTime: markersArray[markerIndex].displayTime
                }]);
            this.layoutUtilsService.showActionNotification('Annotation has been saved', MessageType.Create, 10000, true, false);
        }
        markersArray[markerIndex]._isEditMode = !markersArray[markerIndex]._isEditMode;
    };
    MarkerService.prototype.markerDelete = function (videoObj, markersArray, markerIndex) {
        var markers = videoObj.markers.getMarkers();
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].id === markersArray[markerIndex].id) {
                videoObj.markers.remove([i]);
                break;
            }
        }
        markersArray[markerIndex]._isDeleted = true;
        markersArray[markerIndex]._isEditMode = false;
        this.layoutUtilsService.showActionNotification('Annotation has been deleted', MessageType.Create, 10000, true, false);
    };
    MarkerService.prototype.addMarker = function (videoObj, markersArray) {
        for (var i = 0; i < markersArray.length; i++) {
            var curPlayerTime = videoObj.currentTime();
            var startTime = markersArray[i].time;
            var endTime = markersArray[i].displayTime;
            if (curPlayerTime >= startTime && curPlayerTime <= endTime && !markersArray[i]._isDeleted) {
                this.layoutUtilsService.showActionNotification('You can\'t add annotation there. Please choose another timer position', MessageType.Create, 10000, true, false);
                return false;
            }
        }
        markersArray.push({
            time: videoObj.currentTime(),
            text: '',
            overlayText: '',
            _isNew: true,
            _updatedDate: Date.now(),
            _createdDate: Date.now(),
            _isEditMode: true,
            displayTime: ''
        });
        videoObj.markers.add([{
                time: videoObj.currentTime(),
                text: '',
                overlayText: '',
                _isNew: true,
                _updatedDate: Date.now(),
                _createdDate: Date.now(),
                _isEditMode: true,
                displayTime: ''
            }]);
        return true;
    };
    MarkerService = __decorate([
        Injectable()
    ], MarkerService);
    return MarkerService;
}());
export { MarkerService };
