var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders } from '@angular/common/http';
var HttpUtilsService = /** @class */ (function () {
    function HttpUtilsService() {
    }
    HttpUtilsService.prototype.getFindHTTPParams = function (queryParams) {
        var params = new HttpParams()
            .set('lastNamefilter', queryParams.filter)
            .set('sortOrder', queryParams.sortOrder)
            .set('sortField', queryParams.sortField)
            .set('pageNumber', queryParams.pageNumber.toString())
            .set('pageSize', queryParams.pageSize.toString());
        return params;
    };
    HttpUtilsService.prototype.getHTTPHeader = function () {
        return {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
    };
    HttpUtilsService = __decorate([
        Injectable()
    ], HttpUtilsService);
    return HttpUtilsService;
}());
export { HttpUtilsService };
