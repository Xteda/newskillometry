var BaseEntityModel = /** @class */ (function () {
    function BaseEntityModel() {
        // Edit
        this._isEditMode = false;
        this._isNew = false;
        this._isUpdated = false;
        this._isDeleted = false;
        this._prevState = null;
        // Log
        this._userId = 0; // Admin
    }
    return BaseEntityModel;
}());
export { BaseEntityModel };
