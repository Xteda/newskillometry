var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BaseEntityModel } from './_base-entity.model';
var CurriculumModel = /** @class */ (function (_super) {
    __extends(CurriculumModel, _super);
    function CurriculumModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CurriculumModel.prototype.clear = function () {
        this.title = '';
        this.author = '';
        this.description = '';
        this.status = 0;
        this.children = 0;
        this.hasVideo = false;
    };
    return CurriculumModel;
}(BaseEntityModel));
export { CurriculumModel };
