import { BaseModel } from './_base.model';

export class MarkerModel extends BaseModel {
	id?: number;
	curriculumId?: number;
	courseId?: number;
	lessonId?: number;
	instructionId?: number;
	time: number;
	displayTime: number;
	text: string;
	overlayText: string;

	clear(parentId: number) {
		this.id = undefined;
		this.curriculumId = parentId;
		this.courseId = parentId;
		this.lessonId = parentId;
		this.instructionId = parentId;
		this.time = undefined;
		this.text = '';
		this.displayTime = undefined;
		this.overlayText = '';
	}
}
