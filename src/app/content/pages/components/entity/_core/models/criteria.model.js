var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BaseModel } from './_base.model';
var CriteriaModel = /** @class */ (function (_super) {
    __extends(CriteriaModel, _super);
    function CriteriaModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CriteriaModel.prototype.clear = function (parentId) {
        this.id = undefined;
        this.curriculumId = parentId;
        this.courseId = parentId;
        this.lessonId = parentId;
        this.instructionId = parentId;
        this._specificationName = '';
        this.competencyNumCorrect = 0;
        this.competencyTrialLimit = 0;
        this.competencyTrialNumInput = 0;
        this.maintenanceEnable = false;
        this.maintenancePicker = '';
        this.maintenanceNumCorrect = 0;
        this.maintenanceTrialLimit = 0;
        this.maintenanceTrialNumInput = 0;
    };
    return CriteriaModel;
}(BaseModel));
export { CriteriaModel };
