import { BaseModel } from './_base.model';

export class CriteriaModel extends BaseModel {
	id?: number;
	curriculumId?: number;
	courseId?: number;
	lessonId?: number;
	instructionId?: number;
	competencyNumCorrect: number;
	competencyTrialLimit: number;
	competencyTrialNumInput: number;
	maintenanceEnable: boolean;
	maintenancePicker: string;
	maintenanceNumCorrect: number;
	maintenanceTrialLimit: number;
	maintenanceTrialNumInput: number;

	// Refs
	_specificationName: string;

	clear(parentId: number) {
		this.id = undefined;
		this.curriculumId = parentId;
		this.courseId = parentId;
		this.lessonId = parentId;
		this.instructionId = parentId;
		this._specificationName = '';
		this.competencyNumCorrect = 0;
		this.competencyTrialLimit = 0;
		this.competencyTrialNumInput = 0;
		this.maintenanceEnable = false;
		this.maintenancePicker = '';
		this.maintenanceNumCorrect = 0;
		this.maintenanceTrialLimit = 0;
		this.maintenanceTrialNumInput = 0;
	}
}
