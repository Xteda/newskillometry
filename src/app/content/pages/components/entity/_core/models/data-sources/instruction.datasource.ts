import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { InstructionService} from '../../services/instruction.service';
import { QueryParamsModel } from '../query-models/query-params.model';
import { BaseDataSource } from './_base.datasource';
import { QueryResultsModel } from '../query-models/query-results.model';

export class InstructionDatasource extends BaseDataSource {
	constructor(private instructionsService: InstructionService) {
		super();
	}

	loadProducts(queryParams: QueryParamsModel, parentId?) {
		this.instructionsService.lastFilter$.next(queryParams);
		this.loadingSubject.next(true);

		this.instructionsService.findInstruction(queryParams, parentId)
			.pipe(
				tap(res => {
					// Comment this when you start work with real server
					// This code imitates server calls
					// START
					const result = this.baseFilter(res.items, queryParams, ['_createdDate']);
					this.entitySubject.next(result.items);
					this.paginatorTotalSubject.next(result.totalCount);
					// END

					// Uncomment this when you start work with real server
					// START

					/*this.entitySubject.next(res.items);
                    this.paginatorTotalSubject.next(res.totalCount);*/
					// END
				}),
				catchError(err => of(new QueryResultsModel([], err))),
				finalize(() => this.loadingSubject.next(false))
			).subscribe();
	}
}
