var QueryResultsModel = /** @class */ (function () {
    function QueryResultsModel(_items, _errorMessage) {
        if (_items === void 0) { _items = []; }
        if (_errorMessage === void 0) { _errorMessage = ''; }
        this.items = _items;
        this.totalCount = _items.length;
    }
    return QueryResultsModel;
}());
export { QueryResultsModel };
