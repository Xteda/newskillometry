import { BehaviorSubject } from 'rxjs';
import { QueryResultsModel } from '../query-models/query-results.model';
import * as _ from 'lodash';
// Why not use MatTableDataSource?
/*  In this example, we will not be using the built-in MatTableDataSource because its designed for filtering,
    sorting and pagination of a client - side data array.
    Read the article: 'https://blog.angular-university.io/angular-material-data-table/'
**/
var BaseDataSource = /** @class */ (function () {
    function BaseDataSource() {
        var _this = this;
        this.entitySubject = new BehaviorSubject([]);
        this.hasItems = false; // Need to show message: 'No records found
        // Loading | Progress bar
        this.loadingSubject = new BehaviorSubject(false);
        // Paginator | Paginators count
        this.paginatorTotalSubject = new BehaviorSubject(0);
        this.loading$ = this.loadingSubject.asObservable();
        this.paginatorTotal$ = this.paginatorTotalSubject.asObservable();
        this.paginatorTotal$.subscribe(function (res) { return _this.hasItems = res > 0; });
    }
    BaseDataSource.prototype.connect = function (collectionViewer) {
        // Connecting data source
        return this.entitySubject.asObservable();
    };
    BaseDataSource.prototype.disconnect = function (collectionViewer) {
        // Disonnecting data source
        this.entitySubject.complete();
        this.loadingSubject.complete();
        this.paginatorTotalSubject.complete();
    };
    BaseDataSource.prototype.baseFilter = function (_entities, _queryParams, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        // Filtration
        var entitiesResult = this.searchInArray(_entities, _queryParams.filter, _filtrationFields);
        // Sorting
        // start
        if (_queryParams.sortField) {
            entitiesResult = this.sortArray(entitiesResult, _queryParams.sortField, _queryParams.sortOrder);
        }
        // end
        // Paginator
        // start
        var totalCount = entitiesResult.length;
        var initialPos = _queryParams.pageNumber * _queryParams.pageSize;
        entitiesResult = entitiesResult.slice(initialPos, initialPos + _queryParams.pageSize);
        // end
        var queryResults = new QueryResultsModel();
        queryResults.items = entitiesResult;
        queryResults.totalCount = totalCount;
        return queryResults;
    };
    BaseDataSource.prototype.sortArray = function (_incomingArray, _sortField, _sortOrder) {
        if (_sortField === void 0) { _sortField = ''; }
        if (_sortOrder === void 0) { _sortOrder = 'asc'; }
        if (!_sortField) {
            return _incomingArray;
        }
        switch (_sortField) {
            case 'create':
                _sortField = '_createdDate';
                break;
            case 'update':
                _sortField = '_updatedDate';
                break;
            default: break;
        }
        var result = [];
        result = _incomingArray.sort(function (a, b) {
            if (a[_sortField] < b[_sortField]) {
                return _sortOrder === 'asc' ? -1 : 1;
            }
            if (a[_sortField] > b[_sortField]) {
                return _sortOrder === 'asc' ? 1 : -1;
            }
            return 0;
        });
        return result;
    };
    BaseDataSource.prototype.searchInArray = function (_incomingArray, _queryObj, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        var result = [];
        var resultBuffer = [];
        var indexes = [];
        var firstIndexes = [];
        var doSearch = false;
        _filtrationFields.forEach(function (item) {
            if (item in _queryObj) {
                _incomingArray.forEach(function (element, index) {
                    if (element[item] === _queryObj[item]) {
                        firstIndexes.push(index);
                    }
                });
                firstIndexes.forEach(function (element) {
                    resultBuffer.push(_incomingArray[element]);
                });
                _incomingArray = resultBuffer.slice(0);
                resultBuffer = [].slice(0);
                firstIndexes = [].slice(0);
            }
        });
        Object.keys(_queryObj).forEach(function (key) {
            var searchText = _queryObj[key].toString().trim().toLowerCase();
            if (key && !_.includes(_filtrationFields, key) && searchText) {
                doSearch = true;
                try {
                    _incomingArray.forEach(function (element, index) {
                        var _val = element[key].toString().trim().toLowerCase();
                        if (_val.indexOf(searchText) > -1 && indexes.indexOf(index) === -1) {
                            indexes.push(index);
                        }
                    });
                }
                catch (ex) {
                    console.log(ex, key, searchText);
                }
            }
        });
        if (!doSearch) {
            return _incomingArray;
        }
        indexes.forEach(function (re) {
            result.push(_incomingArray[re]);
        });
        return result;
    };
    return BaseDataSource;
}());
export { BaseDataSource };
