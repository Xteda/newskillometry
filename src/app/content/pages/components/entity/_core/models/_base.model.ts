import { IEdit } from './interfaces/edit.interface';
import { ILog } from './interfaces/log.interface';

export class BaseModel implements IEdit, ILog {
	// Edit
	_isEditMode: boolean = false;
	_isNew: boolean = false;
	_isUpdated: boolean = false;
	_isDeleted: boolean = false;
	_prevState: any = null;
	// Log
	_userId: number = 0; // Admin
	_createdDate: number;
	_updatedDate: number;
}
