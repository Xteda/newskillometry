import {BaseEntityModel} from './_base-entity.model';

export class LessonModel extends BaseEntityModel {

	curriculumId?: number;
	courseId?: number;

	clear(parentId: number) {
		this.title = '';
		this.author = '';
		this.description = '';
		this.status = 0;
		this.children = 0;
		this.hasVideo = false;
		this.curriculumId = parentId;
		this.courseId = parentId;
	}
}
