var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BaseModel } from './_base.model';
var FileModel = /** @class */ (function (_super) {
    __extends(FileModel, _super);
    function FileModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FileModel.prototype.clear = function (parentId) {
        this.id = undefined;
        this.curriculumId = parentId;
        this.type = '';
        this.title = '';
        this.link = '';
        this.size = '';
        this.curriculumId = parentId;
        this.courseId = parentId;
        this.lessonId = parentId;
        this.instructionId = parentId;
    };
    return FileModel;
}(BaseModel));
export { FileModel };
