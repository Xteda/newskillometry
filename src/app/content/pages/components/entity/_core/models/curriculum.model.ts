import {BaseEntityModel} from './_base-entity.model';

export class CurriculumModel extends BaseEntityModel {

	clear() {
		this.title = '';
		this.author = '';
		this.description = '';
		this.status = 0;
		this.children = 0;
		this.hasVideo = false;
	}
}
