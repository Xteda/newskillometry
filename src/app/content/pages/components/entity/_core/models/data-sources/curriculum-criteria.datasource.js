var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { from } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { BaseDataSource } from './_base.datasource';
var CurriculumCriteriaDatasource = /** @class */ (function (_super) {
    __extends(CurriculumCriteriaDatasource, _super);
    function CurriculumCriteriaDatasource(productRemarksService) {
        var _this = _super.call(this) || this;
        _this.productRemarksService = productRemarksService;
        return _this;
    }
    CurriculumCriteriaDatasource.prototype.loadRemarks = function (queryParams, lastState) {
        var _this = this;
        this.loadingSubject.next(true);
        this.productRemarksService.findCriteria(queryParams, lastState.entityId)
            .pipe(catchError(function () { return from([]); }), finalize(function () { return _this.loadingSubject.next(false); })).subscribe(function (criteria) {
            // comment this, when you work with real server
            // start
            // Remove deletedItems
            // tslint:disable-next-line:prefer-const
            var filteredResult = [];
            if (lastState.deletedItems.length > 0) {
                criteria.forEach(function (element) {
                    var d_index = _.findIndex(lastState.deletedItems, function (o) { return o.id === element.id; });
                    if (d_index === -1) {
                        filteredResult.push(element);
                    }
                });
            }
            else {
                filteredResult = criteria;
            }
            // Update: Updated Items
            if (lastState.updatedItems.length > 0) {
                filteredResult.forEach(function (element) {
                    var _rem = _.find(lastState.updatedItems, function (o) { return o.id === element.id; });
                    if (_rem) {
                        element.text = _rem.text;
                    }
                });
            }
            // Add: New
            if (lastState.addedItems.length > 0) {
                lastState.addedItems.forEach(function (element) {
                    filteredResult.push(element);
                });
            }
            var result = _this.baseFilter(filteredResult, queryParams, []);
            _this.entitySubject.next(result.items);
            _this.paginatorTotalSubject.next(result.totalCount);
            // end
            // uncomment this, when you work with real server
            // start
            // this.entitySubject.next(remarks);
            // this.paginatorTotalSubject.next(res.totalCount);
            // end
        });
    };
    return CurriculumCriteriaDatasource;
}(BaseDataSource));
export { CurriculumCriteriaDatasource };
