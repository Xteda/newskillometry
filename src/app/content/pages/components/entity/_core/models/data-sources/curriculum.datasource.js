var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { BaseDataSource } from './_base.datasource';
import { QueryResultsModel } from '../query-models/query-results.model';
var CurriculumDatasource = /** @class */ (function (_super) {
    __extends(CurriculumDatasource, _super);
    function CurriculumDatasource(curriculumService) {
        var _this = _super.call(this) || this;
        _this.curriculumService = curriculumService;
        return _this;
    }
    CurriculumDatasource.prototype.loadProducts = function (queryParams) {
        var _this = this;
        this.curriculumService.lastFilter$.next(queryParams);
        this.loadingSubject.next(true);
        this.curriculumService.findCurriculum(queryParams)
            .pipe(tap(function (res) {
            // Comment this when you start work with real server
            // This code imitates server calls
            // START
            var result = _this.baseFilter(res.items, queryParams, ['_createdDate']);
            _this.entitySubject.next(result.items);
            _this.paginatorTotalSubject.next(result.totalCount);
            // END
            // Uncomment this when you start work with real server
            // START
            /*this.entitySubject.next(res.items);
            this.paginatorTotalSubject.next(res.totalCount);*/
            // END
        }), catchError(function (err) { return of(new QueryResultsModel([], err)); }), finalize(function () { return _this.loadingSubject.next(false); })).subscribe();
    };
    return CurriculumDatasource;
}(BaseDataSource));
export { CurriculumDatasource };
