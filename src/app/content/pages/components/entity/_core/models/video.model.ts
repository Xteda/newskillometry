import { BaseModel } from './_base.model';

export class VideoModel extends BaseModel {
	id?: number;
	videoUrl: string;
	poster: string;
	curriculumId?: number;
	courseId?: number;
	lessonId?: number;
	instructionId?: number;

	clear(parentId: number) {
		this.id = undefined;
		this.curriculumId = parentId;
		this.courseId = parentId;
		this.lessonId = parentId;
		this.instructionId = parentId;
		this.poster = '';
		this.videoUrl = '';
	}
}
