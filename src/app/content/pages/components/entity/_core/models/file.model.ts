import { BaseModel } from './_base.model';

export class FileModel extends BaseModel {
	id?: number;
	type: string;
	size: string;
	title: string;
	link: string;
	curriculumId?: number;
	courseId?: number;
	lessonId?: number;
	instructionId?: number;

	clear(parentId: number) {
		this.id = undefined;
		this.curriculumId = parentId;
		this.type = '';
		this.title = '';
		this.link = '';
		this.size = '';
		this.curriculumId = parentId;
		this.courseId = parentId;
		this.lessonId = parentId;
		this.instructionId = parentId;
	}
}
