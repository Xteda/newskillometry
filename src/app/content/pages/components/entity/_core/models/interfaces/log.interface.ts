export interface ILog {
	_userId: number; // user who did changes
	_createdDate: number; // date when entity were created => format: 'mm/dd/yyyy'
	_updatedDate: number; // date when changed were applied => format: 'mm/dd/yyyy'
}
