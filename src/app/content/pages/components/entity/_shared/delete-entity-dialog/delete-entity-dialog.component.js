var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
var DeleteEntityDialogComponent = /** @class */ (function () {
    function DeleteEntityDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.viewLoading = false;
    }
    DeleteEntityDialogComponent.prototype.ngOnInit = function () {
    };
    DeleteEntityDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    DeleteEntityDialogComponent.prototype.onYesClick = function () {
        var _this = this;
        /* Server loading imitation. Remove this */
        this.viewLoading = true;
        setTimeout(function () {
            _this.dialogRef.close(true); // Keep only this row
        }, 2500);
    };
    DeleteEntityDialogComponent = __decorate([
        Component({
            selector: 'm-delete-entity-dialog',
            templateUrl: './delete-entity-dialog.component.html'
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], DeleteEntityDialogComponent);
    return DeleteEntityDialogComponent;
}());
export { DeleteEntityDialogComponent };
