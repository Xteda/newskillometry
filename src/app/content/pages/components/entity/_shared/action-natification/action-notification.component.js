var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
var ActionNotificationComponent = /** @class */ (function () {
    function ActionNotificationComponent(data) {
        this.data = data;
    }
    ActionNotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.data.showUndoButton || (this.data.undoButtonDuration >= this.data.duration)) {
            return;
        }
        this.delayForUndoButton(this.data.undoButtonDuration).subscribe(function () {
            _this.data.showUndoButton = false;
        });
    };
    ActionNotificationComponent.prototype.delayForUndoButton = function (timeToDelay) {
        return of('').pipe(delay(timeToDelay));
    };
    ActionNotificationComponent.prototype.onDismissWithAction = function () { this.data.snackBar.dismiss(); };
    ActionNotificationComponent.prototype.onDismiss = function () { this.data.snackBar.dismiss(); };
    ActionNotificationComponent = __decorate([
        Component({
            selector: 'm-action-natification',
            templateUrl: './action-notification.component.html',
            changeDetection: ChangeDetectionStrategy.Default
        }),
        __param(0, Inject(MAT_SNACK_BAR_DATA))
    ], ActionNotificationComponent);
    return ActionNotificationComponent;
}());
export { ActionNotificationComponent };
