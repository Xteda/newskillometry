var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';
var UpdateStatusDialogComponent = /** @class */ (function () {
    function UpdateStatusDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.selectedStatusForUpdate = new FormControl('');
        this.viewLoading = false;
        this.loadingAfterSubmit = false;
    }
    UpdateStatusDialogComponent.prototype.ngOnInit = function () { };
    UpdateStatusDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    UpdateStatusDialogComponent.prototype.updateStatus = function () {
        if (this.selectedStatusForUpdate.value.length === 0) {
            return;
        }
        this.dialogRef.close(this.selectedStatusForUpdate.value); // Keep only this row
    };
    UpdateStatusDialogComponent = __decorate([
        Component({
            selector: 'm-update-status-dialog',
            templateUrl: './update-status-dialog.component.html'
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], UpdateStatusDialogComponent);
    return UpdateStatusDialogComponent;
}());
export { UpdateStatusDialogComponent };
