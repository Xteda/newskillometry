import {
	Component,
	OnInit,
	Input, ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
// Material
import { MatDialog } from '@angular/material';
// Services
import { LayoutUtilsService, MessageType } from '../../_core/utils/layout-utils.service';
import { FileService} from '../../_core/utils/file.service';
// Models
import { FileModel} from '../../_core/models/file.model';

import * as _ from 'lodash';
import {DropzoneComponent, DropzoneConfigInterface, DropzoneDirective} from 'ngx-dropzone-wrapper';
import {AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask} from 'angularfire2/storage';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';

// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'm-resources',
	templateUrl: './resources.component.html'
	// changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResourcesComponent implements OnInit {
	public files: FileModel[];
	public oldFiles: FileModel[];
	dropzone;

	@Input() dataFiles;
	@ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;
	@ViewChild(DropzoneDirective) directiveRef?: DropzoneDirective;

	ref: AngularFireStorageReference;
	// Main task
	task: AngularFireUploadTask;

	// Progress monitoring
	percentage: Observable<number>;

	snapshot: Observable<any>;

	// Download URL
	downloadURL: Observable<string>;

	// State for dropzone CSS toggling
	isHovering: boolean;


	public dropzoneConfig: DropzoneConfigInterface = {
		clickable: true,
		maxFiles: 10,
		addRemoveLinks: true,
		acceptedFiles: 'application/pdf, application/msword',
		autoReset: null,
		errorReset: null,
		cancelReset: null
	};

	constructor(private activatedRoute: ActivatedRoute,
				private fileService: FileService,
				private router: Router,
				private fb: FormBuilder,
				public dialog: MatDialog,
				private storage: AngularFireStorage,
				private layoutUtilsService: LayoutUtilsService) { }

	ngOnInit() {
		// this.dropzone = this.componentRef.directiveRef.dropzone();
		if (typeof this.dataFiles !== 'undefined') {
			this.files = this.dataFiles.map(x => Object.assign({}, x));
		} else {
			const newFiles = new Array<FileModel>();
			this.files = newFiles;
			this.oldFiles = Object.assign({}, newFiles);
		}
	}

	toggleHover(event: boolean) {
		this.isHovering = event;
	}
	startUpload(event: FileList) {
		// The File object
		const file = event.item(0);

		// Client-side validation example
		if (file.type !== 'application/pdf') {
			console.error('unsupported file type :( ');
			return;
		}

		// The storage path
		const path = `test/${new Date().getTime()}_${file.name}`;

		this.ref = this.storage.ref(file.name);
		// The main task
		this.task = this.ref.put(file);

		// Progress monitoring
		this.percentage = this.task.percentageChanges();
		this.snapshot   = this.task.snapshotChanges();

		// The file's download URL
		this.task.snapshotChanges().pipe(
			finalize(() => {
				this.downloadURL = this.storage.ref(file.name).getDownloadURL();
				this.downloadURL.subscribe(res => {
					setTimeout(() => {
						this.onUploadSuccess(file, res);
						window.dispatchEvent(new Event('resize'));
					}, 10);
				});
			})
		).subscribe();

	}

	// Determines if the upload task is active
	isActive(snapshot) {
		return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
	}

	deleteFile(fileIndex) {
		console.log(this.files[fileIndex].link);
		this.storage.storage.refFromURL(this.files[fileIndex].link).delete();
		this.files[fileIndex]._isDeleted = true;
		this.fileService.setSavedFiles(this.files);
		this.layoutUtilsService.showActionNotification('File has been deleted', MessageType.Create, 10000, true, false);
	}

	onUploadSuccess(file, resLink) {
		const newFile = new FileModel();
		newFile.title = file.name;
		newFile.link = resLink;
		newFile._isNew = true;
		newFile._updatedDate = file.lastModified;
		newFile.size = this.formatBytes(file.size);
		newFile.type = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length) || 'default';
		this.files.push(newFile);
		this.fileService.setSavedFiles(this.files);
		this.layoutUtilsService.showActionNotification('File has been added', MessageType.Create, 10000, true, false);
	}

	removeEvent(file) {
		console.log(file);
	}

	private formatBytes(bytes, decimals?) {
		if (bytes === 0) { return '0 Bytes'; }
		const k = 1024,
			dm = decimals || 2,
			sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
			i = Math.floor(Math.log(bytes) / Math.log(k));
		return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
	}
}
