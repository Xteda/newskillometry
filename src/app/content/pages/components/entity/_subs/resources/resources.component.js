var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, ViewChild, } from '@angular/core';
// Services
import { MessageType } from '../../_core/utils/layout-utils.service';
// Models
import { FileModel } from '../../_core/models/file.model';
import { DropzoneComponent, DropzoneDirective } from 'ngx-dropzone-wrapper';
import { finalize } from 'rxjs/operators';
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
var ResourcesComponent = /** @class */ (function () {
    function ResourcesComponent(activatedRoute, fileService, router, fb, dialog, storage, layoutUtilsService) {
        this.activatedRoute = activatedRoute;
        this.fileService = fileService;
        this.router = router;
        this.fb = fb;
        this.dialog = dialog;
        this.storage = storage;
        this.layoutUtilsService = layoutUtilsService;
        this.dropzoneConfig = {
            clickable: true,
            maxFiles: 10,
            addRemoveLinks: true,
            acceptedFiles: 'application/pdf, application/msword',
            autoReset: null,
            errorReset: null,
            cancelReset: null
        };
    }
    ResourcesComponent.prototype.ngOnInit = function () {
        // this.dropzone = this.componentRef.directiveRef.dropzone();
        if (typeof this.dataFiles !== 'undefined') {
            this.files = this.dataFiles.map(function (x) { return Object.assign({}, x); });
        }
        else {
            var newFiles = new Array();
            this.files = newFiles;
            this.oldFiles = Object.assign({}, newFiles);
        }
    };
    ResourcesComponent.prototype.toggleHover = function (event) {
        this.isHovering = event;
    };
    ResourcesComponent.prototype.startUpload = function (event) {
        var _this = this;
        // The File object
        var file = event.item(0);
        // Client-side validation example
        if (file.type !== 'application/pdf') {
            console.error('unsupported file type :( ');
            return;
        }
        // The storage path
        var path = "test/" + new Date().getTime() + "_" + file.name;
        this.ref = this.storage.ref(file.name);
        // The main task
        this.task = this.ref.put(file);
        // Progress monitoring
        this.percentage = this.task.percentageChanges();
        this.snapshot = this.task.snapshotChanges();
        // The file's download URL
        this.task.snapshotChanges().pipe(finalize(function () {
            _this.downloadURL = _this.storage.ref(file.name).getDownloadURL();
            _this.downloadURL.subscribe(function (res) {
                setTimeout(function () {
                    _this.onUploadSuccess(file, res);
                    window.dispatchEvent(new Event('resize'));
                }, 10);
            });
        })).subscribe();
    };
    // Determines if the upload task is active
    ResourcesComponent.prototype.isActive = function (snapshot) {
        return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
    };
    ResourcesComponent.prototype.deleteFile = function (fileIndex) {
        console.log(this.files[fileIndex].link);
        this.storage.storage.refFromURL(this.files[fileIndex].link)["delete"]();
        this.files[fileIndex]._isDeleted = true;
        this.fileService.setSavedFiles(this.files);
        this.layoutUtilsService.showActionNotification('File has been deleted', MessageType.Create, 10000, true, false);
    };
    ResourcesComponent.prototype.onUploadSuccess = function (file, resLink) {
        var newFile = new FileModel();
        newFile.title = file.name;
        newFile.link = resLink;
        newFile._isNew = true;
        newFile._updatedDate = file.lastModified;
        newFile.size = this.formatBytes(file.size);
        newFile.type = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length) || 'default';
        this.files.push(newFile);
        this.fileService.setSavedFiles(this.files);
        this.layoutUtilsService.showActionNotification('File has been added', MessageType.Create, 10000, true, false);
    };
    ResourcesComponent.prototype.removeEvent = function (file) {
        console.log(file);
    };
    ResourcesComponent.prototype.formatBytes = function (bytes, decimals) {
        if (bytes === 0) {
            return '0 Bytes';
        }
        var k = 1024, dm = decimals || 2, sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'], i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };
    __decorate([
        Input()
    ], ResourcesComponent.prototype, "dataFiles");
    __decorate([
        ViewChild(DropzoneComponent)
    ], ResourcesComponent.prototype, "componentRef");
    __decorate([
        ViewChild(DropzoneDirective)
    ], ResourcesComponent.prototype, "directiveRef");
    ResourcesComponent = __decorate([
        Component({
            selector: 'm-resources',
            templateUrl: './resources.component.html'
            // changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], ResourcesComponent);
    return ResourcesComponent;
}());
export { ResourcesComponent };
