var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive, HostListener, Output, EventEmitter } from '@angular/core';
var DropZoneDirective = /** @class */ (function () {
    function DropZoneDirective() {
        this.dropped = new EventEmitter();
        this.hovered = new EventEmitter();
    }
    DropZoneDirective.prototype.onDrop = function ($event) {
        $event.preventDefault();
        this.dropped.emit($event.dataTransfer.files);
        this.hovered.emit(false);
    };
    DropZoneDirective.prototype.onDragOver = function ($event) {
        $event.preventDefault();
        this.hovered.emit(true);
    };
    DropZoneDirective.prototype.onDragLeave = function ($event) {
        $event.preventDefault();
        this.hovered.emit(false);
    };
    __decorate([
        Output()
    ], DropZoneDirective.prototype, "dropped");
    __decorate([
        Output()
    ], DropZoneDirective.prototype, "hovered");
    __decorate([
        HostListener('drop', ['$event'])
    ], DropZoneDirective.prototype, "onDrop");
    __decorate([
        HostListener('dragover', ['$event'])
    ], DropZoneDirective.prototype, "onDragOver");
    __decorate([
        HostListener('dragleave', ['$event'])
    ], DropZoneDirective.prototype, "onDragLeave");
    DropZoneDirective = __decorate([
        Directive({
            selector: '[dropZone]'
        })
    ], DropZoneDirective);
    return DropZoneDirective;
}());
export { DropZoneDirective };
