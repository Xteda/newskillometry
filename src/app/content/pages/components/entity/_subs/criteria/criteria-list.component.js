var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
// Models
import { CriteriaModel } from '../../_core/models/criteria.model';
export var TrialLimit;
(function (TrialLimit) {
    TrialLimit[TrialLimit["Unlimited"] = 0] = "Unlimited";
    TrialLimit[TrialLimit["Lesson trial limit"] = 1] = "Lesson trial limit";
    TrialLimit[TrialLimit["Lesson trial minimum requirement"] = 2] = "Lesson trial minimum requirement";
})(TrialLimit || (TrialLimit = {}));
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
var CriteriaComponent = /** @class */ (function () {
    function CriteriaComponent(activatedRoute, criteriaInnerService, router, criteriaService, fb, dialog, typesUtilsService) {
        this.activatedRoute = activatedRoute;
        this.criteriaInnerService = criteriaInnerService;
        this.router = router;
        this.criteriaService = criteriaService;
        this.fb = fb;
        this.dialog = dialog;
        this.typesUtilsService = typesUtilsService;
        // Selection
        this.selection = new SelectionModel(true, []);
        this.trialLimitEnum = TrialLimit;
    }
    /** LOAD DATA */
    CriteriaComponent.prototype.ngOnInit = function () {
        if (typeof this.dataCriteria !== 'undefined') {
            this.criteria = Object.assign({}, this.dataCriteria);
        }
        else {
            var newCriteria = new CriteriaModel();
            this.criteria = newCriteria;
            this.oldCriteria = Object.assign({}, newCriteria);
        }
        this.createFormGroup();
        this.onFieldChanges();
    };
    CriteriaComponent.prototype.ngOnChanges = function (changes) {
        for (var propName in changes) {
            if (propName === 'dataCriteria' && this.criteria) {
                this.criteria = Object.assign({}, this.dataCriteria);
                this.createFormGroup();
                this.onFieldChanges();
            }
        }
    };
    CriteriaComponent.prototype.onFieldChanges = function () {
        var _this = this;
        this.formGroup.valueChanges.subscribe(function (val) {
            var objCriteria = {
                id: _this.criteria.id,
                courseId: _this.criteria.courseId,
                competencyNumCorrect: +val.competencyCorrect,
                competencyTrialLimit: +val.competencyTrialLimitSelect,
                competencyTrialNumInput: +val.competencyTrialNum,
                maintenanceEnable: val.maintenanceEnable,
                maintenancePicker: val.repeatDate,
                maintenanceNumCorrect: +val.maintenanceCorrect,
                maintenanceTrialLimit: +val.maintenanceTrialLimitSelect,
                maintenanceTrialNumInput: +val.maintenanceTrialNum
            };
            _this.criteriaInnerService.setSavedCriteria(objCriteria);
        });
    };
    CriteriaComponent.prototype.createFormGroup = function () {
        this.formGroup = this.fb.group({
            competencyCorrect: [this.criteria.competencyNumCorrect || 0, Validators.required],
            competencyTrialLimitSelect: [this.criteria.competencyTrialLimit || '0', Validators.required],
            competencyTrialNum: [this.criteria.competencyTrialNumInput],
            maintenanceEnable: [this.criteria.maintenanceEnable],
            repeatDate: [this.criteria.maintenancePicker],
            maintenanceCorrect: [this.criteria.maintenanceNumCorrect || 0, Validators.required],
            maintenanceTrialLimitSelect: [this.criteria.maintenanceTrialLimit || '0', Validators.required],
            maintenanceTrialNum: [this.criteria.maintenanceTrialNumInput]
        });
    };
    Object.defineProperty(CriteriaComponent.prototype, "f", {
        get: function () { return this.formGroup.controls; },
        enumerable: true,
        configurable: true
    });
    // ADD REMARK FUNCTIONS: clearAddForm | checkAddForm | addRemarkButtonOnClick | cancelAddButtonOnClick | saveNewRemark
    CriteriaComponent.prototype.clearAddForm = function () {
        var controls = this.formGroup.controls;
        controls['newText'].setValue('');
        controls['newText'].markAsPristine();
        controls['newText'].markAsUntouched();
        controls['newType'].setValue('0');
        controls['newType'].markAsPristine();
        controls['newType'].markAsUntouched();
        controls['newDueDate'].setValue(this.typesUtilsService.getDateFromString());
        controls['newDueDate'].markAsPristine();
        controls['newDueDate'].markAsUntouched();
    };
    CriteriaComponent.prototype.checkAddForm = function () {
        var controls = this.formGroup.controls;
        if (controls['newText'].invalid || controls['newType'].invalid || controls['newDueDate'].invalid) {
            controls['newText'].markAsTouched();
            controls['newType'].markAsTouched();
            controls['newDueDate'].markAsTouched();
            return false;
        }
        return true;
    };
    __decorate([
        Input()
    ], CriteriaComponent.prototype, "dataCriteria");
    CriteriaComponent = __decorate([
        Component({
            selector: 'm-criteria',
            templateUrl: './criteria-list.component.html'
            // changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], CriteriaComponent);
    return CriteriaComponent;
}());
export { CriteriaComponent };
