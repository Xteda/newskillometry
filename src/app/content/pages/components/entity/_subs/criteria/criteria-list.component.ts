import {
	Component,
	OnInit,
	Input,
	SimpleChanges, OnChanges
} from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
// Material
import { MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { fromEvent, merge, BehaviorSubject } from 'rxjs';
// Services
import { TypesUtilsService } from '../../_core/utils/types-utils.service';
import { LayoutUtilsService, MessageType } from '../../_core/utils/layout-utils.service';
import { CurriculumCriteriaService } from '../../_core/services/curriculum-criteria.service';
import { CriteriaService} from '../../_core/utils/criteria.service';
// Models
import { CriteriaModel } from '../../_core/models/criteria.model';

import * as _ from 'lodash';


export enum TrialLimit {
	'Unlimited',
	'Lesson trial limit',
	'Lesson trial minimum requirement'
}
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'm-criteria',
	templateUrl: './criteria-list.component.html'
	// changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaComponent implements OnInit, OnChanges {
	// Incoming data
	public criteria: CriteriaModel;
	public oldCriteria: CriteriaModel;
	// Selection
	selection = new SelectionModel<CriteriaModel>(true, []);
	// Add and Edit
	formGroup: FormGroup;
	trialLimitEnum = TrialLimit;

	@Input() dataCriteria;

	constructor(private activatedRoute: ActivatedRoute,
				public criteriaInnerService: CriteriaService,
				private router: Router,
				private criteriaService: CurriculumCriteriaService,
				private fb: FormBuilder,
				public dialog: MatDialog,
				public typesUtilsService: TypesUtilsService) { }

	/** LOAD DATA */
	ngOnInit() {
		if (typeof this.dataCriteria !== 'undefined') {
			this.criteria = Object.assign({}, this.dataCriteria);
		} else {
			const newCriteria = new CriteriaModel();
			this.criteria = newCriteria;
			this.oldCriteria = Object.assign({}, newCriteria);
		}
		this.createFormGroup();
		this.onFieldChanges();
	}

	ngOnChanges(changes: SimpleChanges) {
		for (const propName in changes) {
			if (propName === 'dataCriteria' && this.criteria) {
				this.criteria = Object.assign({}, this.dataCriteria);
				this.createFormGroup();
				this.onFieldChanges();
			}
		}
	}

	onFieldChanges(): void {
		this.formGroup.valueChanges.subscribe(val => {
			const objCriteria = {
				id: this.criteria.id,
				courseId: this.criteria.courseId,
				competencyNumCorrect: +val.competencyCorrect,
				competencyTrialLimit: +val.competencyTrialLimitSelect,
				competencyTrialNumInput: +val.competencyTrialNum,
				maintenanceEnable: val.maintenanceEnable,
				maintenancePicker: val.repeatDate,
				maintenanceNumCorrect: +val.maintenanceCorrect,
				maintenanceTrialLimit: +val.maintenanceTrialLimitSelect,
				maintenanceTrialNumInput: +val.maintenanceTrialNum,
			};
			this.criteriaInnerService.setSavedCriteria(objCriteria);
		});
	}

	createFormGroup() {
		this.formGroup = this.fb.group({
			competencyCorrect: [this.criteria.competencyNumCorrect || 0, Validators.required],
			competencyTrialLimitSelect: [this.criteria.competencyTrialLimit || '0', Validators.required],
			competencyTrialNum: [this.criteria.competencyTrialNumInput],
			maintenanceEnable: [this.criteria.maintenanceEnable],
			repeatDate: [this.criteria.maintenancePicker],
			maintenanceCorrect: [this.criteria.maintenanceNumCorrect || 0, Validators.required],
			maintenanceTrialLimitSelect: [this.criteria.maintenanceTrialLimit || '0', Validators.required],
			maintenanceTrialNum: [this.criteria.maintenanceTrialNumInput],
		});
	}
	get f() { return this.formGroup.controls; }

	// ADD REMARK FUNCTIONS: clearAddForm | checkAddForm | addRemarkButtonOnClick | cancelAddButtonOnClick | saveNewRemark
	clearAddForm() {
		const controls = this.formGroup.controls;
		controls['newText'].setValue('');
		controls['newText'].markAsPristine();
		controls['newText'].markAsUntouched();
		controls['newType'].setValue('0');
		controls['newType'].markAsPristine();
		controls['newType'].markAsUntouched();
		controls['newDueDate'].setValue(this.typesUtilsService.getDateFromString());
		controls['newDueDate'].markAsPristine();
		controls['newDueDate'].markAsUntouched();
	}

	checkAddForm() {
		const controls = this.formGroup.controls;
		if (controls['newText'].invalid || controls['newType'].invalid || controls['newDueDate'].invalid) {
			controls['newText'].markAsTouched();
			controls['newType'].markAsTouched();
			controls['newDueDate'].markAsTouched();
			return false;
		}

		return true;
	}
}
