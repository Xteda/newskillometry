import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { fromEvent, merge } from 'rxjs';
// Services
import { InstructionService} from '../../_core/services/instruction.service';
import { LayoutUtilsService, MessageType } from '../../_core/utils/layout-utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
// Models
import { InstructionDatasource} from '../../_core/models/data-sources/instruction.datasource';
import { QueryParamsModel } from '../../_core/models/query-models/query-params.model';
import { InstructionModel} from '../../_core/models/instruction.model';
import * as moment from 'moment';
// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4

@Component({
	selector: 'm-instruction-list',
	templateUrl: './instructions-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstructionsListComponent implements OnInit {
	// Table fields
	dataSource: InstructionDatasource;
	displayedColumns = ['select', 'title', 'author', 'children', 'create', 'update', 'status', 'actions'];
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<InstructionModel>(true, []);
	coursesResult: InstructionModel[] = [];

	constructor(private entityService: InstructionService,
				private router: Router,
				public dialog: MatDialog,
				private route: ActivatedRoute,
				private subheaderService: SubheaderService,
				private layoutUtilsService: LayoutUtilsService) { }

	/** LOAD DATA */
	ngOnInit() {
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadProductsList();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadProductsList();
				})
			)
			.subscribe();

		// Init DataSource
		this.dataSource = new InstructionDatasource(this.entityService);
		const queryParams = new QueryParamsModel({});
		queryParams.sortOrder = 'desc';
		queryParams.sortField = 'create';
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			// Set title to page breadCrumbs
			this.subheaderService.setTitle('Instructions');
			if (params.parentId) {
				this.dataSource.loadProducts(queryParams, params.parentId);
				this.subheaderService.setBreadcrumbs([
					{ title: 'Curriculums',  page: '/educational-center/curriculums' },
					{ title: 'Courses',  page: '/educational-center/courses' },
					{ title: 'Lessons', page: `/educational-center/lessons`, queryParams: { parentId: params.parentId } }
				]);
			} else {
				// First load
				this.dataSource.loadProducts(queryParams);
				this.subheaderService.setBreadcrumbs([
					{ title: 'Curriculums',  page: '/educational-center/curriculums' },
					{ title: 'Courses',  page: '/educational-center/courses' },
					{ title: 'Lessons', page: `/educational-center/lessons` }
				]);
			}
		});
		this.dataSource.entitySubject.subscribe(res => this.coursesResult = res);
	}

	loadProductsList() {
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction = 'desc',
			this.sort.active = '_createdDate',
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadProducts(queryParams);
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;

		if (this.filterStatus && this.filterStatus.length > 0) {
			filter.status = +this.filterStatus;
		}

		if (this.filterCondition && this.filterCondition.length > 0) {
			filter.condition = +this.filterCondition;
		}

		filter.author = searchText;

		filter.title = searchText;

		return filter;
	}

	restoreState(queryParams: QueryParamsModel, parentId: number) {
		console.log('restoreState', parentId);
		if (parentId > 0) {
			this.entityService.getInstructionById(parentId).subscribe((res: InstructionModel[]) => {
				console.log('res', res);
			});
		}

		if (!queryParams.filter) {
			return;
		}

		if ('condition' in queryParams.filter) {
			this.filterCondition = queryParams.filter.condition.toString();
		}

		if ('status' in queryParams.filter) {
			this.filterStatus = queryParams.filter.status.toString();
		}

		if (queryParams.filter.model) {
			this.searchInput.nativeElement.value = queryParams.filter.model;
		}
	}

	/** ACTIONS */
	/** Delete */
	deleteProduct(event, _item: InstructionModel) {
		event.stopPropagation();
		const _title: string = 'Instructions Delete';
		const _description: string = 'Are you sure to permanently delete this instruction?';
		const _waitDesciption: string = 'Instructions is deleting...';
		const _deleteMessage = `Instructions has been deleted`;

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.entityService.deleteInstruction(_item.id).subscribe(() => {
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
				this.loadProductsList();
			});
		});
	}

	deleteProducts() {
		const _title: string = 'Instructions Delete';
		const _description: string = 'Are you sure to permanently delete selected instructions?';
		const _waitDesciption: string = 'Instructions are deleting...';
		const _deleteMessage = 'Selected instructions have been deleted';

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			const idsForDeletion: number[] = [];
			for (let i = 0; i < this.selection.selected.length; i++) {
				idsForDeletion.push(this.selection.selected[i].id);
			}
			this.entityService.deleteInstructions(idsForDeletion).subscribe(() => {
				this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
				this.loadProductsList();
				this.selection.clear();
			});
		});
	}

	/** Update Product */
	updateStatusForProducts() {
		const _title = 'Update status for selected instructions';
		const _updateMessage = 'Status has been updated for selected instructions';
		const _statuses = [{ value: 1, text: 'Active' }, { value: 0, text: 'Closed' }];
		const _messages = [];

		this.selection.selected.forEach(elem => {
			_messages.push({
				text: `${elem.author}, ${elem.title}`,
				id: 'test id',
				status: elem.status,
				statusTitle: this.getItemStatusString(elem.status),
				statusCssClass: this.getItemCssClassByStatus(elem.status)
			});
		});

		const dialogRef = this.layoutUtilsService.updateStatusForCustomers(_title, _statuses, _messages);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				this.selection.clear();
				return;
			}

			this.entityService.updateStatusForInstruction(this.selection.selected, +res).subscribe(() => {
				this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update);
				this.loadProductsList();
				this.selection.clear();
			});
		});
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.coursesResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.coursesResult.forEach(row => this.selection.select(row));
		}
	}

	/* UI */
	getItemStatusString(status: number = 0): string {
		return status ? 'Active' : 'Closed';
	}

	getItemCssClassByStatus(status: number = 0): string {
		return status ? 'success' : 'metal';
	}

	getNewCssClassByStatus(x) {
		return moment(new Date()).diff(x, 'days') > 1;
	}

	getChildrenEntities(id) {
		this.router.navigateByUrl('educational-center/lessons?parentId=' + id);
	}

	public timeAgo(x) {
		return moment(x).fromNow();
	}
}
