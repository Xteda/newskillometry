var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { forkJoin, BehaviorSubject } from 'rxjs';
import { MessageType } from '../../_core/utils/layout-utils.service';
import { CriteriaModel } from '../../_core/models/criteria.model';
import { VideoModel } from '../../_core/models/video.model';
import { MarkerModel } from '../../_core/models/marker.model';
import { PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { RecordVideoComponent } from '../../../../../partials/content/general/record-video/record-video.component';
import * as moment from 'moment';
import * as _ from 'lodash';
import { InstructionModel } from '../../_core/models/instruction.model';
import { FileModel } from '../../_core/models/file.model';
var InstructionEditComponent = /** @class */ (function () {
    function InstructionEditComponent(activatedRoute, router, storage, lessonsService, criteriaInnerService, criteriaOuterService, fileInnerService, fileOuterService, instructionService, typesUtilsService, videoService, markersService, markerService, instructionFB, dialog, subheaderService, layoutUtilsService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.storage = storage;
        this.lessonsService = lessonsService;
        this.criteriaInnerService = criteriaInnerService;
        this.criteriaOuterService = criteriaOuterService;
        this.fileInnerService = fileInnerService;
        this.fileOuterService = fileOuterService;
        this.instructionService = instructionService;
        this.typesUtilsService = typesUtilsService;
        this.videoService = videoService;
        this.markersService = markersService;
        this.markerService = markerService;
        this.instructionFB = instructionFB;
        this.dialog = dialog;
        this.subheaderService = subheaderService;
        this.layoutUtilsService = layoutUtilsService;
        this.parentEntityArray = [];
        this.fakeStatus = false;
        this.markers = [];
        this.oldMarkers = [];
        this.selectedTab = 0;
        this.loadingSubject = new BehaviorSubject(false);
        this.loading$ = this.loadingSubject.asObservable();
        this.hasFormErrors = false;
        this.htmlContent = '';
        this.config = {
            editable: true,
            spellcheck: true,
            height: '15rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'yes',
            customClasses: [
                {
                    name: 'quote',
                    "class": 'quote'
                },
                {
                    name: 'redText',
                    "class": 'redText'
                },
                {
                    name: 'titleText',
                    "class": 'titleText',
                    tag: 'h1'
                },
            ]
        };
    }
    InstructionEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadingSubject.next(true);
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.entityId = +params.id;
            _this.lessonsService.getAllLessons().subscribe(function (res) {
                for (var i = 0; i < res.length; i++) {
                    var tempObj = {
                        value: res[i].id,
                        title: res[i].title
                    };
                    _this.parentEntityArray.push(tempObj);
                }
            });
            if (_this.entityId && _this.entityId > 0) {
                _this.instructionService.getInstructionById(_this.entityId).subscribe(function (res) {
                    window.dispatchEvent(new Event('resize'));
                    _this.criteria = res.criteria[0];
                    _this.video = Object.assign({}, res.videos[0]);
                    _this.markers = res.markers;
                    _this.files = res.files;
                    delete res['criteria'];
                    delete res['videos'];
                    delete res['markers'];
                    delete res['files'];
                    _this.instruction = res;
                    _this.fakeStatus = res.status;
                    _this.instruction._prevState = Object.assign({}, res);
                    _this.initInstruction();
                    if (_this.criteria) {
                        _this.criteriaInnerService.setSavedCriteria(_this.criteria);
                        _this.criteria._prevState = Object.assign({}, _this.criteria);
                    }
                    else {
                        var newCriteria = new CriteriaModel();
                        _this.criteria = newCriteria;
                        _this.criteriaInnerService.setSavedCriteria(_this.criteria);
                        _this.criteria._prevState = Object.assign({}, newCriteria);
                    }
                    if (_this.video) {
                        _this.video._prevState = Object.assign({}, _this.video);
                        if (_this.markers) {
                            _this.markers = _.orderBy(_this.markers, ['time'], ['asc']);
                            _this.oldMarkers = _this.markers.map(function (x) { return Object.assign({}, x); });
                            _this.markersGroup = _this.instructionFB.array(_this.getMarkersFields().map(function (marker) { return _this.instructionFB.group(marker); }));
                            _this.instructionForm.addControl('markers', _this.markersGroup);
                        }
                        setTimeout(function () {
                            _this.initVideo();
                        }, 0);
                    }
                    else {
                        var newVideo = new VideoModel();
                        _this.video = newVideo;
                        _this.video._prevState = Object.assign({}, _this.video);
                    }
                    if (_this.files) {
                        _this.fileInnerService.setSavedFiles(_this.files);
                        _this.oldFiles = Object.assign({}, _this.files);
                    }
                    else {
                        var newFiles = new Array();
                        _this.files = newFiles;
                        _this.fileInnerService.setSavedFiles(_this.files);
                        _this.oldFiles = Object.assign({}, newFiles);
                    }
                });
            }
            else {
                var newProduct = new InstructionModel();
                var newCriteria = new CriteriaModel();
                var newVideo = new VideoModel();
                var newMarkers = [];
                var newFiles = new Array();
                // newProduct.clear();
                newCriteria.clear(_this.entityId);
                newVideo.clear(_this.entityId);
                _this.instruction = newProduct;
                _this.instruction._prevState = Object.assign({}, newProduct);
                _this.criteria = newCriteria;
                _this.criteriaInnerService.setSavedCriteria(_this.criteria);
                _this.criteria._prevState = Object.assign({}, newCriteria);
                _this.video = newVideo;
                _this.video._prevState = Object.assign({}, newVideo);
                _this.markers = newMarkers;
                _this.oldMarkers = _this.markers.map(function (x) { return Object.assign({}, x); });
                _this.files = newFiles;
                _this.fileInnerService.setSavedFiles(_this.files);
                _this.oldFiles = Object.assign({}, newFiles);
                _this.initInstruction();
            }
        });
    };
    InstructionEditComponent.prototype.ngOnDestroy = function () {
        this.destroyEvent();
    };
    InstructionEditComponent.prototype.initInstruction = function () {
        this.createForm();
        this.loadingSubject.next(false);
        if (!this.instruction.id) {
            this.subheaderService.setBreadcrumbs([
                { title: 'Curriculums', page: '/educational-center/curriculums' },
                { title: 'Courses', page: '/educational-center/courses' },
                { title: 'Lessons', page: '/educational-center/lessons' },
                { title: 'Instructions', page: '/educational-center/instructions' },
                { title: 'Create instruction', page: '/educational-center/instruction/add' }
            ]);
            return;
        }
        this.subheaderService.setTitle('Edit instruction');
        this.subheaderService.setBreadcrumbs([
            { title: 'Curriculums', page: '/educational-center/curriculums' },
            { title: 'Courses', page: '/educational-center/courses' },
            { title: 'Lessons', page: '/educational-center/lessons' },
            { title: 'Instructions', page: '/educational-center/instructions' },
            { title: 'Edit instruction', page: '/educational-center/instruction/edit', queryParams: { id: this.instruction.id } }
        ]);
    };
    InstructionEditComponent.prototype.createForm = function () {
        var _this = this;
        var tempObj = _.find(this.parentEntityArray, function (o) { return o.value == _this.instruction.lessonId; }) || '';
        var selectValue = tempObj ? tempObj.value.toString() : '';
        this.instructionForm = this.instructionFB.group({
            title: [this.instruction.title, Validators.required],
            description: [this.instruction.description],
            selectedParent: [selectValue, Validators.required]
        });
    };
    InstructionEditComponent.prototype.getMarkersFields = function () {
        var markersControlArray = [];
        for (var i = 0; i < this.markers.length; i++) {
            markersControlArray.push({
                markerTitle: [this.markers[i].text, Validators.required],
                markerDescription: [this.markers[i].overlayText, Validators.required]
            });
        }
        return markersControlArray;
    };
    InstructionEditComponent.prototype.goBack = function (id) {
        if (id === void 0) { id = 0; }
        var _backUrl = 'educational-center/instructions';
        if (id > 0) {
            _backUrl += '?id=' + id;
        }
        this.router.navigateByUrl(_backUrl);
    };
    InstructionEditComponent.prototype.refreshProduct = function (id) {
        if (id === void 0) { id = 0; }
        var _refreshUrl = 'educational-center/instruction/edit?id=' + id;
        this.router.navigateByUrl(_refreshUrl);
    };
    InstructionEditComponent.prototype.reset = function () {
        var _this = this;
        var tempCurriculum = this.instruction._prevState;
        var tempCriteria = this.criteria._prevState;
        var tempVideo = this.video._prevState;
        var tempFiles = this.oldFiles;
        var hasVideo = this.instruction.hasVideo;
        this.instruction = Object.assign({}, this.instruction._prevState);
        this.instruction._prevState = Object.assign({}, tempCurriculum);
        this.fakeStatus = !!this.instruction.status;
        this.createForm();
        if (this.oldMarkers) {
            this.markers = this.oldMarkers.map(function (x) { return Object.assign({}, x); });
            this.markersGroup = this.instructionFB.array(this.getMarkersFields().map(function (marker) { return _this.instructionFB.group(marker); }));
            this.instructionForm.addControl('markers', this.markersGroup);
            if (hasVideo && this.tabGroup.selectedIndex === 0) {
                console.log(this.tabGroup.selectedIndex);
                this.videoJSplayer.markers.reset(this.markers);
            }
        }
        if (!hasVideo && tempCurriculum.hasVideo) {
            this.video = Object.assign({}, tempVideo);
            this.video._prevState = Object.assign({}, tempVideo);
            setTimeout(function () {
                if (_this.tabGroup.selectedIndex === 0) {
                    _this.initVideo();
                    if (_this.markers) {
                        console.log(_this.tabGroup.selectedIndex);
                        _this.videoJSplayer.markers.reset(_this.markers);
                    }
                }
            });
        }
        this.files = tempFiles.map(function (x) { return Object.assign({}, x); });
        this.criteria = Object.assign({}, tempCriteria);
        this.criteria._prevState = Object.assign({}, tempCriteria);
        this.criteriaInnerService.setSavedCriteria(this.criteria);
        this.hasFormErrors = false;
        this.instructionForm.markAsPristine();
        this.instructionForm.markAsUntouched();
        this.instructionForm.updateValueAndValidity();
    };
    InstructionEditComponent.prototype.onSumbit = function (withBack) {
        if (withBack === void 0) { withBack = false; }
        this.hasFormErrors = false;
        var controls = this.instructionForm.controls;
        /** check form */
        if (this.instructionForm.invalid) {
            Object.keys(controls).forEach(function (controlName) {
                return controls[controlName].markAsTouched();
            });
            this.hasFormErrors = true;
            this.selectedTab = 0;
            return;
        }
        else if (this.parentEntity === 'undefined') {
        }
        // tslint:disable-next-line:prefer-const
        var editedProduct = this.prepareProduct();
        if (editedProduct.id > 0) {
            this.updateProduct(editedProduct, withBack);
            return;
        }
        this.addInstruction(editedProduct, withBack);
    };
    InstructionEditComponent.prototype.prepareProduct = function () {
        var controls = this.instructionForm.controls;
        var _product = new InstructionModel();
        _product.id = this.instruction.id;
        _product.title = controls['title'].value;
        _product.author = this.instruction.author || 'John Smith';
        _product.description = controls['description'].value;
        _product.status = this.fakeStatus ? 1 : 0;
        _product.lessonId = controls['selectedParent'].value;
        _product._userId = 1; // TODO: get version from userId
        _product.children = this.instruction.children || 0;
        _product._createdDate = this.instruction._createdDate;
        _product._updatedDate = this.instruction._updatedDate;
        _product.hasVideo = !!this.video.videoUrl;
        _product._updatedDate = Date.now();
        _product._createdDate = this.instruction.id > 0 ? _product._createdDate : _product._updatedDate;
        _product._isNew = this.instruction.id <= 0;
        _product._isUpdated = this.instruction.id > 0;
        return _product;
    };
    InstructionEditComponent.prototype.prepareCriteria = function (entityId) {
        var _criteria = new CriteriaModel();
        var savedCriteria = this.criteriaInnerService.getSavedCriteria();
        _criteria.id = savedCriteria.id;
        _criteria.instructionId = this.entityId || entityId;
        _criteria.competencyNumCorrect = +savedCriteria.competencyNumCorrect;
        _criteria.competencyTrialLimit = +savedCriteria.competencyTrialLimit;
        _criteria.competencyTrialNumInput = +savedCriteria.competencyTrialNumInput;
        _criteria.maintenanceEnable = savedCriteria.maintenanceEnable;
        _criteria.maintenancePicker = savedCriteria.maintenancePicker;
        _criteria.maintenanceNumCorrect = +savedCriteria.maintenanceNumCorrect;
        _criteria.maintenanceTrialLimit = +savedCriteria.maintenanceTrialLimit;
        _criteria.maintenanceTrialNumInput = +savedCriteria.maintenanceTrialNumInput;
        _criteria._updatedDate = Date.now();
        _criteria._createdDate = this.criteria.id > 0 ? _criteria._createdDate : _criteria._updatedDate;
        _criteria._isNew = this.criteria.id <= 0;
        _criteria._isUpdated = this.criteria.id > 0;
        return _criteria;
    };
    InstructionEditComponent.prototype.prepareVideo = function (entityId) {
        var _video = new VideoModel();
        _video.id = this.video.id;
        _video.instructionId = this.entityId || entityId;
        _video.poster = this.video.poster;
        _video.videoUrl = this.video.videoUrl;
        _video._userId = 1; // TODO: get version from userId
        _video._updatedDate = Date.now();
        _video._createdDate = this.video.id > 0 ? this.video._createdDate : _video._updatedDate;
        _video._isUpdated = this.video._isUpdated;
        _video._isDeleted = this.video._isDeleted;
        _video._isNew = this.video.id <= 0;
        return _video;
    };
    InstructionEditComponent.prototype.prepareMarkers = function (entityId) {
        var _markers = [];
        for (var i = 0; i < this.markers.length; i++) {
            var _marker = new MarkerModel();
            _marker.id = this.markers[i].id;
            _marker.instructionId = this.markers[i].instructionId || entityId;
            _marker.time = this.markers[i].time;
            _marker.overlayText = this.markers[i].overlayText;
            _marker.text = this.markers[i].text;
            _marker.displayTime = this.markers[i].displayTime;
            _marker._isEditMode = this.markers[i]._isEditMode;
            _marker._isUpdated = this.markers[i]._isUpdated;
            _marker._isDeleted = this.markers[i]._isDeleted;
            _marker._updatedDate = Date.now();
            _marker._createdDate = this.markers[i]._createdDate ? this.markers[i]._createdDate : this.markers[i]._updatedDate;
            _marker._isNew = this.markers[i]._isNew;
            _markers.push(_marker);
        }
        return _markers;
    };
    InstructionEditComponent.prototype.prepareFiles = function (entityId) {
        var _files = [];
        var savedFiles = this.fileInnerService.getSavedFiles();
        for (var i = 0; i < savedFiles.length; i++) {
            var _file = new FileModel();
            _file.id = savedFiles[i].id;
            _file.instructionId = savedFiles[i].instructionId || entityId;
            _file.type = savedFiles[i].type;
            _file.size = savedFiles[i].size;
            _file.title = savedFiles[i].title;
            _file.link = savedFiles[i].link;
            _file._updatedDate = Date.now();
            _file._isUpdated = savedFiles[i]._isUpdated;
            _file._isDeleted = savedFiles[i]._isDeleted;
            _file._createdDate = savedFiles[i]._createdDate ? savedFiles[i]._createdDate : savedFiles[i]._updatedDate;
            _file._isNew = savedFiles[i]._isNew;
            _files.push(_file);
        }
        return _files;
    };
    InstructionEditComponent.prototype.addInstruction = function (_product, withBack) {
        var _this = this;
        if (withBack === void 0) { withBack = false; }
        this.loadingSubject.next(true);
        this.instructionService.createInstruction(_product).subscribe(function (res) {
            var criteria = _this.prepareCriteria(res.id);
            var video = _this.prepareVideo(res.id);
            forkJoin([_this.criteriaOuterService.createCriteria(criteria),
                _this.videoService.createVideo(video)]).subscribe(function (_a) {
                var criteria = _a[0], video = _a[1];
                _this.loadingSubject.next(false);
                if (withBack) {
                    _this.goBack(res.id);
                }
                else {
                    var message = "New instruction successfully has been added.";
                    _this.layoutUtilsService.showActionNotification(message, MessageType.Create, 10000, true, false);
                    _this.refreshProduct(res.id);
                }
            });
        });
    };
    InstructionEditComponent.prototype.updateProduct = function (_product, withBack) {
        var _this = this;
        if (withBack === void 0) { withBack = false; }
        this.loadingSubject.next(true);
        // Update Product
        // tslint:disable-next-line:prefer-const
        var tasks$ = [this.instructionService.updateInstruction(_product)];
        // Update Criteria
        var criteria = this.prepareCriteria(this.entityId);
        tasks$.push(this.criteriaOuterService.updateCriteria(criteria));
        // Update video
        if (this.instruction.hasVideo || this.instruction._prevState.hasVideo) {
            var video = this.prepareVideo(this.entityId);
            if (video._isDeleted) {
                this.storage.storage.refFromURL(this.video._prevState.videoUrl)["delete"]();
                tasks$.push(this.videoService.deleteVideo(video));
            }
            else if (video.id) {
                tasks$.push(this.videoService.updateVideo(video));
            }
            else {
                tasks$.push(this.videoService.createVideo(video));
            }
            // Update markers
            var markers = this.prepareMarkers(this.entityId);
            if (markers.length > 0) {
                var deletedMarkers = [];
                var updatedMarkers = [];
                for (var i = 0; i < markers.length; i++) {
                    if (markers[i]._isDeleted) {
                        deletedMarkers.push(markers[i]);
                        continue;
                    }
                    if (markers[i]._isNew || markers[i]._isUpdated) {
                        updatedMarkers.push(markers[i]);
                    }
                }
                if (deletedMarkers.length > 0) {
                    for (var i = 0; i < deletedMarkers.length; i++) {
                        var _marker = deletedMarkers[i];
                        tasks$.push(this.markersService.deleteMarker(_marker));
                    }
                }
                if (updatedMarkers.length > 0) {
                    for (var i = 0; i < updatedMarkers.length; i++) {
                        var _marker = updatedMarkers[i];
                        if (_marker.id) {
                            tasks$.push(this.markersService.updateMarker(_marker));
                        }
                        else {
                            tasks$.push(this.markersService.createMarker(_marker));
                        }
                    }
                }
            }
        }
        var files = this.prepareFiles(this.entityId);
        if (files.length > 0) {
            var deletedFiles = [];
            var updatedFiles = [];
            for (var i = 0; i < files.length; i++) {
                if (files[i]._isDeleted && typeof files[i].id != 'undefined') {
                    deletedFiles.push(files[i]);
                    continue;
                }
                if (files[i]._isNew) {
                    updatedFiles.push(files[i]);
                }
            }
            if (deletedFiles.length > 0) {
                for (var i = 0; i < deletedFiles.length; i++) {
                    if (deletedFiles[i].id !== 'undefined') {
                        var _file = deletedFiles[i];
                        tasks$.push(this.fileOuterService.deleteFile(_file));
                    }
                }
            }
            if (updatedFiles.length > 0) {
                for (var i = 0; i < updatedFiles.length; i++) {
                    var _file = updatedFiles[i];
                    if (_file.id) {
                        tasks$.push(this.fileOuterService.updateFile(_file));
                    }
                    else {
                        tasks$.push(this.fileOuterService.createFile(_file));
                    }
                }
            }
            console.log(deletedFiles, updatedFiles);
        }
        forkJoin(tasks$).subscribe(function (res) {
            _this.loadingSubject.next(false);
            if (withBack) {
                _this.goBack(_product.id);
            }
            else {
                var message = "Instruction successfully has been saved.";
                _this.layoutUtilsService.showActionNotification(message, MessageType.Update, 10000, true, false);
                _this.refreshProduct(_product.id);
            }
        });
    };
    InstructionEditComponent.prototype.getComponentTitle = function () {
        var result = 'Create instruction';
        if (!this.instruction || !this.instruction.id) {
            return result;
        }
        result = "Edit instruction - \"" + this.instruction.title + "\" by " + this.instruction.author;
        return result;
    };
    InstructionEditComponent.prototype.onAlertClose = function ($event) {
        this.hasFormErrors = false;
    };
    InstructionEditComponent.prototype.tabChange = function () {
        this.previousTab = this.selectedIndex;
        this.selectedIndex = this.tabGroup.selectedIndex;
        this.criteria = this.criteriaInnerService.getSavedCriteria();
        if (this.tabGroup.selectedIndex === 0) {
            this.initVideo();
        }
    };
    InstructionEditComponent.prototype.destroyEvent = function () {
        if (this.video.videoUrl) {
            var videoArray = videojs.getPlayers();
            for (var videoId in videoArray) {
                if (videoArray.hasOwnProperty(videoId)) {
                    videojs(videoId).dispose();
                    delete videoArray[videoId];
                }
            }
        }
    };
    InstructionEditComponent.prototype.deleteVideo = function (addNew) {
        this.videoJSplayer.markers.removeAll();
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i]._isDeleted = true;
        }
        this.instructionForm.removeControl('markers');
        this.destroyEvent();
        this.video.videoUrl = '';
        this.video.poster = '';
        this.video._isDeleted = true;
        this.instruction.hasVideo = false;
        if (addNew) {
            this.addVideo();
        }
    };
    InstructionEditComponent.prototype.initVideo = function () {
        if (this.video.videoUrl) {
            var scrollService_1 = this.componentRef;
            this.videoJSplayer = videojs('video', {
                fluid: true
            });
            var markerElArray_1 = document.getElementsByClassName('marker');
            this.videoJSplayer.markers({
                breakOverlay: {
                    display: true
                },
                onMarkerReached: function (marker, index) {
                    for (var i = 0; i < markerElArray_1.length; i++) {
                        markerElArray_1[i].classList.remove('bg-success', 'text-white');
                    }
                    markerElArray_1[index].classList.add('bg-success', 'text-white');
                    scrollService_1.directiveRef.scrollTo(0, document.getElementById('marker-' + index).offsetTop - 3, 500);
                },
                onMarkerLeft: function (marker, index) {
                    markerElArray_1[index].classList.remove('bg-success', 'text-white');
                },
                markers: _.filter(this.markers, function (o) { return !o._isDeleted; })
            });
        }
    };
    InstructionEditComponent.prototype.timeMarkerAction = function (mIndex, mTime, mStart) {
        this.markerService.setCurrentTime(this.videoJSplayer, this.markers, mIndex, mTime, mStart);
    };
    InstructionEditComponent.prototype.mEdit = function (mIndex) {
        this.markerService.markerEdit(this.videoJSplayer, this.markers, mIndex, this.instructionForm);
    };
    InstructionEditComponent.prototype.mDelete = function (mIndex) {
        this.markerService.markerDelete(this.videoJSplayer, this.markers, mIndex);
    };
    InstructionEditComponent.prototype.checkMarkers = function (markersArray) {
        for (var i = 0; i < markersArray.length; i++) {
            if (markersArray[i]._isEditMode) {
                return true;
            }
        }
        return false;
    };
    InstructionEditComponent.prototype.checkDeletedMarkers = function () {
        for (var i = 0; i < this.markers.length; i++) {
            if (!this.markers[i]._isDeleted) {
                return true;
            }
        }
        return false;
    };
    InstructionEditComponent.prototype.convertTime = function (sec) {
        return moment.utc(sec * 1000).format('HH:mm:ss');
    };
    InstructionEditComponent.prototype.mAdd = function () {
        var _this = this;
        var success = this.markerService.addMarker(this.videoJSplayer, this.markers);
        if (success) {
            this.markers = _.orderBy(this.markers, ['time'], ['asc']);
            this.instructionForm.removeControl('markers');
            this.markersGroup = this.instructionFB.array(this.getMarkersFields().map(function (marker) { return _this.instructionFB.group(marker); }));
            this.instructionForm.addControl('markers', this.markersGroup);
            window.dispatchEvent(new Event('resize'));
            setTimeout(function () {
                var mIndex;
                mIndex = _.findIndex(_this.markers, { '_isEditMode': true });
                _this.componentRef.directiveRef.scrollTo(0, document.getElementById('marker-' + mIndex).offsetTop - 3, 500);
            }, 50);
        }
    };
    InstructionEditComponent.prototype.mRemoveAll = function () {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i]._isDeleted = true;
        }
        this.instructionForm.removeControl('markers');
        this.videoJSplayer.markers.removeAll();
        this.layoutUtilsService.showActionNotification('All Annotation have been deleted', MessageType.Create, 10000, true, false);
    };
    InstructionEditComponent.prototype.addVideo = function () {
        var _this = this;
        var dialogRef = this.dialog.open(RecordVideoComponent, {
            width: '800px',
            id: 'videoModal',
            disableClose: true
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.video.videoUrl = result.videoUrl;
                _this.instruction.hasVideo = true;
                window.dispatchEvent(new Event('resize'));
                setTimeout(function () {
                    _this.initVideo();
                }, 50);
            }
        });
    };
    InstructionEditComponent.prototype.changeStatus = function (status) {
        this.fakeStatus = !this.fakeStatus;
    };
    __decorate([
        ViewChild('entityTabs')
    ], InstructionEditComponent.prototype, "tabGroup");
    __decorate([
        ViewChild(PerfectScrollbarComponent)
    ], InstructionEditComponent.prototype, "componentRef");
    __decorate([
        ViewChild(PerfectScrollbarDirective)
    ], InstructionEditComponent.prototype, "directiveRef");
    InstructionEditComponent = __decorate([
        Component({
            selector: 'm-instruction-edit',
            templateUrl: './instruction-edit.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], InstructionEditComponent);
    return InstructionEditComponent;
}());
export { InstructionEditComponent };
