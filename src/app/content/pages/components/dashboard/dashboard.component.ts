import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutConfigService } from '../../../../core/services/layout-config.service';
import { SubheaderService } from '../../../../core/services/layout/subheader.service';
import {Chart} from 'angular-highcharts';
import * as objectPath from 'object-path';

@Component({
	selector: 'm-dashboard',
	templateUrl: './dashboard.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
	chart: Chart;
	public config: any;

	constructor(
		private router: Router,
		private layoutConfigService: LayoutConfigService,
		private subheaderService: SubheaderService
	) {
	}

	ngOnInit(): void {
		this.init();
	}

	private init() {
		const chart = new Chart({
			chart: {
				'type': 'spline'
			},
			title: {
				text: 'Linechart'
			},
			credits: {
				enabled: false
			},
			series: [{
				name: 'Line 1',
				data: [1, 2, 3]
			}]
		});
		chart.addPoint(4);
		this.chart = chart;
		chart.addPoint(5);
		setTimeout(() => {
			chart.addPoint(6);
		}, 2000);

		// chart.ref$.subscribe(console.log);
	}
	private addPoint() {
		if (this.chart) {
			this.chart.addPoint(Math.floor(Math.random() * 10));
		} else {
			alert('init chart, first!');
		}
	}
}
