var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Chart } from 'angular-highcharts';
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(router, layoutConfigService, subheaderService) {
        this.router = router;
        this.layoutConfigService = layoutConfigService;
        this.subheaderService = subheaderService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.init();
    };
    DashboardComponent.prototype.init = function () {
        var chart = new Chart({
            chart: {
                'type': 'spline'
            },
            title: {
                text: 'Linechart'
            },
            credits: {
                enabled: false
            },
            series: [{
                    name: 'Line 1',
                    data: [1, 2, 3]
                }]
        });
        chart.addPoint(4);
        this.chart = chart;
        chart.addPoint(5);
        setTimeout(function () {
            chart.addPoint(6);
        }, 2000);
        // chart.ref$.subscribe(console.log);
    };
    DashboardComponent.prototype.addPoint = function () {
        if (this.chart) {
            this.chart.addPoint(Math.floor(Math.random() * 10));
        }
        else {
            alert('init chart, first!');
        }
    };
    DashboardComponent = __decorate([
        Component({
            selector: 'm-dashboard',
            templateUrl: './dashboard.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], DashboardComponent);
    return DashboardComponent;
}());
export { DashboardComponent };
