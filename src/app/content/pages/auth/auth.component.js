var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, HostBinding } from '@angular/core';
import { LayoutConfig } from '../../../config/layout';
var AuthComponent = /** @class */ (function () {
    function AuthComponent(layoutConfigService, authNoticeService, translationService) {
        this.layoutConfigService = layoutConfigService;
        this.authNoticeService = authNoticeService;
        this.translationService = translationService;
        this.id = 'm_login';
        // tslint:disable-next-line:max-line-length
        this.classses = 'm-grid m-grid--hor m-grid--root m-page';
        this.action = 'login';
        this.today = Date.now();
    }
    AuthComponent.prototype.ngOnInit = function () {
        var _this = this;
        // set login layout to blank
        this.layoutConfigService.setModel(new LayoutConfig({ content: { skin: '' } }), true);
        this.translationService.getSelectedLanguage().subscribe(function (lang) {
            if (lang) {
                setTimeout(function () { return _this.translationService.setLanguage(lang); });
            }
        });
    };
    AuthComponent.prototype.ngOnDestroy = function () {
        // reset back to fluid
        this.layoutConfigService.reloadSavedConfig();
    };
    AuthComponent.prototype.register = function () {
        this.action = 'register';
    };
    __decorate([
        HostBinding('id')
    ], AuthComponent.prototype, "id");
    __decorate([
        HostBinding('class')
        // tslint:disable-next-line:max-line-length
    ], AuthComponent.prototype, "classses");
    __decorate([
        Input()
    ], AuthComponent.prototype, "action");
    AuthComponent = __decorate([
        Component({
            selector: 'm-auth',
            templateUrl: './auth.component.html',
            styleUrls: ['./auth.component.scss']
        })
    ], AuthComponent);
    return AuthComponent;
}());
export { AuthComponent };
