var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Output, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import * as objectPath from 'object-path';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router, http, authNoticeService, translate, cdr, permissionsService) {
        this.authService = authService;
        this.router = router;
        this.http = http;
        this.authNoticeService = authNoticeService;
        this.translate = translate;
        this.cdr = cdr;
        this.permissionsService = permissionsService;
        this.model = { email: 'admin@demo.com', password: 'demo' };
        this.actionChange = new Subject();
        this.loading = false;
        this.errors = [];
        this.spinner = {
            active: false,
            spinnerSize: 18,
            raised: true,
            buttonColor: 'primary',
            spinnerColor: 'accent',
            fullWidth: false
        };
    }
    LoginComponent.prototype.getHTTPHeader = function (res) {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + res.result.accessToken
            })
        };
    };
    LoginComponent.prototype.submit = function () {
        var _this = this;
        this.spinner.active = true;
        if (this.validate(this.f)) {
            this.authService.login(this.model).subscribe(function (response) {
                if (typeof response !== 'undefined') {
                    _this.router.navigate(['/']);
                    localStorage.setItem('accessToken', response.result.accessToken);
                    console.log(localStorage);
                    _this.http.get('http://54.214.10.12//api/services/app/User/GetRoles', _this.getHTTPHeader(response)).subscribe(function (res) {
                    });
                }
                else {
                    _this.authNoticeService.setNotice(_this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'error');
                }
                _this.spinner.active = false;
                _this.cdr.detectChanges();
            });
        }
    };
    LoginComponent.prototype.ngOnInit = function () {
        // demo message to show
        this.permissionsService.loadPermissions(['GUEST']);
        if (!this.authNoticeService.onNoticeChanged$.getValue()) {
            var initialNotice = "Use account\n\t\t\t<strong>admin@demo.com</strong> and password\n\t\t\t<strong>demo</strong> to continue.";
            this.authNoticeService.setNotice(initialNotice, 'success');
        }
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        this.authNoticeService.setNotice(null);
    };
    LoginComponent.prototype.validate = function (f) {
        if (f.form.status === 'VALID') {
            return true;
        }
        this.errors = [];
        if (objectPath.get(f, 'form.controls.email.errors.email')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.email.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.password.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
        }
        if (objectPath.get(f, 'form.controls.password.errors.minlength')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
        }
        if (this.errors.length > 0) {
            this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
            this.spinner.active = false;
        }
        return false;
    };
    LoginComponent.prototype.forgotPasswordPage = function (event) {
        this.action = 'forgot-password';
        this.actionChange.next(this.action);
    };
    LoginComponent.prototype.register = function (event) {
        this.action = 'register';
        this.actionChange.next(this.action);
    };
    __decorate([
        Output()
    ], LoginComponent.prototype, "actionChange");
    __decorate([
        Input()
    ], LoginComponent.prototype, "action");
    __decorate([
        ViewChild('f')
    ], LoginComponent.prototype, "f");
    LoginComponent = __decorate([
        Component({
            selector: 'm-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss'],
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
