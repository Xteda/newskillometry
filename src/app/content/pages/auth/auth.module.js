var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatCheckboxModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { SpinnerButtonModule } from '../../partials/content/general/spinner-button/spinner-button.module';
import { AuthNoticeComponent } from './auth-notice/auth-notice.component';
import { NgxPermissionsModule } from 'ngx-permissions';
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                MatButtonModule,
                MatInputModule,
                MatFormFieldModule,
                MatCheckboxModule,
                TranslateModule.forChild(),
                SpinnerButtonModule,
                RouterModule.forChild([
                    {
                        path: '',
                        component: AuthComponent
                    }
                ]),
                NgxPermissionsModule.forRoot()
            ],
            providers: [],
            declarations: [
                AuthComponent,
                LoginComponent,
                RegisterComponent,
                ForgotPasswordComponent,
                AuthNoticeComponent
            ]
        })
    ], AuthModule);
    return AuthModule;
}());
export { AuthModule };
