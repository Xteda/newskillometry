var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import * as objectPath from 'object-path';
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(authService, authNoticeService, translate) {
        this.authService = authService;
        this.authNoticeService = authNoticeService;
        this.translate = translate;
        this.model = { email: '' };
        this.actionChange = new Subject();
        this.loading = false;
        this.errors = [];
        this.spinner = {
            active: false,
            spinnerSize: 18,
            raised: true,
            buttonColor: 'primary',
            spinnerColor: 'accent',
            fullWidth: false
        };
    }
    RegisterComponent.prototype.ngOnInit = function () { };
    RegisterComponent.prototype.loginPage = function (event) {
        event.preventDefault();
        this.action = 'login';
        this.actionChange.next(this.action);
    };
    RegisterComponent.prototype.submit = function () {
        var _this = this;
        this.spinner.active = true;
        if (this.validate(this.f)) {
            this.authService.register(this.model).subscribe(function (response) {
                _this.action = 'login';
                _this.actionChange.next(_this.action);
                _this.spinner.active = false;
                _this.authNoticeService.setNotice(_this.translate.instant('AUTH.REGISTER.SUCCESS'), 'success');
            });
        }
    };
    RegisterComponent.prototype.validate = function (f) {
        if (f.form.status === 'VALID') {
            return true;
        }
        this.errors = [];
        if (objectPath.get(f, 'form.controls.fullname.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.FULLNAME') }));
        }
        if (objectPath.get(f, 'form.controls.email.errors.email')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.email.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.password.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
        }
        if (objectPath.get(f, 'form.controls.password.errors.minlength')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', { name: this.translate.instant('AUTH.INPUT.PASSWORD'), min: 4 }));
        }
        if (objectPath.get(f, 'form.controls.rpassword.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.CONFIRM_PASSWORD') }));
        }
        if (objectPath.get(f, 'form.controls.agree.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.AGREEMENT_REQUIRED'));
        }
        if (this.errors.length > 0) {
            this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
            this.spinner.active = false;
        }
        return false;
    };
    __decorate([
        Input()
    ], RegisterComponent.prototype, "action");
    __decorate([
        Output()
    ], RegisterComponent.prototype, "actionChange");
    __decorate([
        ViewChild('f')
    ], RegisterComponent.prototype, "f");
    RegisterComponent = __decorate([
        Component({
            selector: 'm-register',
            templateUrl: './register.component.html',
            styleUrls: ['./register.component.scss']
        })
    ], RegisterComponent);
    return RegisterComponent;
}());
export { RegisterComponent };
