import { LayoutModule } from '../layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PartialsModule } from '../partials/partials.module';
import { ActionComponent } from './header/action/action.component';
import { ProfileComponent } from './header/profile/profile.component';
import { CoreModule } from '@app/core/core.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from './components/inner/inner.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthenticationModule } from '@app/core/auth/authentication.module';

import { MessengerService } from '@app/core/services/messenger.service';
import { DataTableService } from '@app/core/services/datatable.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';


@NgModule({
	declarations: [
		PagesComponent,
		ActionComponent,
		ProfileComponent,
		ErrorPageComponent,
		InnerComponent,
	],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		PagesRoutingModule,
		CoreModule,
		LayoutModule,
		PartialsModule,
		AngularEditorModule,
		AuthenticationModule,
		NgbModule.forRoot(),
		AngularFireModule.initializeApp({
			apiKey: 'AIzaSyDI5Hfs0chSIz095d80ghZAwS75V_yBX3Y',
			authDomain: 'skillometry.firebaseapp.com',
			databaseURL: 'https://skillometry.firebaseio.com',
			projectId: 'skillometry',
			storageBucket: 'skillometry.appspot.com',
			messagingSenderId: '110544822600'
		}),
		AngularFireStorageModule,
	],
	providers: [
		MessengerService,
		DataTableService,
	]
})
export class PagesModule {
}
