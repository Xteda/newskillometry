var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ActionComponent } from './header/action/action.component';
import { ProfileComponent } from './header/profile/profile.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from './components/inner/inner.component';
var routes = [
    {
        path: '',
        component: PagesComponent,
        // Remove comment to enable login
        // canActivate: [NgxPermissionsGuard],
        data: {
            permissions: {
                only: ['ADMIN', 'USER'],
                except: ['GUEST'],
                redirectTo: '/login'
            }
        },
        children: [
            {
                path: '',
                loadChildren: './components/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'educational-center',
                loadChildren: './components/entity/entity.module#EntityModule'
            },
            {
                path: 'builder',
                loadChildren: './builder/builder.module#BuilderModule'
            },
            {
                path: 'header/actions',
                component: ActionComponent
            },
            {
                path: 'profile',
                component: ProfileComponent
            },
            {
                path: 'inner',
                component: InnerComponent
            },
        ]
    },
    {
        path: 'login',
        // canActivate: [NgxPermissionsGuard],
        loadChildren: './auth/auth.module#AuthModule',
        data: {
            permissions: {
                except: 'ADMIN'
            }
        }
    },
    {
        path: '404',
        component: ErrorPageComponent
    },
    {
        path: 'error/:type',
        component: ErrorPageComponent
    },
];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());
export { PagesRoutingModule };
