var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, HostBinding, ChangeDetectionStrategy } from '@angular/core';
var MessengerInComponent = /** @class */ (function () {
    function MessengerInComponent() {
        this.classes = 'm-messenger__wrapper';
    }
    MessengerInComponent.prototype.ngOnInit = function () { };
    __decorate([
        HostBinding('class')
    ], MessengerInComponent.prototype, "classes");
    __decorate([
        Input()
    ], MessengerInComponent.prototype, "message");
    MessengerInComponent = __decorate([
        Component({
            selector: 'm-messenger-in',
            templateUrl: './messenger-in.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], MessengerInComponent);
    return MessengerInComponent;
}());
export { MessengerInComponent };
