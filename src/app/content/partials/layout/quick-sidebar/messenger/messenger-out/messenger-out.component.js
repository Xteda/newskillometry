var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, HostBinding, ChangeDetectionStrategy } from '@angular/core';
var MessengerOutComponent = /** @class */ (function () {
    function MessengerOutComponent() {
        this.classes = 'm-messenger__wrapper';
    }
    MessengerOutComponent.prototype.ngOnInit = function () { };
    __decorate([
        HostBinding('class')
    ], MessengerOutComponent.prototype, "classes");
    __decorate([
        Input()
    ], MessengerOutComponent.prototype, "message");
    MessengerOutComponent = __decorate([
        Component({
            selector: 'm-messenger-out',
            templateUrl: './messenger-out.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], MessengerOutComponent);
    return MessengerOutComponent;
}());
export { MessengerOutComponent };
