var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTimelineComponent } from './list-timeline.component';
import { TimelineItemComponent } from './timeline-item/timeline-item.component';
import { CoreModule } from '../../../../../core/core.module';
var ListTimelineModule = /** @class */ (function () {
    function ListTimelineModule() {
    }
    ListTimelineModule = __decorate([
        NgModule({
            imports: [CommonModule, CoreModule],
            declarations: [ListTimelineComponent, TimelineItemComponent],
            exports: [ListTimelineComponent, TimelineItemComponent]
        })
    ], ListTimelineModule);
    return ListTimelineModule;
}());
export { ListTimelineModule };
