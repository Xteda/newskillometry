var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, Input } from '@angular/core';
var TimelineItemComponent = /** @class */ (function () {
    function TimelineItemComponent() {
        this.classes = 'm-list-timeline__item';
    }
    TimelineItemComponent.prototype.ngOnInit = function () {
        if (this.item.read) {
            this.classes += ' m-list-timeline__item--read';
        }
    };
    TimelineItemComponent.prototype.badgeClass = function () {
        var badges = {
            urgent: 'm-badge--info',
            important: 'm-badge--warning',
            resolved: 'm-badge--success',
            pending: 'm-badge--danger'
        };
        if (this.item.tags.length > 0) {
            return badges[this.item.tags[0]];
        }
    };
    __decorate([
        Input()
    ], TimelineItemComponent.prototype, "item");
    __decorate([
        HostBinding('class')
    ], TimelineItemComponent.prototype, "classes");
    TimelineItemComponent = __decorate([
        Component({
            selector: 'm-timeline-item',
            templateUrl: './timeline-item.component.html',
            styleUrls: ['./timeline-item.component.scss']
        })
    ], TimelineItemComponent);
    return TimelineItemComponent;
}());
export { TimelineItemComponent };
