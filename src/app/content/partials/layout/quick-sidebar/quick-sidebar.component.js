var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { QuickSidebarOffcanvasDirective } from '../../../../core/directives/quick-sidebar-offcanvas.directive';
var QuickSidebarComponent = /** @class */ (function () {
    function QuickSidebarComponent(el) {
        this.el = el;
        this.id = 'm_quick_sidebar';
        this.classes = 'm-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light';
        this.styleOverflow = 'hidden';
    }
    QuickSidebarComponent.prototype.ngOnInit = function () { };
    QuickSidebarComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this.mQuickSidebarOffcanvas = new QuickSidebarOffcanvasDirective(_this.el);
            // manually call the directives' lifecycle hook method
            _this.mQuickSidebarOffcanvas.ngAfterViewInit();
        });
    };
    __decorate([
        HostBinding('id')
    ], QuickSidebarComponent.prototype, "id");
    __decorate([
        HostBinding('class')
    ], QuickSidebarComponent.prototype, "classes");
    __decorate([
        HostBinding('attr.mQuickSidebarOffcanvas')
    ], QuickSidebarComponent.prototype, "mQuickSidebarOffcanvas");
    __decorate([
        HostBinding('style.overflow')
    ], QuickSidebarComponent.prototype, "styleOverflow");
    QuickSidebarComponent = __decorate([
        Component({
            selector: 'm-quick-sidebar',
            templateUrl: './quick-sidebar.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], QuickSidebarComponent);
    return QuickSidebarComponent;
}());
export { QuickSidebarComponent };
