var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, HostBinding, ChangeDetectionStrategy } from '@angular/core';
var TooltipsComponent = /** @class */ (function () {
    function TooltipsComponent() {
        this.classes = 'm-nav-sticky';
        this.marginTop = '30px';
    }
    TooltipsComponent.prototype.ngOnInit = function () { };
    __decorate([
        HostBinding('class')
    ], TooltipsComponent.prototype, "classes");
    __decorate([
        HostBinding('style.margin-top')
    ], TooltipsComponent.prototype, "marginTop");
    TooltipsComponent = __decorate([
        Component({
            selector: 'm-tooltips',
            templateUrl: './tooltips.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], TooltipsComponent);
    return TooltipsComponent;
}());
export { TooltipsComponent };
