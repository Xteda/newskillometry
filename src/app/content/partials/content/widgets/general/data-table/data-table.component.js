var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { merge } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../../../../core/models/query-params.model';
import { DataTableDataSource } from './data-table.data-source';
var DataTableComponent = /** @class */ (function () {
    function DataTableComponent(dataTableService) {
        this.dataTableService = dataTableService;
        this.displayedColumns = ['id', 'cManufacture',
            'cModel', 'cModelYear', 'cMileage', 'cColor', 'cPrice', 'cCondition', 'cStatus', 'actions'];
        this.selection = new SelectionModel(true, []);
    }
    DataTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        /* Data load will be triggered in two cases:
        - when a pagination event occurs => this.paginator.page
        - when a sort event occurs => this.sort.sortChange
        **/
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () {
            _this.loadItems();
        }))
            .subscribe();
        // Init DataSource
        this.dataSource = new DataTableDataSource(this.dataTableService);
        // First load
        this.loadItems(true);
    };
    DataTableComponent.prototype.loadItems = function (firstLoad) {
        if (firstLoad === void 0) { firstLoad = false; }
        var queryParams = new QueryParamsModel({}, this.sort.direction, this.sort.active, this.paginator.pageIndex, firstLoad ? 6 : this.paginator.pageSize);
        this.dataSource.loadItems(queryParams);
        this.selection.clear();
    };
    /* UI */
    DataTableComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = 0; }
        switch (status) {
            case 0:
                return 'Selling';
            case 1:
                return 'Sold';
        }
        return '';
    };
    DataTableComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = 0; }
        switch (status) {
            case 0:
                return 'success';
            case 1:
                return 'metal';
        }
        return '';
    };
    DataTableComponent.prototype.getItemConditionString = function (condition) {
        if (condition === void 0) { condition = 0; }
        switch (condition) {
            case 0:
                return 'New';
            case 1:
                return 'Used';
        }
        return '';
    };
    DataTableComponent.prototype.getItemCssClassByCondition = function (condition) {
        if (condition === void 0) { condition = 0; }
        switch (condition) {
            case 0:
                return 'primary';
            case 1:
                return 'accent';
        }
        return '';
    };
    __decorate([
        ViewChild(MatPaginator)
    ], DataTableComponent.prototype, "paginator");
    __decorate([
        ViewChild(MatSort)
    ], DataTableComponent.prototype, "sort");
    DataTableComponent = __decorate([
        Component({
            selector: 'm-data-table',
            templateUrl: './data-table.component.html'
        })
    ], DataTableComponent);
    return DataTableComponent;
}());
export { DataTableComponent };
