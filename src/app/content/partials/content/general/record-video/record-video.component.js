var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { finalize, map } from 'rxjs/operators';
import { MAT_DIALOG_DATA } from '@angular/material';
var RecordVideoComponent = /** @class */ (function () {
    function RecordVideoComponent(afStorage, dialogRef, data) {
        this.afStorage = afStorage;
        this.dialogRef = dialogRef;
        this.data = data;
        this.obj = {};
        this.deviceReady = false;
        this.isSaving = false;
        this.current = 0;
        this.isRecording = false;
        this.recorded = false;
    }
    RecordVideoComponent.prototype.closeDialog = function () {
        this.dialogRef.close(this.downloadSrc);
    };
    RecordVideoComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var visualizer = document.getElementById('visualizer');
        var paths = document.getElementById('vis-path');
        var mask = document.getElementById('mask');
        var h = document.getElementsByClassName('error-mic')[0];
        var path;
        this.videoRecord = videojs(document.getElementById('record-video'), {
            loop: false,
            fluid: true,
            width: 602,
            height: 339,
            plugins: {
                record: {
                    audio: true,
                    maxLength: 300,
                    debug: true,
                    timeSlice: 1000,
                    videoMimeType: 'video/webm;codecs=H264',
                    video: {
                        width: 1920,
                        height: 1080
                    }
                }
            }
        });
        this.videoRecord.record().getDevice();
        this.videoRecord.on('deviceReady', function () {
            _this.deviceReady = true;
            var soundAllowed = function (stream) {
                // Audio stops listening in FF without // window.persistAudioStream = stream;
                // https://bugzilla.mozilla.org/show_bug.cgi?id=965483
                // https://support.mozilla.org/en-US/questions/984179
                // window.persistAudioStream = stream;
                h.innerHTML = 'Thanks';
                h.setAttribute('style', 'opacity: 0;');
                var audioContent = new AudioContext();
                var audioStream = audioContent.createMediaStreamSource(stream);
                var analyser = audioContent.createAnalyser();
                audioStream.connect(analyser);
                analyser.fftSize = 1024;
                var frequencyArray = new Uint8Array(analyser.frequencyBinCount);
                visualizer.setAttribute('viewBox', '0 0 20 300');
                // Through the frequencyArray has a length longer than 255, there seems to be no
                // significant data after this point. Not worth visualizing.
                path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
                mask.appendChild(path);
                var doDraw = function () {
                    requestAnimationFrame(doDraw);
                    analyser.getByteFrequencyData(frequencyArray);
                    var adjustedLength;
                    adjustedLength = Math.floor(frequencyArray[1]) - (Math.floor(frequencyArray[1]) % 5);
                    paths.setAttribute('d', 'M ' + (1) + ',300  l 0,-' + adjustedLength);
                };
                doDraw();
            };
            var soundNotAllowed = function (error) {
                h.innerHTML = 'You must allow your microphone.';
                console.log(error);
            };
            navigator.getUserMedia({ audio: true }, soundAllowed, soundNotAllowed);
        });
        this.videoRecord.on('finishRecord', function () {
            _this.recData = _this.videoRecord.recordedData;
            _this.recorded = true;
        });
    };
    RecordVideoComponent.prototype.ngOnDestroy = function () {
        this.videoRecord.record().reset();
        this.videoRecord.record().stopDevice();
        this.recorded = false;
        this.isRecording = false;
        this.videoRecord.dispose();
        var videoArray = videojs.getPlayers();
        delete videoArray['record-video'];
        // this.downloadSrc.unsubscribe();
    };
    RecordVideoComponent.prototype.closeModal = function () {
        this.dialogRef.close();
    };
    // Methods
    RecordVideoComponent.prototype.startRecord = function () {
        var _this = this;
        if (!this.isRecording) {
            this.videoRecord.record().start();
            this.isRecording = true;
        }
        else {
            this.videoRecord.record().stop();
            setTimeout(function () {
                _this.videoRecord.play();
            }, 50);
        }
    };
    RecordVideoComponent.prototype.discardRecord = function () {
        this.videoRecord.record().reset();
        this.videoRecord.record().getDevice();
        this.recorded = false;
        this.isSaving = false;
        this.isRecording = false;
    };
    RecordVideoComponent.prototype.saveRecord = function () {
        var _this = this;
        this.isSaving = true;
        this.isRecording = false;
        var data = this.videoRecord.recordedData;
        if (this.videoRecord.recordedData.video) {
            data = this.videoRecord.recordedData.video;
        }
        this.ref = this.afStorage.ref(data.name);
        this.task = this.ref.put(data);
        this.uploadState = this.task.snapshotChanges().pipe(map(function (s) { return s.state; }));
        this.uploadProgress = this.task.percentageChanges();
        this.task.snapshotChanges().pipe(finalize(function () {
            _this.downloadSrc = _this.afStorage.ref(data.name).getDownloadURL();
            _this.videoDuration = _this.videoRecord.record().getDuration();
            _this.downloadSrc.subscribe(function (url) {
                _this.obj.videoUrl = url;
                _this.obj.dur = _this.videoDuration;
                _this.isSaving = false;
                _this.dialogRef.close(_this.obj);
            });
        })).subscribe();
    };
    RecordVideoComponent = __decorate([
        Component({
            selector: 'm-record-video',
            templateUrl: './record-video.component.html'
        }),
        __param(2, Inject(MAT_DIALOG_DATA))
    ], RecordVideoComponent);
    return RecordVideoComponent;
}());
export { RecordVideoComponent };
