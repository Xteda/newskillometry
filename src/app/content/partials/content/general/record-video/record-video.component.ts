import { Component, AfterViewInit, Inject, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';

@Component({
	selector: 'm-record-video',
	templateUrl: './record-video.component.html'
})
export class RecordVideoComponent implements AfterViewInit, OnDestroy {
	ref: AngularFireStorageReference;
	task: AngularFireUploadTask;
	uploadProgress: Observable<number>;
	uploadState: Observable<string>;

	obj: any = {};

	deviceReady: boolean = false;
	videoDuration: number;
	isSaving = false;
	current = 0;
	private videoRecord: any;
	public isRecording: boolean = false;
	public recorded: boolean = false;
	public recData: string;
	downloadSrc: Observable<string>;

	constructor(
		private afStorage: AngularFireStorage,
		public dialogRef: MatDialogRef<RecordVideoComponent>,
		@Inject(MAT_DIALOG_DATA) public data?: any
	) { }

	closeDialog() {
		this.dialogRef.close(this.downloadSrc);
	}

	ngAfterViewInit() {
		const visualizer = document.getElementById('visualizer');
		const paths = document.getElementById('vis-path');
		const mask = document.getElementById('mask');
		const h = document.getElementsByClassName('error-mic')[0];
		let path;
		this.videoRecord = videojs(document.getElementById('record-video'), {
			loop: false,
			fluid: true,
			width: 602,
			height: 339,
			plugins: {
				record: {
					audio: true,
					maxLength: 300,
					debug: true,
					timeSlice: 1000,
					videoMimeType: 'video/webm;codecs=H264',
					video: {
						width: 1920,
						height: 1080
					}
				}
			}
		});

		this.videoRecord.record().getDevice();

		this.videoRecord.on('deviceReady', () => {
			this.deviceReady = true;
			const soundAllowed = function(stream) {
				// Audio stops listening in FF without // window.persistAudioStream = stream;
				// https://bugzilla.mozilla.org/show_bug.cgi?id=965483
				// https://support.mozilla.org/en-US/questions/984179
				// window.persistAudioStream = stream;
				h.innerHTML = 'Thanks';
				h.setAttribute('style', 'opacity: 0;');
				const audioContent = new AudioContext();
				const audioStream = audioContent.createMediaStreamSource(stream);
				const analyser = audioContent.createAnalyser();
				audioStream.connect(analyser);
				analyser.fftSize = 1024;

				const frequencyArray = new Uint8Array(analyser.frequencyBinCount);
				visualizer.setAttribute('viewBox', '0 0 20 300');

				// Through the frequencyArray has a length longer than 255, there seems to be no
				// significant data after this point. Not worth visualizing.
				path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
				mask.appendChild(path);
				const doDraw = function() {
					requestAnimationFrame(doDraw);
					analyser.getByteFrequencyData(frequencyArray);
					let adjustedLength;
					adjustedLength = Math.floor(frequencyArray[1]) - (Math.floor(frequencyArray[1]) % 5);
					paths.setAttribute('d', 'M ' + (1) + ',300  l 0,-' + adjustedLength);

				};
				doDraw();
			};

			const soundNotAllowed = function(error) {
				h.innerHTML = 'You must allow your microphone.';
				console.log(error);
			};

			navigator.getUserMedia({ audio: true }, soundAllowed, soundNotAllowed);
		});

		this.videoRecord.on('finishRecord', () => {
			this.recData = this.videoRecord.recordedData;
			this.recorded = true;
		});

	}

	ngOnDestroy() {
		this.videoRecord.record().reset();
		this.videoRecord.record().stopDevice();
		this.recorded = false;
		this.isRecording = false;
		this.videoRecord.dispose();
		const videoArray = videojs.getPlayers();
		delete videoArray['record-video'];
		// this.downloadSrc.unsubscribe();
	}

	closeModal() {
		this.dialogRef.close();
	}

	// Methods
	startRecord() {
		if (!this.isRecording) {
			this.videoRecord.record().start();
			this.isRecording = true;
		} else {
			this.videoRecord.record().stop();
			setTimeout(() => {
				this.videoRecord.play();
			}, 50);
		}
	}

	discardRecord() {
		this.videoRecord.record().reset();
		this.videoRecord.record().getDevice();
		this.recorded = false;
		this.isSaving = false;
		this.isRecording = false;
	}

	saveRecord() {
		this.isSaving = true;
		this.isRecording = false;
		let data = this.videoRecord.recordedData;
		if (this.videoRecord.recordedData.video) {
			data = this.videoRecord.recordedData.video;
		}
		this.ref = this.afStorage.ref(data.name);
		this.task = this.ref.put(data);
		this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
		this.uploadProgress = this.task.percentageChanges();
		this.task.snapshotChanges().pipe(
			finalize(() => {
				this.downloadSrc = this.afStorage.ref(data.name).getDownloadURL();
				this.videoDuration = this.videoRecord.record().getDuration();
				this.downloadSrc.subscribe(url => {
					this.obj.videoUrl = url;
					this.obj.dur = this.videoDuration;
					this.isSaving = false;
					this.dialogRef.close(this.obj);

				});
			})
		).subscribe();
	}
}
