var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
var NoticeComponent = /** @class */ (function () {
    function NoticeComponent() {
        this.classes = '';
    }
    NoticeComponent.prototype.ngOnInit = function () {
        if (this.icon) {
            this.classes += ' m-alert--icon';
        }
    };
    __decorate([
        Input()
    ], NoticeComponent.prototype, "classes");
    __decorate([
        Input()
    ], NoticeComponent.prototype, "icon");
    NoticeComponent = __decorate([
        Component({
            selector: 'm-notice',
            templateUrl: './notice.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], NoticeComponent);
    return NoticeComponent;
}());
export { NoticeComponent };
