var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { HighlightModule } from 'ngx-highlightjs';
import { MaterialPreviewComponent } from './material-preview.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { CoreModule } from '../../../../../core/core.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
var MaterialPreviewModule = /** @class */ (function () {
    function MaterialPreviewModule() {
    }
    MaterialPreviewModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                NgbModule,
                CoreModule,
                HighlightModule.forRoot({ theme: 'googlecode' }),
                PerfectScrollbarModule,
                // material modules
                MatTabsModule,
                MatExpansionModule,
                MatCardModule,
                MatIconModule
            ],
            exports: [MaterialPreviewComponent],
            declarations: [MaterialPreviewComponent]
        })
    ], MaterialPreviewModule);
    return MaterialPreviewModule;
}());
export { MaterialPreviewModule };
