var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
var MaterialPreviewComponent = /** @class */ (function () {
    function MaterialPreviewComponent() {
    }
    MaterialPreviewComponent.prototype.ngOnInit = function () {
    };
    MaterialPreviewComponent.prototype.changeCodeVisibility = function () {
        this.viewItem.isCodeVisible = !this.viewItem.isCodeVisible;
    };
    MaterialPreviewComponent.prototype.hasExampleSource = function () {
        if (!this.viewItem) {
            return false;
        }
        if (!this.viewItem.cssCode && !this.viewItem.htmlCode && !this.viewItem.scssCode && !this.viewItem.tsCode) {
            return false;
        }
        return true;
    };
    __decorate([
        Input()
    ], MaterialPreviewComponent.prototype, "viewItem");
    MaterialPreviewComponent = __decorate([
        Component({
            selector: 'm-material-preview',
            templateUrl: './material-preview.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], MaterialPreviewComponent);
    return MaterialPreviewComponent;
}());
export { MaterialPreviewComponent };
