var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import * as objectPath from 'object-path';
var PortletComponent = /** @class */ (function () {
    function PortletComponent(loader) {
        this.loader = loader;
        this.loader.complete();
    }
    PortletComponent.prototype.ngAfterViewInit = function () {
        // hide portlet footer if no content
        if (this.elFooter && this.elFooter.nativeElement.children.length == 0) {
            this.elFooter.nativeElement.style.display = 'none';
            this.elPortlet.nativeElement.classList.add('m-portlet--full-height');
        }
        // add portlet responsive mobile for sticky portlet
        if (objectPath.get(this.options, 'enableSticky')) {
            this.elPortlet.nativeElement.classList.add('m-portlet--responsive-mobile');
        }
        // hide portlet header tools if no content
        if (this.elHeadTools && this.elHeadTools.nativeElement.children.length == 0) {
            this.elHeadTools.nativeElement.style.display = 'none';
        }
    };
    PortletComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], PortletComponent.prototype, "loading$");
    __decorate([
        Input()
    ], PortletComponent.prototype, "options");
    __decorate([
        ViewChild('mPortlet')
    ], PortletComponent.prototype, "elPortlet");
    __decorate([
        ViewChild('mPortletHead')
    ], PortletComponent.prototype, "elHead");
    __decorate([
        ViewChild('mPortletBody')
    ], PortletComponent.prototype, "elBody");
    __decorate([
        ViewChild('mPortletFooter')
    ], PortletComponent.prototype, "elFooter");
    __decorate([
        ViewChild('mPortletHeadTools')
    ], PortletComponent.prototype, "elHeadTools");
    PortletComponent = __decorate([
        Component({
            selector: 'm-portlet',
            templateUrl: './portlet.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], PortletComponent);
    return PortletComponent;
}());
export { PortletComponent };
