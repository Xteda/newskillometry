var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionControlConfig } from './accordion-control.config';
import { AccordionControlComponent, AccordionControlPanelDirective, AccordionControlPanelTitleDirective, AccordionControlPanelContentDirective } from './accordion-control.component';
export { AccordionControlConfig } from './accordion-control.config';
export { AccordionControlComponent, AccordionControlPanelDirective, AccordionControlPanelTitleDirective, AccordionControlPanelContentDirective } from './accordion-control.component';
var ACCORDION_CONTROL_DIRECTIVES = [
    AccordionControlComponent,
    AccordionControlPanelDirective,
    AccordionControlPanelTitleDirective,
    AccordionControlPanelContentDirective
];
var AccordionControlModule = /** @class */ (function () {
    function AccordionControlModule() {
    }
    AccordionControlModule_1 = AccordionControlModule;
    AccordionControlModule.forRoot = function () {
        return { ngModule: AccordionControlModule_1, providers: [AccordionControlConfig] };
    };
    AccordionControlModule = AccordionControlModule_1 = __decorate([
        NgModule({
            imports: [
                CommonModule
            ],
            exports: ACCORDION_CONTROL_DIRECTIVES,
            declarations: ACCORDION_CONTROL_DIRECTIVES
        })
    ], AccordionControlModule);
    return AccordionControlModule;
    var AccordionControlModule_1;
}());
export { AccordionControlModule };
