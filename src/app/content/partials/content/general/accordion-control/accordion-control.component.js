var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ContentChildren, Directive, EventEmitter, Input, Output, ChangeDetectionStrategy } from '@angular/core';
import { isString } from '../../../../../core/services/utils.service';
var nextId = 0;
/**
 * This directive should be used to wrap accordion panel titles that need to contain HTML markup or other directives.
 */
var AccordionControlPanelTitleDirective = /** @class */ (function () {
    function AccordionControlPanelTitleDirective(templateRef) {
        this.templateRef = templateRef;
    }
    AccordionControlPanelTitleDirective = __decorate([
        Directive({
            selector: 'ng-template[AccordionControlPanelTitle]'
        })
    ], AccordionControlPanelTitleDirective);
    return AccordionControlPanelTitleDirective;
}());
export { AccordionControlPanelTitleDirective };
/**
 * This directive must be used to wrap accordion panel content.
 */
var AccordionControlPanelContentDirective = /** @class */ (function () {
    function AccordionControlPanelContentDirective(templateRef) {
        this.templateRef = templateRef;
    }
    AccordionControlPanelContentDirective = __decorate([
        Directive({
            selector: 'ng-template[AccordionControlPanelContent]'
        })
    ], AccordionControlPanelContentDirective);
    return AccordionControlPanelContentDirective;
}());
export { AccordionControlPanelContentDirective };
/**
 * The NgbPanel directive represents an individual panel with the title and collapsible
 * content
 */
var AccordionControlPanelDirective = /** @class */ (function () {
    function AccordionControlPanelDirective() {
        /**
         *  A flag determining whether the panel is disabled or not.
         *  When disabled, the panel cannot be toggled.
         */
        this.disabled = false;
        this.height = 0;
        this.contentHeight = 0;
        /**
         *  An optional id for the panel. The id should be unique.
         *  If not provided, it will be auto-generated.
         */
        this.id = "m-accordion-control-panel-" + nextId++;
        /**
         * A flag telling if the panel is currently open
         */
        this.isOpen = false;
    }
    AccordionControlPanelDirective.prototype.ngAfterContentChecked = function () {
        // We are using @ContentChildren instead of @ContantChild as in the Angular version being used
        // only @ContentChildren allows us to specify the {descendants: false} option.
        this.titleTpl = this.titleTpls.first;
        this.contentTpl = this.contentTpls.first;
    };
    __decorate([
        Input()
    ], AccordionControlPanelDirective.prototype, "disabled");
    __decorate([
        Input()
    ], AccordionControlPanelDirective.prototype, "id");
    __decorate([
        Input()
    ], AccordionControlPanelDirective.prototype, "title");
    __decorate([
        Input()
    ], AccordionControlPanelDirective.prototype, "iconClass");
    __decorate([
        Input()
    ], AccordionControlPanelDirective.prototype, "type");
    __decorate([
        ContentChildren(AccordionControlPanelTitleDirective, { descendants: false })
    ], AccordionControlPanelDirective.prototype, "titleTpls");
    __decorate([
        ContentChildren(AccordionControlPanelContentDirective, { descendants: false })
    ], AccordionControlPanelDirective.prototype, "contentTpls");
    AccordionControlPanelDirective = __decorate([
        Directive({
            selector: 'm-accordion-control-panel'
        })
    ], AccordionControlPanelDirective);
    return AccordionControlPanelDirective;
}());
export { AccordionControlPanelDirective };
/**
 * The NgbAccordion directive is a collection of panels.
 * It can assure that only one panel can be opened at a time.
 */
var AccordionControlComponent = /** @class */ (function () {
    function AccordionControlComponent() {
        /**
         * An array or comma separated strings of panel identifiers that should be opened
         */
        this.activeIds = [];
        this.hasAnimation = false;
        /**
         * Whether the closed panels should be hidden without destroying them
         */
        this.destroyOnHide = true;
        /**
         * A panel change event fired right before the panel toggle happens. See PanelChangeEvent for payload details
         */
        this.panelChange = new EventEmitter();
    }
    /**
     * Programmatically toggle a panel with a given id.
     */
    AccordionControlComponent.prototype.toggle = function (panelId, accordionBodyScrollHeight) {
        var panel = this.panels.find(function (p) { return p.id === panelId; });
        if (panel && !panel.disabled) {
            var defaultPrevented_1 = false;
            if (this.hasAnimation) {
                panel.height = panel.height ? 0 : panel.contentHeight;
            }
            this.panelChange.emit({ panelId: panelId, nextState: !panel.isOpen, preventDefault: function () { defaultPrevented_1 = true; } });
            if (!defaultPrevented_1) {
                panel.isOpen = !panel.isOpen;
                if (this.closeOthers) {
                    this._closeOthers(panelId);
                }
                this._updateActiveIds();
            }
        }
    };
    AccordionControlComponent.prototype.ngAfterContentChecked = function () {
        var _this = this;
        // active id updates
        if (isString(this.activeIds)) {
            this.activeIds = this.activeIds.split(/\s*,\s*/);
        }
        // update panels open states
        this.panels.forEach(function (panel) {
            panel.isOpen = !panel.disabled && _this.activeIds.indexOf(panel.id) > -1;
            if (_this.hasAnimation) {
                var domPanel = document.getElementById(panel.id);
                panel.contentHeight = domPanel && domPanel.scrollHeight ? domPanel.scrollHeight : 200;
            }
        });
        // closeOthers updates
        if (this.activeIds.length > 1 && this.closeOthers) {
            this._closeOthers(this.activeIds[0]);
            this._updateActiveIds();
        }
    };
    AccordionControlComponent.prototype._closeOthers = function (panelId) {
        this.panels.forEach(function (panel) {
            if (panel.id !== panelId) {
                panel.isOpen = false;
            }
        });
    };
    AccordionControlComponent.prototype._updateActiveIds = function () {
        this.activeIds = this.panels.filter(function (panel) { return panel.isOpen && !panel.disabled; }).map(function (panel) { return panel.id; });
    };
    __decorate([
        ContentChildren(AccordionControlPanelDirective)
    ], AccordionControlComponent.prototype, "panels");
    __decorate([
        Input()
    ], AccordionControlComponent.prototype, "activeIds");
    __decorate([
        Input()
    ], AccordionControlComponent.prototype, "hasAnimation");
    __decorate([
        Input()
    ], AccordionControlComponent.prototype, "closeOthers");
    __decorate([
        Input()
    ], AccordionControlComponent.prototype, "destroyOnHide");
    __decorate([
        Input()
    ], AccordionControlComponent.prototype, "type");
    __decorate([
        Output()
    ], AccordionControlComponent.prototype, "panelChange");
    AccordionControlComponent = __decorate([
        Component({
            selector: 'm-accordion-control',
            exportAs: 'AccordionControl',
            host: {
                'role': 'tablist',
                '[attr.aria-multiselectable]': '!closeOtherPanels',
                'class': 'm-accordion'
            },
            templateUrl: './accordion-control.component.html',
            styles: ["\n\t\t.m-accordion--animation {\n\t\t\toverflow: hidden;\n        \t-webkit-transition: height .5s;\n      \t\ttransition: height .5s;\n\t\t}\n\t"],
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], AccordionControlComponent);
    return AccordionControlComponent;
}());
export { AccordionControlComponent };
