var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
var CodePreviewComponent = /** @class */ (function () {
    function CodePreviewComponent() {
    }
    CodePreviewComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], CodePreviewComponent.prototype, "title");
    __decorate([
        Input()
    ], CodePreviewComponent.prototype, "htmlCode");
    __decorate([
        Input()
    ], CodePreviewComponent.prototype, "tsCode");
    __decorate([
        Input()
    ], CodePreviewComponent.prototype, "scssCode");
    CodePreviewComponent = __decorate([
        Component({
            selector: 'm-code-preview',
            templateUrl: './code-preview.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        })
    ], CodePreviewComponent);
    return CodePreviewComponent;
}());
export { CodePreviewComponent };
