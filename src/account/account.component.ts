﻿import {Component, ViewContainerRef, OnInit, ViewEncapsulation, Injector, HostBinding} from '@angular/core';
import { LoginService } from './login/login.service';
import { AppComponentBase } from '@shared/app-component-base';
import {LayoutConfig} from '@app/config/layout';
import {LayoutConfigService} from '@app/core/services/layout-config.service';

@Component({
	selector: 'm-auth',
    templateUrl: './account.component.html',
    styleUrls: [
        './account.component.less'
    ],
    encapsulation: ViewEncapsulation.None
})
export class AccountComponent extends AppComponentBase implements OnInit {

    private viewContainerRef: ViewContainerRef;
	@HostBinding('class')
		// tslint:disable-next-line:max-line-length
	classses: any = 'm-grid m-grid--hor m-grid--root m-page';
	today = new Date();

    versionText: string;
    currentYear: number;

    public constructor(
		private layoutConfigService: LayoutConfigService,
        injector: Injector,
        private _loginService: LoginService
    ) {
        super(injector);

        this.currentYear = new Date().getFullYear();
        this.versionText = this.appSession.application.version + ' [' + this.appSession.application.releaseDate.format('YYYYDDMM') + ']';
    }

    showTenantChange(): boolean {
        return abp.multiTenancy.isEnabled;
    }

    ngOnInit(): void {
        $('body').attr('class', 'login-page');
		this.layoutConfigService.setModel(new LayoutConfig({ content: { skin: '' } }), true);
		/*if (this.splashScreen) {
			this.splashScreenService.init(this.splashScreen.nativeElement);
		}*/
    }
}
