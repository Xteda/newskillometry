var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { IsTenantAvailableInput } from '@shared/service-proxies/service-proxies';
import { AppTenantAvailabilityState } from '@shared/AppEnums';
// import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
var TenantChangeModalComponent = /** @class */ (function (_super) {
    __extends(TenantChangeModalComponent, _super);
    function TenantChangeModalComponent(_accountService, injector) {
        var _this = _super.call(this, injector) || this;
        _this._accountService = _accountService;
        _this.tenancyName = '';
        _this.active = false;
        _this.saving = false;
        return _this;
    }
    TenantChangeModalComponent.prototype.show = function (tenancyName) {
        this.tenancyName = tenancyName;
        this.active = true;
        // this.modal.show();
    };
    TenantChangeModalComponent.prototype.onShown = function () {
        $(this.tenancyNameInput.nativeElement).focus().select();
    };
    TenantChangeModalComponent.prototype.save = function () {
        var _this = this;
        if (!this.tenancyName) {
            abp.multiTenancy.setTenantIdCookie(undefined);
            this.close();
            location.reload();
            return;
        }
        var input = new IsTenantAvailableInput();
        input.tenancyName = this.tenancyName;
        this.saving = true;
        this._accountService.isTenantAvailable(input)
            .pipe(finalize(function () { _this.saving = false; }))
            .subscribe(function (result) {
            switch (result.state) {
                case AppTenantAvailabilityState.Available:
                    abp.multiTenancy.setTenantIdCookie(result.tenantId);
                    _this.close();
                    location.reload();
                    return;
                case AppTenantAvailabilityState.InActive:
                    _this.message.warn(_this.l('TenantIsNotActive', _this.tenancyName));
                    break;
                case AppTenantAvailabilityState.NotFound://NotFound
                    _this.message.warn(_this.l('ThereIsNoTenantDefinedWithName{0}', _this.tenancyName));
                    break;
            }
        });
    };
    TenantChangeModalComponent.prototype.close = function () {
        this.active = false;
        // this.modal.hide();
    };
    __decorate([
        ViewChild('tenancyNameInput')
    ], TenantChangeModalComponent.prototype, "tenancyNameInput");
    __decorate([
        ViewChild('modalContent')
    ], TenantChangeModalComponent.prototype, "modalContent");
    TenantChangeModalComponent = __decorate([
        Component({
            selector: 'tenantChangeModal',
            templateUrl: './tenant-change-modal.component.html'
        })
    ], TenantChangeModalComponent);
    return TenantChangeModalComponent;
}(AppComponentBase));
export { TenantChangeModalComponent };
