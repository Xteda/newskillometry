var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
var AccountLanguagesComponent = /** @class */ (function (_super) {
    __extends(AccountLanguagesComponent, _super);
    function AccountLanguagesComponent(injector) {
        return _super.call(this, injector) || this;
    }
    AccountLanguagesComponent.prototype.ngOnInit = function () {
        this.languages = _.filter(this.localization.languages, function (l) { return !l.isDisabled; });
        this.currentLanguage = this.localization.currentLanguage;
    };
    AccountLanguagesComponent.prototype.changeLanguage = function (languageName) {
        abp.utils.setCookieValue("Abp.Localization.CultureName", languageName, new Date(new Date().getTime() + 5 * 365 * 86400000), //5 year
        abp.appPath);
        location.reload();
    };
    AccountLanguagesComponent = __decorate([
        Component({
            selector: 'account-languages',
            templateUrl: './account-languages.component.html',
            styleUrls: [
                './account-languages.component.less'
            ]
        })
    ], AccountLanguagesComponent);
    return AccountLanguagesComponent;
}(AppComponentBase));
export { AccountLanguagesComponent };
