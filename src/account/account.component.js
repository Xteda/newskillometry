var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
var AccountComponent = /** @class */ (function (_super) {
    __extends(AccountComponent, _super);
    function AccountComponent(injector, _loginService) {
        var _this = _super.call(this, injector) || this;
        _this._loginService = _loginService;
        _this.currentYear = new Date().getFullYear();
        _this.versionText = _this.appSession.application.version + ' [' + _this.appSession.application.releaseDate.format('YYYYDDMM') + ']';
        return _this;
    }
    AccountComponent.prototype.showTenantChange = function () {
        return abp.multiTenancy.isEnabled;
    };
    AccountComponent.prototype.ngOnInit = function () {
        $('body').attr('class', 'login-page');
    };
    AccountComponent = __decorate([
        Component({
            templateUrl: './account.component.html',
            styleUrls: [
                './account.component.less'
            ],
            encapsulation: ViewEncapsulation.None
        })
    ], AccountComponent);
    return AccountComponent;
}(AppComponentBase));
export { AccountComponent };
