var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
var LoginComponent = /** @class */ (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(injector, loginService, _router, _sessionService) {
        var _this = _super.call(this, injector) || this;
        _this.loginService = loginService;
        _this._router = _router;
        _this._sessionService = _sessionService;
        _this.submitting = false;
        return _this;
    }
    LoginComponent.prototype.ngAfterViewInit = function () {
        $(this.cardBody.nativeElement).find('input:first').focus();
    };
    Object.defineProperty(LoginComponent.prototype, "multiTenancySideIsTeanant", {
        get: function () {
            return this._sessionService.tenantId > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "isSelfRegistrationAllowed", {
        get: function () {
            if (!this._sessionService.tenantId) {
                return false;
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.submitting = true;
        this.loginService.authenticate(function () { return _this.submitting = false; });
    };
    __decorate([
        ViewChild('cardBody')
    ], LoginComponent.prototype, "cardBody");
    LoginComponent = __decorate([
        Component({
            templateUrl: './login.component.html',
            styleUrls: [
                './login.component.less'
            ],
            animations: [accountModuleAnimation()]
        })
    ], LoginComponent);
    return LoginComponent;
}(AppComponentBase));
export { LoginComponent };
