﻿import {Component, Injector, ElementRef, ViewChild, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/app-component-base';
import { LoginService } from './login.service';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AbpSessionService } from '@abp/session/abp-session.service';
import { AuthNoticeService} from '@app/core/auth/auth-notice.service';
import { SpinnerButtonOptions } from '../../app/content/partials/content/general/spinner-button/button-options.interface';

@Component({
    templateUrl: './login.component.html',
    styleUrls: [
        './login.component.less'
    ],
    animations: [accountModuleAnimation()]
})
export class LoginComponent extends AppComponentBase implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('cardBody') cardBody: ElementRef;

    submitting: boolean = false;

	spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};

    constructor(
        injector: Injector,
        public loginService: LoginService,
        private _router: Router,
		public authNoticeService: AuthNoticeService,
        private _sessionService: AbpSessionService
    ) {
        super(injector);
    }

    ngOnInit() {
		if (!this.authNoticeService.onNoticeChanged$.getValue()) {
			const initialNotice = `Use account
			<strong>admin</strong> and password
			<strong>123qwe</strong> to continue.`;
			this.authNoticeService.setNotice(initialNotice, 'success');
		}
	}

    ngAfterViewInit(): void {
        $(this.cardBody.nativeElement).find('input:first').focus();
    }

    get multiTenancySideIsTeanant(): boolean {
        return this._sessionService.tenantId > 0;
    }

    get isSelfRegistrationAllowed(): boolean {
        if (!this._sessionService.tenantId) {
            return false;
        }

        return true;
    }

    login(): void {
        this.submitting = true;
        this.loginService.authenticate(
            () => this.submitting = false
        );
    }

	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
	}
}
