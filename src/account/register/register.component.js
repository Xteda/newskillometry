var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, ViewChild } from '@angular/core';
import { RegisterInput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
var RegisterComponent = /** @class */ (function (_super) {
    __extends(RegisterComponent, _super);
    function RegisterComponent(injector, _accountService, _router, _loginService) {
        var _this = _super.call(this, injector) || this;
        _this._accountService = _accountService;
        _this._router = _router;
        _this._loginService = _loginService;
        _this.model = new RegisterInput();
        _this.saving = false;
        return _this;
    }
    RegisterComponent.prototype.ngAfterViewInit = function () {
        $(this.cardBody.nativeElement).find('input:first').focus();
    };
    RegisterComponent.prototype.back = function () {
        this._router.navigate(['/login']);
    };
    RegisterComponent.prototype.save = function () {
        var _this = this;
        this.saving = true;
        this._accountService.register(this.model)
            .pipe(finalize(function () { _this.saving = false; }))
            .subscribe(function (result) {
            if (!result.canLogin) {
                _this.notify.success(_this.l('SuccessfullyRegistered'));
                _this._router.navigate(['/login']);
                return;
            }
            //Autheticate
            _this.saving = true;
            _this._loginService.authenticateModel.userNameOrEmailAddress = _this.model.userName;
            _this._loginService.authenticateModel.password = _this.model.password;
            _this._loginService.authenticate(function () { _this.saving = false; });
        });
    };
    __decorate([
        ViewChild('cardBody')
    ], RegisterComponent.prototype, "cardBody");
    RegisterComponent = __decorate([
        Component({
            templateUrl: './register.component.html',
            animations: [accountModuleAnimation()]
        })
    ], RegisterComponent);
    return RegisterComponent;
}(AppComponentBase));
export { RegisterComponent };
