export var environment = {
    production: true,
    hmr: false,
    appConfig: 'appconfig.json'
};
