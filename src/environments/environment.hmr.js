// "Hot Module Replacement" enabled environment
export var environment = {
    production: false,
    hmr: true,
    appConfig: 'appconfig.json'
};
