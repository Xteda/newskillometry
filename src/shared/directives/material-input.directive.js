var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Directive, Injectable } from '@angular/core';
var MaterialInput = /** @class */ (function () {
    function MaterialInput(_element) {
        this._element = _element;
    }
    MaterialInput.prototype.ngAfterViewInit = function () {
        $.AdminBSB.input.activate($(this._element.nativeElement).parent());
    };
    MaterialInput = __decorate([
        Directive({
            selector: '[materialInput]'
        }),
        Injectable()
    ], MaterialInput);
    return MaterialInput;
}());
export { MaterialInput };
