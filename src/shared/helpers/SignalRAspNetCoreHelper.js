import { AppConsts } from '@shared/AppConsts';
import { UtilsService } from '@abp/utils/utils.service';
var SignalRAspNetCoreHelper = /** @class */ (function () {
    function SignalRAspNetCoreHelper() {
    }
    SignalRAspNetCoreHelper.initSignalR = function () {
        var encryptedAuthToken = new UtilsService().getCookieValue(AppConsts.authorization.encrptedAuthTokenName);
        abp.signalr = {
            autoConnect: true,
            connect: undefined,
            hubs: undefined,
            qs: AppConsts.authorization.encrptedAuthTokenName + "=" + encodeURIComponent(encryptedAuthToken),
            remoteServiceBaseUrl: AppConsts.remoteServiceBaseUrl,
            startConnection: undefined,
            url: '/signalr'
        };
        jQuery.getScript(AppConsts.appBaseUrl + '/assets/abp/abp.signalr-client.js');
    };
    return SignalRAspNetCoreHelper;
}());
export { SignalRAspNetCoreHelper };
