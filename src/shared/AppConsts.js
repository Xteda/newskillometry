var AppConsts = /** @class */ (function () {
    function AppConsts() {
    }
    AppConsts.userManagement = {
        defaultAdminUserName: 'admin'
    };
    AppConsts.localization = {
        defaultLocalizationSourceName: 'TrainingMachine'
    };
    AppConsts.authorization = {
        encrptedAuthTokenName: 'enc_auth_token'
    };
    return AppConsts;
}());
export { AppConsts };
